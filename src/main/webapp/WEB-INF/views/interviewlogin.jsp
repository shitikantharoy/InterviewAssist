<%@page session="false"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Interview Login Page</title>

<c:url var="home" value="/" scope="request" />
<meta charset='utf-8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css"> 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/login.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/formElement.css">      
<script src="${pageContext.request.contextPath}/resources/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/jquery-ui.js"></script>

<%-- <spring:url value="/resources/core/js/jquery.1.10.2.min.js"
	var="jqueryJs" /> --%>
<%-- <script src="${jqueryJs}"></script> --%>
<script>
function validate(){
   
	var error =  "";
	var name = document.interview.name.value;
	var email = document.interview.email.value;
	var phone = document.interview.phone.value;
	
	if(name == "" || email == "" || phone == "" ) {
  
		error = "Please fill all the boxes before submitting!";
  
	}  else if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))) {
    	
    	error = "You have entered an invalid email address!";
    } else {
    	
    	var stripped = phone.replace(/[\(\)\.\-\ ]/g, '');
       	if (stripped == "" ||
     		   isNaN(parseInt(stripped)) ||
     		   !(stripped.length == 10)) {
       		
       		error = "You have entered a invalid phone number!";	
       	}
    } 
	
	if(error != "") {
	      
	     	$("div.error").show();
			$("div.error").focus();
			$("div.error span").html(error);
			
			return false;
	} else {
		return true;
	}

}
function reset()
{
	document.getElementById("interview").reset();
}

/* function validateEmail(mail)   
{  
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))  
  {  
    return (true)  
  }  
    alert("You have entered an invalid email address!")  
    return (false)  
}  

function validatePhone(phone) {
    var error = "";
    var stripped = phone.replace(/[\(\)\.\-\ ]/g, '');

   	if (stripped == "" ||
		   isNaN(parseInt(stripped)) ||
		   !(stripped.length == 10)) {
        error = "You didn't enter a phone number.";
    } else if (isNaN(parseInt(stripped))) {
        phone = "";
        error = "The phone number contains illegal characters.";

    } else if (!(stripped.length == 10)) {
        phone = "";
        error = "The phone number is the wrong length. Make sure you included an area code.\n";
    }
   
	if(error != "") {
		alert(error);
		return false;
	}

	return true;
} */
</script>
</head>

<body onload="reset()">
   
			<header>
				<div class="container">
				<img src="/InterviewAssist/resources/logo.png" alt="logo" />
				</div>
			</header>
	      <h1 align="center" class="htext"><font face=Calibri>Welcome to CenturyLink</font></h1>
	      
	      
			
				<section id="content">
				<form name='interview' id="interview" action="<c:url value='/instructions' />" method='POST' onsubmit="return validate();" >
	      		<%--<form id="loginForm" method="post" action="<c:url value='/validate' />" enctype="application/x-www-form-urlencoded"> --%>

	      				<h1>Candidate Login</h1>
	      				
	      				<div  style="color: red">${errorMessage}</div>
	      				<div class="error" tabindex="1" style="display:none;color: red;margin-top: 10px;">
			<img src="/InterviewAssist/resources/warning.gif" alt="Warning!" width="24" height="24" style="float:left; margin: -5px 10px 0px 0px;">
			<span></span>
			<br clear="all">
		</div>

			<div class="form-row-login">

				<label>Name</label> 
				<%--<input type="text" name="uName" id ="uNameId"  size="13"/> --%>
				<input type=text id="name" name="name">
			</div>

			<div class="form-row-login">
				<label>Email</label> 
                <%-- <input type="password" name="password" size="13" id="passwordId"/>--%>
                <input type="text" class="form-control" id="email" name="email">                        
			</div>


			<div class="form-row-login">
				<label>Phone</label> 
                <%-- <input type="password" name="password" size="13" id="passwordId"/>--%>
               <input type="text" id="phone" name="phone">
			</div>

			
			<div class="form-row-login">
				 <input type="hidden" id="action" name="action" value="getinstruction">
				 <input type="submit" name="login" value="Login" />
				 <input type="reset" name="clear" value="Clear" />
				
			</div>
           
            <input type="hidden" name="event" value="<c:out value="${event}"/>"/>
			
		</form></section>
        <footer>
       	<div id="footer" >
				2017   CenturyLink India, Inc. All Rights Reserved. 
		</div>  
		</footer>  
       </div>
      	
      	
    
</body>

</html>