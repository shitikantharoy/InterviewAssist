<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.centurylink.spring.model.*" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="UTF-8">
 <title>InCTLI</title>      
      <script type="text/javascript">
      
      </script>
    </head>
    <body>
			
				<section id="content">
<form:form method="POST" action="importQuestionToDB" modelAttribute="fileBucket" enctype="multipart/form-data" class="form-horizontal" id="questionForm">
		<br><br>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="levels" style="text-align: left;font-weight: bold;font-size: 120%;" for="file">Upload a file</label>
					
					<a href="${pageContext.request.contextPath}/resources/QuestionImport.xlsx" >Get Sample</a>
					
					<div class="form-row-login" style="margin-top:20px">
					
						<form:input type="file" path="file" id="file" class="form-control input-sm"/>
						<c:if test="${response!=null}"><c:out value="${response}"/></c:if>
					</div>
				</div>
			</div>
	
			<div class="form-row-login">
				<div class="form-actions floatRight">
					<input type="submit" value="Upload" class="btn btn-primary btn-sm" id="importQuestion">
				</div>
			</div>
		</form:form>	</section>
       <footer>
       	<div id="footer" >
				<br> 2017 CenturyLink India, Inc. All Rights Reserved. 
		</div>
		</footer>    
      	
      	
    
</body>
  
</html>
