<%@ page import="com.centurylink.spring.model.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta charset='utf-8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/jquery.datetimepicker.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="${pageContext.request.contextPath}/resources/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/login.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/formElement.css">
<meta charset="utf-8" />
<title>Expand all subgrids on load</title>
</head>
    
<script>

</script>
<form id="reportForm" method="post" action="#">




<h3 style="margin-bottom: 2px;">Details</h3>

<%Report report = new Report() ;
if(request.getAttribute("report")!=null ){
	report = (Report)request.getAttribute("report");
}
%>		

<table>
		<tr>
			<td>Question:</td>
			<td><%=report.getQuestionDesc() %></td>
		</tr>
		<%if("Programming".equalsIgnoreCase(report.getQuestionType())){ %>
			<tr>
			<td>Code:</td>
			<td><%=report.getCode() %></td>
		</tr>
		<%} %>
</table>



</form>


</html>