<!doctype html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang=''>
<head>
   <meta charset='utf-8'>
   <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="styles.css">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/menu.css">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/login.css">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/formElement.css">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/jquery-ui.css">
  
   <script src="${pageContext.request.contextPath}/resources/jquery.js"></script>
   <script src="${pageContext.request.contextPath}/resources/jquery-ui.js"></script>
   <script src="${pageContext.request.contextPath}/resources/script.js"></script>
   <title>InCTLI</title>

<script>
	$(function() {
		$("#privilegeID").selectmenu();
	});
</script>
</head>
<body>
<header><div class="container">
<img src="/InterviewAssist/resources/logo.png" alt="logo" /></div>

<nav>
<div class="container">
<div id='cssmenu'>
<ul>
   <li  class='has-sub'><a href='#'><span>Admin</span></a>
		 <ul>
         <li ><a href='<c:url value='/user?val=addUser'/>'><span>Add User</span></a></li>
         <li ><a href='<c:url value='/user?val=editUser'/>'><span>Edit/Search User</span></a></li>		 
      </ul>
   </li>
   <li  class='has-sub'><a href='#'><span>Questions</span></a>
		 <ul>
             <li ><a href='<c:url value='/question?val=addMCQQuestion'/>'><span>Add MCQ Questions</span></a></li>
             <li ><a href='<c:url value='/question?val=addProgQuestion'/>'><span>Add Programming Questions</span></a></li>
             <li ><a href='<c:url value='/importQuestions'/>'><span>Import MCQ Questions</span></a></li>
             <li ><a href='<c:url value='/question?val=editQuestion'/>'><span>Edit/Search Questions</span></a></li>
         </ul>
   </li>
   <li  class='has-sub'><a href='#'><span>Events</span></a>
		 <ul>
         <li ><a href='<c:url value='/question?val=addEvent'/>'><span>Add Event</span></a></li>
         <li ><a href='<c:url value='/question?val=editEvent'/>'><span>Edit/Search Event</span></a></li>		 
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Reports</span></a>
   	<ul>
         <li ><a href='<c:url value='/report?val=report'/>'><span>Report</span></a></li>
      </ul>
   </li>
   <li><a href='#'><span>Static</span></a></li>
   <li  class='has-sub'><a href='#'><span>Account</span></a>
		 <ul>
         <li ><a href='<c:url value='/login?logout'/>'><span>Logout</span></a></li>
      </ul>
   </li>
</ul>
</div>
</div>
</nav>

</header>

<section>
<div class="container">

<!-- <div>
	Welcome
</div> -->

<c:if test="${userSelect=='addUser'}">
		<div>
			<jsp:include page="addUser.jsp"></jsp:include>
		</div>
</c:if>

<c:if test="${userSelect=='editUser'}">
		<div>
			<jsp:include page="editUser.jsp"></jsp:include>
		</div>
</c:if>

<c:if test="${userSelect=='addMCQQuestion'}">
		<div>
			<jsp:include page="addQuestion.jsp"></jsp:include>
		</div>
</c:if>
<c:if test="${userSelect=='addProgQuestion'}">
		<div>
			<jsp:include page="addProgQuestion.jsp"></jsp:include>
		</div>
</c:if>
<c:if test="${userSelect=='editQuestion'}">
		<div>
			<jsp:include page="editQuestion.jsp"></jsp:include>
		</div>
</c:if>

<c:if test="${userSelect=='addEvent'}">
		<div>
			<jsp:include page="addEvent.jsp"></jsp:include>
		</div>
</c:if>

<c:if test="${userSelect=='editEvent'}">
		<div>
			<jsp:include page="editEvent.jsp"></jsp:include>
		</div>
</c:if>

<c:if test="${userSelect=='report'}">
		<div>
			<jsp:include page="report.jsp"></jsp:include>
		</div>
</c:if>

<c:if test="${userSelect=='importQuestions'}">
		<div>
			<jsp:include page="importQuestions.jsp"></jsp:include>
		</div>
</c:if>
</div>
</section>
<footer></footer>
</body>
</html>
