<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html lang="en">
<head>
<!-- The jQuery library is a prerequisite for all jqSuite products -->
<!-- Added for Modal dialog -->
  <script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/jquery.min.js"></script>
  <script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/bootstrap.min.js"></script>	
<!-- Added for Modal dialog --> 
<script type="text/ecmascript" src="../../../js/jquery.min.js"></script>
<!-- This is the Javascript file of jqGrid -->
<script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/jquery.jqGrid.js"></script>
<!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- We support more than 40 localizations -->
<script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/grid.locale-en.js"></script>
<script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<!-- The link to the CSS that the grid needs -->
<link rel="stylesheet" type="text/css" media="screen"
	href="${pageContext.request.contextPath}/resources/ui.jqgrid.css" />
<!-- Added for Modal dialog -->	
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap.min.css">
<!-- Added for Modal dialog -->

<meta charset="utf-8" />
<title>Expand all subgrids on load</title>
</head>
<body>
	<div class="container">
	
	  <!-- Modal -->
	  <div id="messageModal" class="modal fade">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <!-- dialog body -->
	      <div class="modal-body">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <label id="modalText"></label>
	      </div>
	      <!-- dialog buttons -->
	      <div class="modal-footer"><button type="button" class="btn btn-primary"  data-dismiss="modal">OK</button></div>
	    </div>
	  </div>
	</div>
	</div>
		
	<div class="headerDIV">
	EDIT QUESTION
	</div>
	<div style="margin-top: 10px;"></div>
	<table id="jqGrid"></table>
	<div id="jqGridPager"></div>
	 <div id="detailsPlaceholder" style="margin-top: 50px;">
       
       
       <table id="jqGridMCQDetails"></table>
	<div id="jqGridMCQDetailsPager"></div>
    </div>




	<script type="text/javascript">
    
  
		$(document).ready(function() {
			$("#jqGrid").jqGrid({
				url : '/InterviewAssist/getQuestionSet',
				datatype : "json",
				contentType: "application/json",
				colModel : [

				{
						label: "",
                        name: "actions",
                        width: 20,
                        search : false,
                        formatter: "actions",
                        formatoptions: {
                            keys: true,
                            mtype: 'POST',
                           url : "/InterviewAssist/editQuestion",
                           
                            editOptions: {
                            	/*  serializeDelData: function(postdata) {
                            		 alert("sdsfsdfsdf");
                                     return JSON.stringify(postdata);
                                 } */
                            },
                            addOptions: {},
                            delOptions: {
                            	url : '/InterviewAssist/deleteQuestion',
                                mtype: 'POST',
                                reloadAfterSubmit: true,
                                ajaxDelOptions: {
                                    contentType: "application/json"
                                },
                                serializeDelData: function(postdata) {
                                    return JSON.stringify(postdata);
                                }
                            },
                            onSuccess:function(jqXHR) {
                            	$("#modalText").text(jqXHR.responseText);
            					$("#messageModal").modal('show');
                                return true;
                            },
                            onError:function(rowid, jqXHR, textStatus) {
                                $("#modalText").text(jqXHR.responseText);
            					$("#messageModal").modal('show');
                            }
                        },
                    },{
					label : 'QNo',
					name : 'questionNo',
					width : 30,
					key : true,
					editable : false,
					hidden : true,
					editrules : {
						required : true
					}
				}, {
					label : 'Question',
					name : 'questionDesc',
					width : 100,
					editable : false,
					searchoptions: {
						sopt : ['cn']
					}

				// must set editable to true if you want to make the field editable
				}, {
					label : 'Question Type',
					name : 'questionType',
					width : 45,
					editable : false,
					//search : false
					stype: "select",
					searchoptions: { value: ":[All];MCQ:MCQ;Programming:Programming" } 
				}, {
					label : 'Complexity',
					name : 'questionComplexity',
					width : 35,
					editable : true,
					edittype : "select",
					editoptions : {
						value : "Simple:Simple;Medium:Medium;Complex:Complex"
					},
					stype: "select",
					searchoptions: { value: ":[All];Simple:Simple;Medium:Medium;Complex:Complex" } 
				}, {
					label : 'Example',
					name : 'example',
					index : 'example',
					width : 100,
					editable : true,
					edittype : "textarea",
					editoptions : {
						rows : "3",
						cols : "60"
					},
					search : false
				}, {
					label : 'Status',
					name : 'questionActive',
					width : 15,
					editable : true,
					edittype : "select",
					editoptions : {
						value : "Y:Y;N:N"
					},
					/*stype: "select",
					searchoptions: { value: ":[All];Y:Y;N:N" }*/
					search : false
				}, {
					label : 'Time',
					name : 'questionTime',
					width : 20,
					edittype : "text",
					editable : true,
					editrules:{
						//custom rules
                        custom_func: validatePositive,
                        custom: true,
                        required: true
                    },
                    search : false,
                    hidden : true
				} ],
				sortname : 'questionType',
				sortorder : 'desc',
				loadonce : true,
				width : 1145,
				height : 210,
				multiselect : false,
				rowNum : 10,
				pager : "#jqGridPager",
				rowList : [ 10, 20, 30 ],
				view : true,
				caption : "Questions",
				onSelectRow: showDetailsGrid
				/* onSelectRow: function(rowid, selected) {
						alert(rowid);
						if(rowid != null) {
							alert("asdfasfasdf");
							$("#jqGridDetails").jqGrid('setGridParam', {
								url : '/InterviewAssist/getTestCases',
								type : "GET",
								data: rowid,
								datatype : 'JSON'
						}); // the last setting is for demo only
						$("#jqGridDetails").jqGrid('setCaption', "Question No " + ids + "Tescases").trigger("reloadGrid");
					}
				}, */
				//onSortCol : clearSelection,
				//onPaging : clearSelection
			});

			// activate the toolbar searching
            $('#jqGrid').jqGrid('filterToolbar');
			/*$('#jqGrid').jqGrid('navGrid',"#jqGridPager", {                
                search: false, // show search button on the toolbar
                add: false,
                edit: false,
                del: false,
                refresh: true
            });*/
			
			$('#jqGrid').navGrid('#jqGridPager', {
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				view : true,
				position : "left",
				closeOnEscape : true,
				cloneToTop : false
			}, {
				editCaption : "Edit Question",
				recreateForm : true,
				beforeSubmit : function(postdata, form, oper) {
					var questionNoArry = "";
					questionNoArry = $.makeArray($('#jqGrid').jqGrid('getGridParam', 'selarrrow').map(function(elem) {
						return $('#jqGrid').jqGrid('getCell', elem, 'questionNo');
					}));
					if (questionNoArry.length > 1) {
						return [ false, 'Multiple edit is not allowed.' ];
						//return [true,''];
					}

					$.ajax({
						type : "POST",
						url : "/InterviewAssist/editQuestion",
						data : JSON.stringify(postdata),
						dataType : "json",
						contentType : "application/json; charset=utf-8",
						success : function(response, textStatus, xhr) {
							jQuery('#jqGrid').jqGrid('clearGridData').jqGrid('setGridParam', {
								data : response,
								datatype : 'json'
							}).trigger('reloadGrid');
							return [ true, '', '' ];
						},
						error : function(xhr, textStatus, errorThrown) {
							$("#modalText").text("error");
        					$("#messageModal").modal('show');
						}
					});

				},
				closeAfterEdit : true,
				errorTextFormat : function(data) {
					return 'Error: ' + data.responseText
				}
			}, {
				closeAfterAdd : true,
				recreateForm : true,
				errorTextFormat : function(data) {
					return 'Error: ' + data.responseText
				}
			}, {
				beforeSubmit : function(postdata, form, oper) {
					/*var questionNoArry = "";
					$.makeArray($('#jqGrid').jqGrid('getGridParam', 'selarrrow').map(function(elem) {
						questionNoArry += $('#jqGrid').jqGrid('getCell', elem, 'questionNo') + "-";
						return questionNoArry;
					}));*/
					
					//alert("ffffffffffffffffffffff"+(JSON.stringify(postdata)));

					$.ajax({
						type : "POST",
						url : '/InterviewAssist/deleteQuestion',
						data : postdata,
						dataType : "json",
						contentType : "application/json; charset=utf-8",
						success : function(response, textStatus, xhr) {
							jQuery('#jqGrid').jqGrid('clearGridData').jqGrid('setGridParam', {
								data : response,
								datatype : 'json'
							}).trigger('reloadGrid');
							return [ true, '', '' ];
						},
						error : function(xhr, textStatus, errorThrown) {
							$("#modalText").text("error");
        					$("#messageModal").modal('show');
						}
					});
				},
				errorTextFormat : function(data) {
					return 'Error: ' + data.responseText
				}
			}).navButtonAdd('#jqGridPager', {
		        caption: "Export to Excel",
		        buttonicon: "ui-icon-disk",
		        onClickButton: function () {
		            $("#jqGrid").jqGrid("exportToExcel",{
		    			includeLabels : true,
		    			includeGroupHeader : true,
		    			includeFooter: true,
		    			fileName : "jqGridExport.xlsx",
		    			maxlength : 40 // maxlength for visible string data 
		    		});
		        }
		    });
            
		});
		
		
		
		 function showDetailsGrid(rowID,selected) {

			 
			var questionType = $('#jqGrid').jqGrid('getCell', rowID, 'questionType');
			
		if ("Programming" == questionType) {
				$("#detailsPlaceholder").empty();
				$("#detailsPlaceholder").append(
				"<table id='jqGridTestDetails'></table><div id='jqGridTestDetailsPager'></div> <input type='button' name='editTestCase' value='Edit TestCase' onclick='startTestCaseEdit()'/><input type='button' name='saveTestCase' value='Save TestCase' onclick='saveTestCaseRows()'/><input type='button' name='cancel' value='Cancel' onclick='restoreTestCaseRow()'/>");

				$("#jqGridTestDetails").jqGrid({
					url : '/InterviewAssist/getTestCases?questionNo=' + rowID ,
					//data: "41",
					datatype : "json",
					contentType : "application/json; charset=utf-8",
					colModel : [{ label : 'testCaseId',
						name : 'testCaseId',
						width : 30,
						key : true,
						editable : true,
						//hidden : true,
						editrules : {
							required : true
							
						},
						editoptions : {
							readonly : true,
							defaultValue:rowID
						}
					},{
						label : 'QNo',
						name : 'qno',
						width : 30,
						editable : false,
						hidden : false,
						editrules : {
							required : true
						}
					},{
						label : 'Test Case',
						name : 'testCase',
						width : 170,
						editable : true,
						edittype : "textarea",
						editoptions : {
							required : true,
							rows : "2",
							cols : "40"
						},
						editrules:{
						//custom rules
                       // custom_func: validatePositive,
                       // custom: true,
                        //required: true
                    	}
					}, {
						label : 'Test Case File Name Date',
						name : 'testFileName',
						width : 50,
						editable : true,
						editoptions : {
							required : true
						},
						editrules:{
						//custom rules
                        //custom_func: validatePositive,
                       // custom: true,
                        //required: true
                    	}
					}, {
						label : 'Testcase Weightage',
						name : 'testCaseWeightage',
						width : 50,
						editable : true,
						editoptions : {
							required : true
						},
						editrules:{
						//custom rules
                        custom_func: validatePositive,
                        custom: true,
                        required: true
                    	}
					} ],
					width : 1149,
					//rowNum : 5,
					loadonce : true,
					height : 350,
					viewrecords : true,
					caption : 'Question Testcase',
					pager : "#jqGridTestDetailsPager"
				});
				
				
				$('#jqGridTestDetails').navGrid('#jqGridTestDetailsPager', {
					edit : false,
					add : false,
					del : false,
					search : false,
					refresh : false,
					view : true,
					position : "left",
					cloneToTop : false
				}, {
					editCaption : "Edit Question",
					recreateForm : true,
					closeAfterEdit : true,
					errorTextFormat : function(data) {
						return 'Error: ' + data.responseText
					}
				}, {
					addCaption : "Add TestCase",
					closeAfterAdd : true,
					recreateForm : true,
					beforeSubmit : function(postdata, form, oper) {

						$.ajax({
							type : "POST",
							url : "/InterviewAssist/insertTestCase",
							data : JSON.stringify(postdata),
							dataType : "json",
							contentType : "application/json; charset=utf-8",
							success : function(response, textStatus, xhr) {
								jQuery('#jqGridTestDetails').jqGrid('clearGridData').jqGrid('setGridParam', {
									data : response,
									datatype : 'json'
								}).trigger('reloadGrid');
								return [ true, '', '' ];
							},
							error : function(xhr, textStatus, errorThrown) {
								$("#modalText").text("error");
	        					$("#messageModal").modal('show');
							}
						});

					},
					errorTextFormat : function(data) {
						return 'Error: ' + data.responseText
					}
				}, {
					errorTextFormat : function(data) {
						return 'Error: ' + data.responseText
					}
				});

			}
		
		if ("MCQ" == questionType) {
			$("#detailsPlaceholder").empty();
			$("#detailsPlaceholder").append(
					"<table id='jqGridMCQDetails'></table><div id='jqGridMCQDetailsPager'></div> <input type='button' name='editMCQ' value='Edit MCQ' onclick='startEdit()'/><input type='button' name='saveMCQ' value='Save MCQ' onclick='saveRows()'/><input type='button' name='cancel' value='Cancel' onclick='restoreRow()'/> ");

			$("#jqGridMCQDetails").jqGrid({
				url : '/InterviewAssist/getMCQDetails?questionNo=' + rowID +'',
				//data: "41",
				type : "GET",
				datatype : "json",
				contentType : "application/json; charset=utf-8",
				colModel : [{ label : 'MCQ ID',
					name : 'mcqID',
					width : 30,
					key : true,
					hidden : false
					
				}, { label : 'QNo',
					name : 'qno',
					width : 30,
					editable : true,
					hidden : true,
					editrules : {
						required : true
						
					}
				}, { label : 'Answer ID',
					name : 'answerId',
					width : 30,
					editable : true,
					hidden : true,
					editrules : {
						required : true
						
					}
				}, {
					label : 'Option',
					name : 'optionTxt',
					editable : true,
					//edittype : "textarea",
					editrules : {
						required : true
						
					},
					editoptions : {
						//	readonly : true,
						defaultValue:20
					},
					width : 40
				}, {
					label : 'Answer',
					name : 'optionAns',
					editable : true,
					//edittype : "radio",
					//hidden : true,
					formatter: 'checkbox',
					edittype : 'checkbox',
					width : 40
				}],
				width : 1149,
				rowNum : 5,
				loadonce : true,
				height : '100',
				viewrecords : true,
				caption : 'Multiple choice',
				pager : "#jqGridMCQDetailsPager"
			});
			
			
		}

		
		}
		 
		 
		 function saveRows() {
	            var grid = $("#jqGridMCQDetails");
	            var ids = grid.jqGrid('getDataIDs');
	            
	           $("#jqGridMCQDetails  input").each(function() {
	            	if("" == $(this).val())
	            		$(this).val("empty");
				});
	            
	            for (var i = 0; i < ids.length; i++) {
	                grid.jqGrid('saveRow', ids[i]);
	            }
	            var gridData = $("#jqGridMCQDetails").getRowData();
	            
	            $.ajax({
					type : "POST",
					url : "/InterviewAssist/saveMCQDetails",
					data :JSON.stringify(gridData),
					dataType : "json",
					contentType : "application/json; charset=utf-8",
					success : function(response, textStatus, xhr) {
						jQuery('#jqGridMCQDetails').jqGrid('clearGridData').jqGrid('setGridParam', {
							data : response,
							datatype : 'json'
						}).trigger('reloadGrid');
						return [ true, '', '' ];
					},
					error : function(xhr, textStatus, errorThrown) {
						$("#modalText").text("error");
    					$("#messageModal").modal('show');
					}
				});

	        }
		 
		 function startEdit() {
	            var grid = $("#jqGridMCQDetails");
	            var ids = grid.jqGrid('getDataIDs');
	            $("#jqGridMCQDetails").addRowData("Codice", {}, 'last');

	            for (var i = 0; i < ids.length+1; i++) {
	            	
	                grid.jqGrid('editRow',ids[i]);
	            }
	        };
	        
	        function restoreRow() {
	            var grid = $("#jqGridMCQDetails");
	            var ids = grid.jqGrid('getDataIDs');

	            for (var i = 0; i < ids.length; i++) {
	            	
	                grid.jqGrid('restoreRow',ids[i]);
	            }
	        };
	        
	        function saveTestCaseRows() {
	            var grid = $("#jqGridTestDetails");
	            var ids = grid.jqGrid('getDataIDs');
	           $("#jqGridTestDetails  input").each(function() {
	            	if("" == $(this).val())
	            	{
	            		$(this).val("empty");
	            	}
	            	if (isNaN($(this).val())) {
	            		return false; 
					} 
				});
	            
	            for (var i = 0; i < ids.length; i++) {
	                grid.jqGrid('saveRow', ids[i]);
	            }
	            var gridData = $("#jqGridTestDetails").jqGrid('getGridParam', 'data');
	            var count = 0;
	            for (j = 0; j < gridData.length; j++) {
            		if (jQuery.parseJSON(JSON.stringify(gridData[j])).testCaseId == 0) {
            			if (jQuery.parseJSON(JSON.stringify(gridData[j])).testCase != null && jQuery.parseJSON(JSON.stringify(gridData[j])).testFileName != null) {
            				count = count + parseInt(jQuery.parseJSON(JSON.stringify(gridData[j])).testCaseWeightage);
            			}
            			
					} else {
		            	count = count + parseInt(jQuery.parseJSON(JSON.stringify(gridData[j])).testCaseWeightage);
					}
	            }
	            
	            if (count == 100) {
	            	$.ajax({
						type : "POST",
						url : "/InterviewAssist/insertTestCase",
						data :JSON.stringify(gridData),
						dataType : "json",
						contentType : "application/json; charset=utf-8",
						success : function(response, textStatus, xhr) {
							$("#modalText").text(xhr.responseText);
        					$("#messageModal").modal('show');
							jQuery('#jqGridTestDetails').jqGrid('clearGridData').jqGrid('setGridParam', {
								data : response,
								datatype : 'json'
							}).trigger('reloadGrid');
							return [ true, '', '' ];
						},
						error : function(xhr, textStatus, errorThrown) {
							$("#modalText").text("error");
							$("#messageModal").modal('show');
						}
					}); 
				} else if(count < 100 || count > 100) { 
					$("#modalText").text("Sum of all testcase weightage should be 100");
					$("#messageModal").modal('show');
				}
	        }
		 
		 function startTestCaseEdit() {
	            var grid = $("#jqGridTestDetails");
	            var ids = grid.jqGrid('getDataIDs');
	            
	            
	            $("#jqGridMCQDetails").addRowData("Codice", {}, 'last');
	            
	            for (var i = 0; i < ids.length+1; i++) {
	            	
	                grid.jqGrid('editRow',ids[i]);
	            }
	            
	            /* var currentValue = grid.getGridParam('rowNum');
	            grid.setGridParam({rowNum:currentValue+10}); */
	        };
	        
	        function restoreTestCaseRow() {
	            var grid = $("#jqGridTestDetails");
	            var ids = grid.jqGrid('getDataIDs');

	            for (var i = 0; i < ids.length; i++) {
	            	
	                grid.jqGrid('restoreRow',ids[i]);
	            }
	        };
	        
		 
		 function radioButtonFormatter(cellValue, options, rowObject) {
			 
			 var radioHtml ="";
			 if('Single'== rowObject.optionType){
				 if(cellValue ==""){
					 radioHtml = '<input type="radio" id="optionAns'+rowObject.mcqID+'" value="" name="optionAns" disabled="disabled"></input>';
				 }else{
					 radioHtml = '<input type="radio" id="optionAns'+rowObject.mcqID+'" value="" name="optionAns" checked=checked disabled="disabled"></input>'
				 }
			 
			 }
			 
			 if('Multiple'==rowObject.optionType){
				 if(cellValue ==""){
					 radioHtml = '<input type="checkbox" id="optionAns'+rowObject.mcqID+'" name="optionAns" value="" disabled="disabled"/>';
				 }else{
					 radioHtml = '<input type="checkbox" id="optionAns'+rowObject.mcqID+'" name="optionAns" checked=checked value="" disabled="disabled"/>'
				 }
			 }
			   
			 return radioHtml;
		}
		 
		 function addRadioOrCheckBox() {
			 var radioHtml ="";
			/// var optionType = $('#jqGridMCQDetails').jqGrid('getCell', 2, 'optionType');
			
			 var radioHtml ="";
			 if('Single'== $("#optionTypeHid").val()){
					 radioHtml = '<input type="radio" id="optionAns'+rowObject.mcqID+'"value="" name="optionAns"></input>';
			 
			 }
			 
			 if('Multiple'==$("#optionTypeHid").val()){
					 radioHtml = '<input type="checkbox" id="optionAns'+rowObject.mcqID+'" name="optionAns" value="" />';
			 }
			   
			 return radioHtml;
		}
		 
		 function setHiddenVal(cellValue, options, rowObject){
			 var radioHtml = '<input type="hidden" value='+cellValue+' name="optionTypeHid" id ="optionTypeHid" ></input>';
			 return radioHtml;
		 }

		function validatePositive(value, column) {
			if (isNaN(value)) {
				return [ false, "Please enter a positive value" ];
			} else {
				return [ true, "" ];
			}
		}

		function clearSelection() {
			jQuery("#jqGridTestDetails").jqGrid('setGridParam', {
				url : "empty.json",
				datatype : 'json'
			}); // the last setting is for demo purpose only
			jQuery("#jqGridTestDetails").jqGrid('setCaption', 'Detail Grid:: none');
			jQuery("#jqGridTestDetails").trigger("reloadGrid");

		}
	</script>

</body>
</html>