<%@page session="false"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Interview Login Page</title>

<c:url var="home" value="/" scope="request" />
<meta charset='utf-8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/display.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/formElement.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/jquery-ui.css">
<script src="${pageContext.request.contextPath}/resources/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/jquery-ui.js"></script>

<script>
	$(document)
			.ready(
					function() {
						$(".loader").hide();
						$("#thankUID").hide();
						$("#starttest")
						.click(
								function() {
										
										var querystring = '/InterviewAssist/starttest?action='
												+ $("#action").val()
												+ '&event='
												+ $("#event").val()
												+ '&name='
												+ $("#name").val()
												+ '&email='
												+ $("#email").val()
												+ '&phone='
												+ $("#phone").val();

										openPopUpWindow(querystring);

										$(".row1").hide();
										//$("#instructionID").hide();
										$(".loader").show();
									
								});
					});
	
	function openPopUpWindow(querystring) {

		var wind_width = screen.availWidth;
		if (wind_width == undefined || wind_width == screen.width)
			wind_width = screen.width;

		var wind_height = screen.availHeight;
		if (wind_height == undefined || wind_height == screen.height)
			wind_height = screen.height;// a random number

		var params = 'width=' + wind_width + ', height=' + wind_height
				+ ', top=0, left=0';
		win = window.open(querystring, "IEStyleFullScreen", params
				+ ", scrollbars, status=no");
		
		}
	
	if (window.history && window.history.pushState) {

	    $(window).on('popstate', function() {
	      var hashLocation = location.hash;
	      //alert(hashLocation);
	      var hashSplit = hashLocation.split("#!/");
	      var hashName = hashSplit[1];

	      if (hashName !== '') {
	        var hash = window.location.hash;
	        //alert(hashName+"    "+hash);
	        if (hash === '') {
	        	
	        	 if(win.calledFromParent()){ 
	        		 win.focus();
	            //window.location='/InterviewAssist/resources/Test.html';
	             var querystring = '/InterviewAssist/event?event='
					+ $("#event").val();
				
	            window.location=querystring;
	            return false;
	        	 } else {
	        		 
	        		 window.history.pushState('forward', null, './#forward');
	        	 }
	        }
	      }
	    });

	    window.history.pushState('forward', null, './#forward');
	  }
	
	window.addEventListener("beforeunload", function (e) { 
		 
		 var querystring = './instructions?event='
				+ $("#event").val()
				+ '&name='
				+ $("#name").val()
				+ '&email='
				+ $("#email").val()
				+ '&phone='
				+ $("#phone").val();
		 window.history.pushState('forward', null, querystring);
		   //window.location="/InterviewAssist/event?event=Test";
	     return;
	   });
	
</script>
</head>

<body >

	<header>
		<div class="container">
			<img src="/InterviewAssist/resources/logo.png" alt="logo" />
		</div>
	</header>


	<h1 align="center" class="htext">
		<font face=Calibri>Welcome to CenturyLink</font>
	</h1>

	<section class="main-content">
		<div class="container">
			<form:form modelAttribute="candidateForm" id="instructions" method="GET">

				<div class="row1">


					<%--<form id="loginForm" method="post" action="<c:url value='/validate' />" enctype="application/x-www-form-urlencoded"> --%>

					<!-- <h1>Test Instructions</h1> -->


					<div class="col1">
						<div id="instructionID">
							<h1>Test Instructions</h1>
							<div class="form-row-login  steps-blok">

								<h3>
									Step <span>1</span>
								</h3>
								<p>Do Not Use the "Back" Button on Your Browser During the
									Test once you have begun taking the quiz. Instead, use the
									scroll bar to move back to check earlier questions. Don't close
									the window of the test for any reason.</p>

							</div>
							<div class="form-row-login  steps-blok">

								<h3>
									Step <span>2</span>
								</h3>
								<p>Review All of Your Answers Before Submitting the Quiz.
									Make sure you have not accidentally changed your response to a
									question or made a typographic mistake.</p>

							</div>
							<div class="form-row-login  steps-blok">

								<h3>
									Step <span>3</span>
								</h3>
								<p>Any programming question will not be evaluated until you
									press Run Code button. Please don't modify skeleton code.</p>

							</div>
							<div class="form-row-login  steps-blok">

								<h3>
									Step <span>4</span>
								</h3>
								<p>When you are satisfied with the result make sure you
									finish by clicking the SUBMIT button at the bottom of the
									screen. Click the Submit Button ONLY ONCE! After you submit the
									test answers, you will receive a score unless you have exceeded
									the time limit for the quiz.</p>

							</div>

							<div class="form-row-login note2">

								<h3>Note:</h3>
								<p>If accidently test is terminated, please start the test
									with same details as you used while login the test will resume
									from same point.</p>

							</div>


							<div class="form-row-login">
								<input type="button" name="starttest" id="starttest"
									value="Start Test" />

							</div>

							<input type="hidden" id="event" name="event"
								value="<c:out value="${candidateForm.event}"/>" /> <input
								type="hidden" id="name" name="name"
								value="<c:out value="${candidateForm.name}"/>" /> <input
								type="hidden" id="email" name="email"
								value="<c:out value="${candidateForm.email}"/>" /> <input
								type="hidden" id="phone" name="phone"
								value="<c:out value="${candidateForm.phone}"/>" /> <input
								type="hidden" id="action" name="action" value="starttest" /> 
						</div>
					</div>
					<div class="col2">
						<img src="/InterviewAssist/resources/logo.png" alt="logo" />
					</div>
				</div>

				<div class="loader">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>

				<div id="thankUID">
					<div>Thank you for attending the exam</div>
				</div>
			</form:form>
		</div>
	</section>

	<footer>
		<div id="footer">2017 CenturyLink India, Inc. All Rights
			Reserved.</div>
	</footer>



</body>

</html>