<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<script src="${pageContext.request.contextPath}/resources/jquery.datetimepicker.full.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/jquery.datetimepicker.min.css">
</head>

<style>
.ErrorField {
	border-color: #D00;
	color: #D00;
	background: #FFFFFE;
}

span.ValidationErrors {
	display: block !important;
	min-height: 19px;
	font-size: 12px;
	color: #D00;
	padding-left: 10px;
	font-family: sans-serif;
}
</style>
<script>


function isNotNull(test) {
	if (test != null && test != '') {
		return true;
	} else {
		return false;
	}
}

function reset(){
	alert("dddd");
	//$("#addEventForm").trigger("reset");
}

$(function() {
	
	$.datetimepicker.setLocale('en');
	$("#applicabilityID").selectmenu();
	$("#eventTechnologyID").selectmenu();
	$("#eventStatusID").selectmenu();
	var dateToday = new Date(); 
	$('#startDT').datetimepicker({
		numberOfMonths: 3,
        showButtonPanel: true,
        minDate: dateToday
	});
	
	$('#endDT').datetimepicker({
		numberOfMonths: 3,
        showButtonPanel: true,
        minDate: dateToday
	});

	});
	
	
	$(document).ready(function() {
		
		
		$("#resetID").click(function() {
			$("div.error").hide();
		});
		
		
		$("#addEventID").click(function() {
			$("div.error").hide();
			var returnVal = new Boolean(true);
			
			 if (!(isNotNull($('#startDT').val())) || !(isNotNull($('#endDT').val())) || !(isNotNull($('#eventNameID').val()))
					|| !(isNotNull($('#applicabilityID').val())) || !(isNotNull($('#eventTechnologyID').val()))) {
				$("div.error").show();
				$("div.error").focus();
				$("div.error span").html("Please enter all the mandatory * marked field.");
				returnVal = false;
				return returnVal;
			}
			
			$("#mcqDiv input, #programDiv input").each(
					function() {
						if (isNotNull($(this).val()) && !($.isNumeric($(this).val())) ) {
							$("div.error").show();
							$("div.error").focus();
							$(this).focus();
							$("div.error span").html(
									"Please enter the "+$(this).attr("title")+" in numeric.");
							returnVal = false;
							return returnVal;
						}
			});
			
			if(!($.isNumeric($('#eventDurationID').val()))){
				$("div.error").show();
				$("div.error").focus();
				$('#eventDurationID').focus();
				$("div.error span").html("Please enter event duration in minutes.");
				returnVal = false;
				return returnVal;
			}
			
			
			
			if (returnVal) {
				var dataString = JSON.stringify($('#addEventForm').serializeObject());
				$.ajax({
					type : "POST",
					url : "/InterviewAssist/addEvent",
					data : dataString,
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					success : function(data) {
						if (data.indexOf("Error") != -1) {
							$("div.error").show();
							$("div.error").focus();
							$("div.error span").html(data);
							returnVal = false;
							return returnVal;
						} else {
							$("#addEventForm").trigger("reset");
						}
					},
					failure : function(errMsg) {
						alert(errMsg);//handle it in a proper way
					}
				});
			}
		});

	});
</script>

<form action="#" id="addEventForm">
<div class="headerDIV">
	ADD EVENT
</div>

<div class="error" tabindex="1" style="display:none;color: red;margin-top: 10px;">
	<img src="/InterviewAssist/resources/warning.gif" alt="Warning!" width="24" height="24" style="float:left; margin: -5px 10px 0px 0px;">
	<span></span>
	<br clear="all">
</div>
		<!-- <h3 style="margin-bottom: 2px;">Add Event</h3> -->
		<div id="eventSection" style="margin-left: 50px;">

			<div class="form-row">
				<label class="levels" >Event Name<span class="required">*</span></label> 
				<input type="text" name="eventName" id="eventNameID" size="31"  title="Event Name"/>
			</div>
			
			<div class="form-row">
				<label class="levels">Start Date<span class="required">*</span></label> 
				<input type="text" name="eventStartDate" id="startDT"  size="31"  />
			</div>
			<div class="form-row">
				<label class="levels">End Date<span class="required">*</span></label> 
				<input type="text" name="eventEndDate" id="endDT" size="31"  />
			</div>
			
			<div class="emptyDIV"></div>
			<div id="mcqDiv">
				<div class="form-row">
					<label class="levels">Simple MCQ</label> 
					<input type="text" name="simpleMcq" id="simpleMcqID" size="31"  title="Simple MCQ" />
				</div>
				
				<div class="form-row">
					<label class="levels">Medium MCQ</label> 
					<input type="text" name="mediumMcq" id="mediumMcqID"  size="31"  title="Medium MCQ" />
				</div>
				<div class="form-row">
					<label class="levels">Complex MCQ</label> 
					<input type="text" name="complexMcq" id="complexMcqID"  size="31"  title="Complex MCQ" />
				</div>
			</div>
			<div class="emptyDIV"></div>
			<div id="programDiv">
				<div class="form-row">
					<label class="levels">Simple Program</label> 
					<input type="text" name="simplePgm" id="simplePgmID" size="31"  title="Simple Program"/>
				</div>
				
				<div class="form-row">
					<label class="levels">Medium Program</label> 
					<input type="text" name="mediumPgm" value="" id="mediumPgmID"  size="31"  title="Medium Program"/>
				</div>
				<div class="form-row">
					<label class="levels">Complex Program</label> 
					<input type="text" name="complexPgm" id="complexPgmID"  size="31"   title="Complex Program"/>
				</div>
			</div>
			<div class="emptyDIV"></div>
			<div class="form-row">
				<label class="levels">Event Duration (in minutes)<span class="required">*</span></label> 
				<input type="text" name="eventDuration" id="eventDurationID" size="31"  title="Event Duration"//>
			</div>
			
			
			
			<div class="form-row">
				<label class="levels">Applicability<span class="required">*</span></label>
				
				<select name="applicability"  id="applicabilityID" class="drop_down">
				 	<option value=""></option>
				    <c:forEach items="${roleLST}" var="roleVal" varStatus="status">
				        <option value="${roleVal.role}"><c:out value="${roleVal.role}" /></option>
				    </c:forEach>
				</select>
			</div>
			
			<div class="form-row">
				<label class="levels">Technology<span class="required">*</span></label> <select name="eventTechnology" id="eventTechnologyID" class="drop_down">
					<option></option>
					<option>Java</option>
					<option>C</option>
					<option>C++</option>
					<option>C#</option>
					<option>.net</option>
					<option>AngularJS</option>
					<option>JavaScript</option>
				</select>
			</div>
			
			<%--<div class="form-row">
				<label class="levels">Status &nbsp; <span class="required">*</span></label> <select name="eventStatus" id="eventStatusID" class="drop_down">
					<option></option>
					<option>ACTIVE</option>
					<option>IN-ACTIVE</option>
				</select>
			</div>  --%>
			
			<div class="emptyDIV"></div>

	<div>
		<input type="button" name="addEvent" value="Add Event" id="addEventID" /> 
		<input type="reset" name="cancel" value="Reset"  id="resetID"/>
	</div>

</form>


<script>

$.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
 	
</script>

</html>