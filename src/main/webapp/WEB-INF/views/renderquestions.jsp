<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!doctype html>

<html lang=''>
<head>
<meta charset='utf-8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css">


<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/interviewTool.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/formElement.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/font-awesome.min.css">
	
	
<script src="${pageContext.request.contextPath}/resources/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/script.js"></script>
<script src="${pageContext.request.contextPath}/resources/ace.js"
	type="text/javascript" charset="utf-8"></script>
<script>

function calledFromParent(){
	 if (confirm('The data you have entered may not be saved. Are you sure you want to exit the Test?')) {
		 	//alert("Confirm yes");
			self.close();
			return true;
		} else {
			//alert("Confirm No");
			return false;
		}
 } 
   function isEmpty(value){
		  return (value == null || value.length === 0);
		}
 $(document).ready(function(){
	 
	 

		 $('#mainBody').bind('cut copy paste', function (e) 
		 	 	{
		 	  	e.preventDefault();
		 	 });

		 	 $("#mainBody").on("contextmenu",function(e)
		 	 {
		 	  return false;
		 	 }); 
 
		
		var progCode= document.getElementById("Progcode").value;
		
		if(!isEmpty(progCode))
		{
			var editor = ace.edit("editor");
			editor.onPaste = function() { return ""; }
			editor.getSession().setValue(progCode);	
		}
		
			
		
		
		$("#compile").click(function(){
			 $(this).data('clicked', true);
			 var editor = ace.edit("editor");
			 $("#programCode").val(editor.getSession().getValue());
			$("#action").val("compile");
	    });
		$("#validate").click(function(){
			 $(this).data('clicked', true);
			var editor = ace.edit("editor");
			 $("#programCode").val(editor.getSession().getValue());
			$("#action").val("validate");
	    });

		$("#next").click(function(){
			$(this).data('clicked', true);
			if($("#Progcode").val() != '') {
				var editor = ace.edit("editor");
				$("#programCode").val(editor.getSession().getValue());
			}
			$("#action").val("next");
	    });

		$("#previous").click(function(){
			 $(this).data('clicked', true);
			 if($("#Progcode").val() != '') {
					var editor = ace.edit("editor");
					$("#programCode").val(editor.getSession().getValue());
			 }
			 $("#action").val("previous");
	    });
		
		$("#exit").click(function(){
			
			$(this).data('clicked', true);
			$("#action").val("exit");
			
			 if (confirm('Are you sure you are done with your test? Please review all your answer before existing the test.')) {
				 //window.opener.document.getElementById("ballsWaveG").style.display='none';
				 //window.opener.document.getElementById("thankUID").style.display="block";
				 alert("Thank you for attending the Test");
				 
				 var querystring = '/InterviewAssist/event?event='+ $("#event").val();
				 
				 opener.window.location = querystring;
				 self.close();
				 return true;
			 }else{
				 return false;
			 }
	    });
	}); 
 

   
/*    if (window.history && window.history.pushState) {

       $(window).on('popstate', function() {
         var hashLocation = location.hash;
         alert(hashLocation);
         var hashSplit = hashLocation.split("#!/");
         var hashName = hashSplit[1];

         if (hashName !== '') {
           var hash = window.location.hash;
           alert(hashName+"    "+hash);
           if (hash === '') {
             $.ajax({
                            type: "POST",
                            url: "/InterviewAssist/renderquestions?action=EXIT",
                            async: true           
                        }); 
               window.location='/InterviewAssist/resources/Test.html';
               return false;
           }
         }
       });

       window.history.pushState('forward', null, './#forward');
     } */
   
     $(window).on('mouseover', (function () {
		    window.onbeforeunload = null;
		}));
		$(window).on('mouseout', (function () {
		    window.onbeforeunload = ConfirmLeave;
		}));
		function ConfirmLeave() {
			 $("#candidateForm").attr("action", "/InterviewAssist/renderquestions?action=SAVE");
             $( "#candidateForm" ).submit();
		    return "";
		}
		var prevKey="";
		$(document).keydown(function (e) {            
		    if (e.key=="F5") {
		        window.onbeforeunload = ConfirmLeave;
		    }
		    else if (e.key.toUpperCase() == "W" && prevKey == "CONTROL") {                
		        window.onbeforeunload = ConfirmLeave;   
		    }
		    else if (e.key.toUpperCase() == "R" && prevKey == "CONTROL") {
		        window.onbeforeunload = ConfirmLeave;
		    }
		    else if (e.key.toUpperCase() == "F4" && (prevKey == "ALT" || prevKey == "CONTROL")) {
		        window.onbeforeunload = ConfirmLeave;
		    }
		    prevKey = e.key.toUpperCase();
		});	
 
</script>
<title>InCTLI</title>


</head>
<body onload="clock();" onkeydown="return (event.keyCode != 116)" id="mainBody">

<section class="main-content">
	<form:form action="renderquestions" modelAttribute="candidateForm">
	<header>
		<div class="container">
		<div class="logo">
		<img src="/InterviewAssist/resources/logo.png" alt="logo" /></div>
		    <div class="form-row-time" >
		    	<table>
		    		<tr>
		    			<td align="left"><b>Remaining Time:-</b></td>
		    			<td align="left"><span id="counter"
					style="color: #cc0000;">?</span></td>
		    		</tr>
		    		<tr>
		    			<td align="left"><b>Total Questions:-</b></td>
		    			<td align="left"><font color="#cc0000" ><c:out value="${totalquestions}" /></font></td>
		    		</tr>
		    	</table>
			</div>
			
			<div class="emptyDIV" id="infoMessage"></div>
			<div class="form-row-name">
				Welcome,&nbsp;<c:out value="${name}" />&nbsp;&nbsp;<span id="infoMessage"
					style="color: #cc0000;"></span>
			</div>
		</div>
		</header>
		
		<section><div class="container">
		<div class="error"
			style="display: none; color: red; margin-top: 10px;">
			<img src="/InterviewAssist/resources/warning.gif" alt="Warning!"
				width="24" height="24"
				style="float: left; margin: -5px 10px 0px 0px;"> <span></span>
			<br clear="all">
		</div>



		<div class="emptyDIV"></div>
		<div class="question-main">
			<h3>
				Question # <c:out value="${candidateForm.displayIndex}" />
				<div class="exit"><input type="submit" name="exit" value="Exit" id="exit" /></div>
			</h3>
			
		</div>
		<div class="form-row question">
		<!-- 	<div class="question"> -->
						
						<c:out value="${candidateForm.displayQuestion.questionDesc}" />
				<%-- <c:out value="${candidateForm.optionType}"/>&nbsp; 
				 	<c:out value="${candidateForm.currentIndex}" />&nbsp; 
				 	<c:out value="${candidateForm.lastQuestion}" />&nbsp;  --%>
				<br>
				<c:out value="${candidateForm.displayQuestion.example}" />	
			

			<c:if
				test="${candidateForm.displayQuestion.questionType != 'Programming'}">
				<c:if test="${candidateForm.optionType == 'Multiple' }">
					<div id="multipleChoice" class="check">
						<!-- <label class="levels">Applicability <span class="required">*</span></label> -->

						<c:forEach items="${candidateForm.options}" var="answer" varStatus="status">
							<div  class="check1">
							<input type="checkbox" id="checkBox_${status.index}" name="answerOptions" value="${status.count}" <c:out value='${answer.selected}' /> />
							<c:out value='${answer.option}' />
							</div>
						</c:forEach>

					</div>
				</c:if>

				<c:if test="${candidateForm.optionType == 'Single' }">
					<div id="singleChoice" class="check">
						<!-- <label class="levels">Option Type &nbsp; <span
							class="required">*</span></label> -->

						<c:forEach items="${candidateForm.options}" var="answer" varStatus="status">
							<div  class="check1">
							<input type="radio" id="radio_${status.index}"	name="answerOptions" value="${status.count}" <c:out value='${answer.selected}' /> />
							<c:out value='${answer.option}' />
							</div>
						</c:forEach>

						<!-- <input type="radio" id="singleChoiceAns" name="singleChoiceAns" value="1" />Value 1 <br>
						<input type="radio" id="singleChoiceAns" name="singleChoiceAns" value="1" />Value 2 -->
					</div>
				</c:if>
			</c:if>

		</div>

		<c:if
			test="${candidateForm.displayQuestion.questionType == 'Programming'}">
			<div id="editor"
				style="overflow: scroll; position: relative; height: 450px; width: 730px; margin-top: 15px;"></div>
			<script type="text/javascript">
					var editor = ace.edit("editor");
				    editor.setTheme("ace/theme/textmate");
				    editor.session.setMode("ace/mode/java");
				</script>
		</c:if>

		<c:if
			test="${candidateForm.displayQuestion.questionType == 'MCQ'}">
			<div id="editor"
				style="height: 200px; width: 730px; margin-top: 15px;"></div>
		</c:if>

		
			<c:if
				test="${CompilationMessage != null && candidateForm.displayQuestion.questionType == 'Programming' && CompilationMessage != 'SUCCESS'}">
				<div>
				<label style="color: red; font-weight: bold;">Compilation
					Message:- <c:out value="${CompilationMessage}" />
				</label>
				</div>
			</c:if>

		
			<c:if
				test="${CompilationMessage != null && candidateForm.displayQuestion.questionType == 'Programming' && CompilationMessage == 'SUCCESS'}">
				<div>
				<label style="color: green; font-weight: bold;">Compilation
					Message:- <c:out value="${CompilationMessage}" />
				</label>
				</div>
			</c:if>

			<c:if
				test="${ValidationMessage != null && candidateForm.displayQuestion.questionType == 'Programming'}">
				<div>
				<label style="color: green; font-weight: bold;">Message:- <c:out
						value="${ValidationMessage}" />
				</label>
				</div>
			</c:if>

		<br>
		<div>
			<!-- <input type="button" name="addQuestion" value="Add Question" id="addQuestion" /> 
		<input type="submit" name="cancel" value="Cancel" /> -->

			
			<c:if test="${candidateForm.currentIndex != 0}">
				<input type="submit" name="previous" value="Previous" id="previous" />
			</c:if>
			<c:if test="${candidateForm.lastQuestion != true}">
				<input type="submit" name="next" value="Next" id="next" />
			</c:if>
			<c:if
				test="${candidateForm.displayQuestion.questionType == 'Programming'}">
				<input type="submit" name="compile" value="Compile" id="compile" />
				<input type="submit" name="validate" value="Run Code" id="validate" />
			</c:if>
			

		</div>

		<input type="hidden" name="answer" id="answer" />
		<input type="hidden" id="action" name="action" />
		<input type="hidden" id="programCode" name="programCode" />
		<input type="hidden" id="currentIndex" name="currentIndex"
			value="<c:out value="${candidateForm.currentIndex}"/>" />
		<input type="hidden" id="Progcode" name="Progcode"
			value="<c:out value="${candidateForm.displayQuestion.templateCode}"/>" />
			
		<input type="hidden" id="event" name="event" value="<c:out value="${candidateForm.event}"/>" />
		  




		<script>
    /* var editor = ace.edit("editor");
    editor.setTheme("ace/theme/textmate");
    editor.session.setMode("ace/mode/java"); */
    
    
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
 	
</script>

</div></section>
	</form:form>

	<script type="text/javascript">
                
             function uncheckAll(){
                 /*  var elLength = document.getElementById("_id12").elements.length;
                     for (i=0; i<elLength; i++)
                     {
                         var type = document.getElementById("_id12").elements[i].type;
                         if (type=="checkbox"){
                             document.getElementById("_id12").elements[i].checked = false;
                         }
                     } */
             }
            
             ////////// CONFIGURE THE COUNTDOWN SCRIPT HERE //////////////////

            var month = '0';     //  '*' for next month, '0' for this month or 1 through 12 for the month 
            var day = '+0';        //  Offset for day of month day or + day  
            var hour = 20;        //  0 through 23 for the hours of the day
            var tz = +5.5;          //  Offset for your timezone in hours from UTC
            var lab = 'counter';      //  The id of the page entry where the timezone countdown is to show
            var minutes = '20';
            var secs = '10';
            var availableMins = '10';
            function start() {
                displayTZCountDown(setTZCountDown(month,day,hour,tz,minutes,secs),lab);
            }

            // **    The start function can be changed if required   **
            //window.onload = start;

            function setTZCountDown(month,day,hour,tz,minutes,secs){

                var toDate = new Date();
                if (month == '*')toDate.setMonth(toDate.getMonth() + 1);
                else if (month > 0) 
                { 
                if (month <= toDate.getMonth())toDate.setYear(toDate.getYear() + 1);
                toDate.setMonth(month-1);
                }
                if (day.substr(0,1) == '+') 
                {var day1 = parseInt(day.substr(1));
                toDate.setDate(toDate.getDate()+day1);
                } 
                else{toDate.setDate(day);
                }
                toDate.setHours(hour);
                toDate.setMinutes(minutes + availableMins);
                toDate.setSeconds(secs);
                
                var fromDate = new Date();
                var diffDate = new Date(0);
                
                /*var examEndTime = new Date();
                examEndTime.setHours(0);
                examEndTime.setMinutes(0);
                examEndTime.setSeconds(0);
                
                if(examEndTime < toDate){
                    diffDate.setMilliseconds(examEndTime - fromDate);
                }else{*/
                    diffDate.setMilliseconds(toDate - fromDate);
                //}
                
                return Math.floor(diffDate.valueOf()/1000);
            }

            function displayTZCountDown(countdown,tzcd) {

			totalQuestionsCount =2;

                if (countdown < 0) {
                    document.getElementById(tzcd).innerHTML = "Sorry, Time Over."; 
                    //alert("Sorry, Times Up!!");
                    // submit the form and log him out also
                    process();
                    return;
                }else {
                    var secs = countdown % 60; 
                }
                if (secs < 10) {
                    secs = '0'+secs;
                }
                var countdown1 = (countdown - secs) / 60;
                var mins = countdown1 % 60; 
                if (mins < 10) {
                    mins = '0'+mins;
                }
                countdown1 = (countdown1 - mins) / 60;
                var hours = countdown1 % 24;
                var days = (countdown1 - hours) / 24;
                document.getElementById(tzcd).innerHTML = hours+ 'h : ' +mins+ 'm : '+secs+'s';
             
               var remMins = Math.round(totalQuestionsCount* 0.3)-1; 
               var remMins1 = Math.round(totalQuestionsCount* 0.3);
              
               if(remMins >=1){
                 if(mins <= remMins && mins >= 1 ){
                     document.getElementById('infoMessage').innerHTML = 'Please hurry to complete the test!';
                    }
               }
                if(mins == 0){
                    document.getElementById('infoMessage').innerHTML = 'You have less than a minute to complete the test!!'; 
                   
                } 
                setTimeout('displayTZCountDown('+(countdown-1)+',\''+tzcd+'\');',1000);
            }

         </script>

	<script type="text/javascript">
            <!-- Standard Scroll Clock by kurt.grigg@virgin.net -->
            var H='....';
            var H=H.split('');
            var M='.....';
            var M=M.split('');
            var S='......';
            var S=S.split('');
            var Ypos=0;
            var Xpos=0;
            var Ybase=8;
            var Xbase=8;
            var dots=12;

            function clock(){
            var time=new Date ();
            var secs=time.getSeconds();
            var sec=-1.57 + Math.PI * secs/30;
            var mins=time.getMinutes();
            var min=-1.57 + Math.PI * mins/30;
            var hr=time.getHours();
            var hrs=-1.57 + Math.PI * hr/6 + Math.PI*parseInt(time.getMinutes())/360;
            for (i=0; i < dots; ++i){
            document.getElementById("dig" + (i+1)).style.top=0-15+40*Math.sin(-0.49+dots+i/1.9).toString() + "px";
            document.getElementById("dig" + (i+1)).style.left=0-14+40*Math.cos(-0.49+dots+i/1.9).toString() + "px";
            }
            for (i=0; i < S.length; i++){
            document.getElementById("sec" + (i+1)).style.top =Ypos+i*Ybase*Math.sin(sec).toString() + "px";
            document.getElementById("sec" + (i+1)).style.left=Xpos+i*Xbase*Math.cos(sec).toString() + "px";
            }
            for (i=0; i < M.length; i++){
            document.getElementById("min" + (i+1)).style.top =Ypos+i*Ybase*Math.sin(min).toString() + "px";
            document.getElementById("min" + (i+1)).style.left=Xpos+i*Xbase*Math.cos(min).toString() + "px";
            }
            for (i=0; i < H.length; i++){
            document.getElementById("hour" + (i+1)).style.top =Ypos+i*Ybase*Math.sin(hrs).toString() + "px";
            document.getElementById("hour" + (i+1)).style.left=Xpos+i*Xbase*Math.cos(hrs).toString() + "px";
            } 
            setTimeout('clock()',50);
            }
            
            function process(){
                alert("Sorry, The Time is over.\n" + "Please wait, your score is being calculated.");
                // save and take him to results page
                //anonymousProcess('hiddenLink');  
                
               /*  $.ajax({
                    type: "POST",
                    url: "/InterviewAssist/logoffUser",
                    async: false           
                }); */
                
                $("#candidateForm").attr("action", "/InterviewAssist/renderquestions?action=EXIT");
                $( "#candidateForm" ).submit();
                //document.getElementById('candidateForm').action="/InterviewAssist/renderquestions?action=EXIT";
                //document.getElementById('candidateForm').submit();
            }
            
                                                           
                       
/* function setEditorVal(form){
               var editor = ace.edit("editor");
               var programCode = editor.getSession().getValue();
               document.getElementById('programChallenegeFormID:hiddenEditorCodeTxtValue').value = programCode;
               //alert(document.getElementById('programChallenegeFormID:hiddenEditorCodeTxtValue').value);
                editor.getSession().setValue(document.getElementById('programChallenegeFormID:hiddenEditorCodeTxtValue').value) ;
} */

          </script>


	<script type="text/javascript">
                var hour = <c:out value="${candidateForm.loginTimeHour}"/>;
                var minutes = <c:out value="${candidateForm.loginTimeMinutes}"/>;
                var secs = <c:out value="${candidateForm.loginTimeSecs}"/>;
                var totalNumOfAnsweredQs = 1;
                var totalQuestionsCount = 5;
                
                var totalTime = <c:out value="${totalTime}"/>;; 
                var availableMins = Math.round(totalTime);      
                                start(); 
               
            </script>

		<footer>
       	<div id="footer" >
				2017   CenturyLink India, Inc. All Rights Reserved. 
		</div>  
		</footer>	

</section>
		
         
</body>
</html>

