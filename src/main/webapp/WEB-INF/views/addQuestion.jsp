<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page import="com.centurylink.spring.model.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<script src="${pageContext.request.contextPath}/resources/ace.js" type="text/javascript" charset="utf-8"></script>    
<script src="${pageContext.request.contextPath}/resources/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
</head>

<style>
.ErrorField {
	border-color: #D00;
	color: #D00;
	background: #FFFFFE;
}

span.ValidationErrors {
	display: block !important;
	min-height: 19px;
	font-size: 12px;
	color: #D00;
	padding-left: 10px;
	font-family: sans-serif;
}
</style>
<script>

$(function() {
	
	
	$("#questionComplexityID").selectmenu();
		$("#questionTechnologyID").selectmenu();
		$("#questionActiveID").selectmenu();
		var finalTestValues = [];
		
		
		$("#resetID").click(function() {
			$("div.error").hide();
		});
		
		$("#addOptions").click(
		function() {
			$("div.error").hide();
			var totalOptVal = parseInt($("#totalMCQOpt").val());
			var validationJunit = new Boolean(true);
			$("#multipleOptions textarea, #multipleOptions input").each(
					function() {
						if (!(isNotNull($(this).val()))) {
							$("div.error").show();
							$("div.error").focus();
							$("div.error span")
									.html(
											"Please enter the option and then proceed to add next option.");
							validationJunit = false;
							return validationJunit;
						}
					});
			if (validationJunit){
				if( totalOptVal < 5) {
					totalOptVal += 1;
					$("#totalMCQOpt").val(totalOptVal);
					$("#multipleOptions")
							.append(
									'<div class="emptyDIV"></div><div class="form-row">	<label class="levels">Option '
											+ totalOptVal
											+ ' &nbsp; <span class="required">*</span></label><input type="text" name="multipleOptionTxt'+totalOptVal+'" id="multipleOptionTxtID" size="25"  placeholder="Enter the Option"/></div><div class="form-row"><span id="mutlipleChoiceAns" class="levels"><label class="levels">Correct Answer <span class="required">*</span></label><input type="checkbox" id="mutlipleChoiceAns" name="mutlipleChoiceAns" value="'+totalOptVal+'" /></span></div>');
				} else {
					alert("You had reached the maxmium limit to add the options.")
				}
			}
				
		});

		$("#accordion").accordion({
			collapsible : true
		});

	});

	function isNotNull(test) {
		if (test != null && test != '') {
			return true;
		} else {
			return false;
		}
	}

	$(document).ready(function() {

		 $("#addQuestion").click(
				function() {
					$("div.error").hide();
					var returnVal = new Boolean(true);

					$("#questionSection textarea, #questionSection input, #questionSection select").each(
							function() {
								if (!(isNotNull($(this).val())) && "role" != $(this).attr("name") ) {
									$("div.error").show();
									$("div.error").focus();
									$("div.error span").html(
											"Please enter all the fields in question section is manadatory.");
									returnVal = false;
									return returnVal;
								}
					});
					
					if($("#validCheckBox input:checked").length == 0 && returnVal){
							$("div.error").show();
							$("div.error").focus();
							$("div.error span").html("Please selected the applicability.");
							returnVal = false;
							return returnVal;
					}
					
					if(returnVal ){
						$("#multipleOptions  input[type=text]").each(function() {
							if (!(isNotNull($(this).val()))) {
								$("div.error").show();
								$("div.error").focus();
								$("div.error span").html("Please enter all the MCQ text option.");
								returnVal = false;
								return returnVal;
							}
						});
					}
						
					if(returnVal ){
						if ($("#multipleOptions input:checked").length > 0) {
						
							if($("#multipleOptions input:checked").length == 1){
								$("#optionTypSelect").val("Single");
							}else{
								$("#optionTypSelect").val("Multiple");
							}
							
							var chkList = "";
							$("#multipleOptions input[type=checkbox]:checked").each(function() {
								//  var sThisVal = (this.checked ? this.val : "0");
								chkList += $(this).val() + ",";
							});
							$("#selectedMultiAnsOpt").val(chkList);
						}else{
							$("div.error").show();
							$("div.error").focus();
							$("div.error span").html("Please check at least one checkbox.");
							returnVal = false;
							return returnVal;
						}
					}
					
					
					//alert("gggggggggggggggggggggggg");
					/*	if (isNotNull($("#questionTimeID").val()) && !($.isNumeric($("#questionTimeID").val()))) {
						$("div.error").show();
						$("div.error").focus();
						$("div.error span").html(
								"Please enter the time for question in minutes.");
						returnVal = false;
						return returnVal;
					}
					
					

					if ($("#questionTypeID").val() == "MCQ") {

						var optionType = $("#optionSelectID").val();
						if ("Multiple" == optionType) {
							var checked = $("#multipleOptions input:checked").length > 0;
							if (!checked) {
								$("div.error").show();
								$("div.error").focus();
								$("div.error span").html("Please check at least one checkbox.");
								returnVal = false;
								return returnVal;
							} else {
								var chkList = "";
								$("#multipleOptions input[type=checkbox]:checked").each(function() {
									//  var sThisVal = (this.checked ? this.val : "0");
									chkList += $(this).val() + ",";
									$("#selectedMultiAnsOpt").val(chkList);
								});
							}

							$("#multipleOptions  input[type=text]").each(function() {
								if (!(isNotNull($(this).val()))) {
									$("div.error").show();
									$("div.error").focus();
									$("div.error span").html("Please enter all the multiple choice option.");
									returnVal = false;
									return returnVal;
								}
							});
						} else {
							$("#singleOptions input").each(function() {
								if (!(isNotNull($(this).val()))) {
									$("div.error").show();
									$("div.error").focus();
									$("div.error span").html("Please enter all the fields for options.");
									returnVal = false;
									return returnVal;
								}
							});
						}
					}

					if ($("#questionTypeID").val() == "Programming" && returnVal == true) {

						var editor = ace.edit("editor");
						if (isNotNull(editor.getSession().getValue()) && isNotNull($('#fileNameID').val())) {

							var sampleCode = editor.getSession().getValue();
							if (sampleCode.indexOf($('#fileNameID').val()) != -1) {
								$("#templateCode").val(sampleCode);
								$("div.error").hide();
							} else {
								$("div.error").show();
								$("div.error").focus();
								$("div.error span").html(
										"Please enter correct file name similar to the sample code.");
								returnVal = false;
								return returnVal;
							}

						} else {
							$("div.error").show();
							$("div.error").focus();
							$("div.error span").html("Please enter the Pseudo Code and file name for it.");
							returnVal = false;
							return returnVal;
						}

						if (isNotNull($('#testCodeID').val()) && isNotNull($('#testFileNMID').val())
								&& isNotNull($('#testWtID').val())) {
							$("div.error").hide();
						} else {
							$("div.error").show();
							$("div.error").focus();
							$("div.error span").html("Please enter Test Code, File Name and Weightage.");
							returnVal = false;
							return returnVal;
						}
						
						if (isNotNull($("#testWtID").val()) && !($.isNumeric($("#testWtID").val()))) {
							$("div.error").show();
							$("div.error").focus();
							$("div.error span").html(
									"Please enter the junit weightage in minutes.");
							returnVal = false;
							return returnVal;
						}

					}*/

					if (returnVal) {
						$('#questionType').val("MCQ");
						var carDataString = JSON.stringify($('#questionForm').serializeObject());
						$.ajax({
							type : "POST",
							url : "/InterviewAssist/addQuestionSet",
							data : carDataString,
							contentType : "application/json; charset=utf-8",
							dataType : "json",
							success : function(data) {
								//alert(data &&  data.indexOf("Error") == -1);
								if (data.indexOf("Error") != -1) {
									$("div.error").show();
									$("div.error").focus();
									$("div.error span").html(data);
									returnVal = false;
									return returnVal;
								}else{
									alert(data);
									$("#questionForm").trigger("reset");
									$("div.error").hide();
								}
							},
							failure : function(errMsg) {
								alert(errMsg);//handle it in a proper way
							}
						});
					}
				}); 
	});
	
</script>

<form action="#" id="questionForm">
<div class="error" tabindex="1" style="display:none;color: red;margin-top: 10px;">
	<img src="/InterviewAssist/resources/warning.gif" alt="Warning!" width="24" height="24" style="float:left; margin: -5px 10px 0px 0px;">
	<span></span>
	<br clear="all">
</div>
	<div id="accordion" style="margin-top: 10px;">
		<h3 style="margin-bottom: 2px;">Question</h3>
		<div id="questionSection">

			<div class="form-row">
				<label class="levels">MCQ Question &nbsp; <span class="required">*</span></label>
				<textarea name="questionDesc" id="questionDescID" placeholder="Enter The Question" cols="71" rows="3"></textarea>
			</div>
			<div class="emptyDIV"></div>
			<div class="form-row">
				<label class="levels">Technology &nbsp; <span class="required">*</span></label> <select multiple="multiple" name="questionTechnology" id="questionTechnologyID" class="drop_down">
					<option></option>
					<option>Java</option>
					<option>C</option>
					<option>C++</option>
					<option>C#</option>
					<option>.net</option>
					<option>AngularJS</option>
					<option>JavaScript</option>
				</select>
			</div>
			<div class="form-row">
				<label class="levels">Complexity &nbsp;<span class="required">*</span></label> <select name="questionComplexity" id="questionComplexityID" class="drop_down">
					<option></option>
					<option>Simple</option>
					<option>Medium</option>
					<option>Complex</option>
				</select>
			</div>
			<div class="emptyDIV"></div>
			<div class="form-row">
				<span id="validCheckBox" class="levels"> 
				<label class="levels">Applicability <span class="required">*</span></label> 
				<c:forEach items="${roleLST}" var="roleVal" varStatus="status">
					
					<input type="checkbox" id="validCheckBox_${status.index}" name="role" value="${roleVal.role}" placeholder="Software Engineeer"><c:out value="${roleVal.role}" />&nbsp;
				</c:forEach>
				
					<!-- <input type="checkbox" id="validCheckBox_1" name="role" value="SE" placeholder="Software Engineeer">SE 
					<input type="checkbox" id="validCheckBox_2" name="role"	value="SSE" placeholder="Senior Software Engineeer">SSE 
					<input type="checkbox" id="validCheckBox_3" name="role" value="ML" placeholder="Module Lead">ML 
					<input type="checkbox" id="validCheckBox_4" name="role" value="TL" placeholder="Technical Lead">TL 
					<input type="checkbox" id="validCheckBox_5" name="role" value="PL" placeholder="Project Lead">PL 
					<input type="checkbox" id="validCheckBox_6"	name="role" value="PM" placeholder="Trainee">Trainee -->
				</span>
			</div>
		</div>
		<h3 style="margin-bottom: 2px;">Add Multiple Choice</h3>
		<div id="multipleChoiceSection" >

			<div class="emptyDIV"></div>
				<div id="multipleOptions">
					<div class="form-row">
							<label class="levels">Option 1 &nbsp; <span class="required">*</span></label>
							<input type="text" name="multipleOptionTxt1" id="multipleOptionTxtID" size="25"  placeholder="Enter the Option"/>
					</div>
					<div class="form-row">
						<span id="mutlipleChoiceAns" class="levels"> 
							<label class="levels">Correct Answer <span class="required">*</span></label> 
							<input type="checkbox" id="mutlipleChoiceAns" name="mutlipleChoiceAns" value="1" />
						</span>
					</div>
					<div class="emptyDIV"></div>
					<div class="form-row">
							<label class="levels">Option 2 &nbsp; <span class="required">*</span></label>		
							<input type="text" name="multipleOptionTxt2" id="multipleOptionTxtID" size="25"  placeholder="Enter the Option"/>
					</div>
					<div class="form-row">
						<span id="mutlipleChoiceAns" class="levels"> 
							<label class="levels">Correct Answer <span class="required">*</span></label> 
							<input type="checkbox" id="mutlipleChoiceAns" name="mutlipleChoiceAns" value="2" />
						</span>
					</div>
				</div>
			
			<div class="emptyDIV"></div>
			<div>
				<input type="button" name="addOptions" value="Add Options" id="addOptions" />
				
			</div>

		</div>

	</div>
	<div>
		<input type="button" name="addQuestion" value="Add Question" id="addQuestion" /> 
		<input type="reset" name="cancel" value="Reset" id="resetID" />
	</div>

	<!-- <input type="hidden" name="testCaseList" id="testCaseList" value=""> -->

<input type="hidden" name="totalMCQOpt" id="totalMCQOpt" value="2">
<input type="hidden" name="role" id="role"/>
<input type="hidden" name="selectedMultiAnsOpt" id="selectedMultiAnsOpt"/>
<input type="hidden" name="optionTypSelect" id="optionTypSelect"/>
<input type="hidden" name="questionType" id="questionType"/>

</form>


<script>
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
</script>

</html>