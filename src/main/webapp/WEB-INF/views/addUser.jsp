<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<!-- The jQuery library is a prerequisite for all jqSuite products -->
<!-- Added for Modal dialog -->
  <script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/bootstrap.min.js"></script>	
<!-- Added for Modal dialog -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap.min.css">
</head>
<script>

$(function() {
	$("#privilegeID").selectmenu();
	
	
    $('#addUserID').click(function(event) {
    	var userJson =$('#userForm').serializeArray();
    	var properJsonObj = jQFormSerializeArrToJson(userJson);
    	
     	if (jQuery.parseJSON(JSON.stringify(properJsonObj)).uname != "" && jQuery.parseJSON(JSON.stringify(properJsonObj)).cuid != "") {
     		$.ajax({
                type: "POST",
                url: "/InterviewAssist/addUserDtls",
                data:  JSON.stringify(properJsonObj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data){
                     //handle it in a proper way
                     $("#modalText").text(data);
 					 $("#messageModal").modal('show');
                     $("#userForm").trigger("reset");
                },
                failure: function(errMsg) {
                   //handle it in a proper way
                   $("#modalText").text(errMsg);
				   $("#messageModal").modal('show');
                }
            });
        } else {
        	$("#modalText").text("Please fill UserName and Cuid field");
			$("#messageModal").modal('show');
        }
        
        return false;
    });
});


function jQFormSerializeArrToJson(formSerializeArr){
 var jsonObj = {};
 jQuery.map( formSerializeArr, function( n, i ) {
     jsonObj[n.name] = n.value;
 });

 return jsonObj;
}

</script>


<div class="container">
	
	  <!-- Modal -->
	  <div id="messageModal" class="modal fade">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <!-- dialog body -->
	      <div class="modal-body">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <label id="modalText"></label>
	      </div>
	      <!-- dialog buttons -->
	      <div class="modal-footer"><button type="button" class="btn btn-primary"  data-dismiss="modal">OK</button></div>
	    </div>
	  </div>
	</div>
	</div>

<form id="userForm" method="post" action="#">

	<div class="headerDIV">
	ADD USER
	</div>

	<div class="form-row">
		<input type="text" name="uname" id="nameID" size="31" placeholder="User Name" required/>
	</div>
	<div class="emptyDIV"></div>
	<div class="form-row">
		<input type="text" name="cuid" id="cuidId" size="31" placeholder="CUID" required/>
	</div>
	<div class="emptyDIV"></div>
	<div class="form-row">
			<label class="levels">Privilege: <span class="required">*</span></label>
			<select name="privilege" id="privilegeID">
		      <option>Admin</option>
		      <option>Non_Admin</option>
   		</select> 
	</div>
	<div class="emptyDIV"></div>
<div>
   	 	<input type="button" name="login" value="AddUser" id="addUserID" /> 
   	 	<input type="reset" name="cancel" value="Reset" id="resetID"/>
</div>
</form>

</html>