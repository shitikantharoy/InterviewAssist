<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html lang="en">
<head>
<!-- Added for Modal dialog -->
  <script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/jquery.min.js"></script>
  <script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/bootstrap.min.js"></script>	
<!-- Added for Modal dialog --> 
<!-- The jQuery library is a prerequisite for all jqSuite products -->
<script type="text/ecmascript" src="../../../js/jquery.min.js"></script>
<!-- This is the Javascript file of jqGrid -->
<script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/jquery.jqGrid.js"></script>
<!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- We support more than 40 localizations -->
<script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/grid.locale-en.js"></script>

<!-- The link to the CSS that the grid needs -->
<link rel="stylesheet" type="text/css" media="screen"
	href="${pageContext.request.contextPath}/resources/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap.min.css">	
<meta charset="utf-8" />
<title>Expand all subgrids on load</title>
</head>
<body>

<div class="container">
	
	  <!-- Modal -->
	  <div id="messageModal" class="modal fade">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <!-- dialog body -->
	      <div class="modal-body">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <label id="modalText"></label>
	      </div>
	      <!-- dialog buttons -->
	      <div class="modal-footer"><button type="button" class="btn btn-primary"  data-dismiss="modal">OK</button></div>
	    </div>
	  </div>
	</div>
	</div>


<div class="headerDIV">
	EDIT Users
	</div>
	<div style="margin-top: 10px;"></div>
	<table id="jqGrid"></table>
	<div id="jqGridPager"></div>

<div id="detailsPlaceholder" style="margin-top: 50px;">
       
       
       <table id="jqGridMCQDetails"></table>
	<div id="jqGridMCQDetailsPager"></div>
    </div>


	<script type="text/javascript">
    
  
		$(document).ready(function() {
			$("#jqGrid").jqGrid({
				url : '/InterviewAssist/getUsers',
				datatype : "json",
				colModel : [

				{
						label: "",
                        name: "actions",
                        width: 10,
                        search : false,
                        formatter: "actions",
                        formatoptions: {
                            keys: true,
                            mtype: 'POST',
                            delbutton: false,
                            url : "/InterviewAssist/editUser",
                           
                            editOptions: {
                            },
                            addOptions: {},
                            delOptions: {
                            	/* url : '/InterviewAssist/deleteQuestion',
                                mtype: 'POST',
                                reloadAfterSubmit: true,
                                ajaxDelOptions: {
                                    contentType: "application/json"
                                },
                                serializeDelData: function(postdata) {
                                    return JSON.stringify(postdata);
                                } */
                            },
                            onSuccess:function(jqXHR) {
                                $("#modalText").text(jqXHR.responseText);
           						$("#messageModal").modal('show'); 
                                return true;
                            },
                            onError:function(rowid, jqXHR, textStatus) {
                                $("#modalText").text(jqXHR.responseText);
           						$("#messageModal").modal('show');
                            }
                        },
                    },{
					label : 'User Id',
					name : 'userId',
					width : 40,
					key : true,
					editable : false,
					hidden : true,
					editrules : {
						required : true
					}
				}, {
					label : 'CUID',
					name : 'cuid',
					width : 40,
					editable : true,
					searchoptions: {
						sopt : ['cn']
					}
				}, {
					label : 'Name',
					name : 'uname',
					width : 40,
					editable : true,
					searchoptions: {
						sopt : ['cn']
					}
				}, {
					label : 'Privilege',
					name : 'privilege',
					width : 40,
					editable : true,
					edittype : "select",
					editoptions : {
						value : "Admin:Admin;Non_Admin:Non_Admin"
					},
					search : false
				} ],
				sortname : 'uname',
				sortorder : 'asc',
				loadonce : true,
				width : 1145,
				height : 210,
				multiselect : false,
				rowNum : 10,
				rowList : [ 10, 20, 30 ],
				view : true,
				caption : "Users",
				pager : "#jqGridPager"
			});
			
			// activate the toolbar searching
            $('#jqGrid').jqGrid('filterToolbar');

			
            $('#jqGrid').navGrid('#jqGridPager', {
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				view : true,
				position : "left",
				closeOnEscape : true,
				cloneToTop : false
			}, {
			}, {
			}, {
			});
			

		});
		
		 
	</script>

</body>
</html>