<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta charset='utf-8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script type="text/ecmascript" src="../../../js/jquery.min.js"></script>
<script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/jquery.jqGrid.js"></script>
<script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/grid.locale-en.js"></script>
<script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/jquery.datetimepicker.full.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/jquery.datetimepicker.min.css">

<link rel="stylesheet" type="text/css" media="screen"
	href="${pageContext.request.contextPath}/resources/ui.jqgrid.css" />
<meta charset="utf-8" />
<title>Expand all subgrids on load</title>
</head>

<!-- <div style="margin-top: 10px;"></div>
	<table id="jqGrid"></table>
	<div id="jqGridPager"></div>
	 <div id="detailsPlaceholder" style="margin-top: 50px;">
       
       
       <table id="jqGridMCQDetails"></table>
	<div id="jqGridMCQDetailsPager"></div>
    </div> -->
    
<script>

$(function() {
	
	
	$("#applicability").selectmenu();
	
	$('#eventStartDate').datetimepicker({
		timepicker:false,
		formatDate:'d.m.Y',
		format:'Y-m-d',
		defaultDate:'+03.01.1970'
	});
	
	$('#eventEndDate').datetimepicker({
		timepicker:false,
		formatDate:'d.m.Y',
		format:'Y-m-d',
		defaultDate:'+03.01.1970'
	});
	

	
	 var getData = function (request, response) {
	        $.getJSON(
	            "/InterviewAssist/getEventNames?input="+request.term,
	            function (data) {
	                response(data);
	            });
	    };
	 
	    var selectItem = function (event, ui) {
	        $("#eventName").val(ui.item.value);
	        return false;
	    }
	 
	    $("#eventName").autocomplete({
	        source: getData,
	        select: selectItem,
	        minLength: 3
	    });

		$('#searchReport')
				.click(
						function() {

							var eventName = $("#eventName").val();
							var betweenDates="";
							if ((isNotNull($("#eventStartDate").val())) && !(isNotNull($("#eventEndDate").val()))) {
								alert("To date can't be null, when From date is entered.");
								return false;
							}else if(!(isNotNull($("#eventStartDate").val())) && (isNotNull($("#eventEndDate").val()))){
								alert("From date can't be null, when To date is entered.");
								return false;
							}else if((isNotNull($("#eventStartDate").val())) && (isNotNull($("#eventEndDate").val()))){
								betweenDates = $("#eventStartDate").val() + " "	+ $("#eventEndDate").val();
							}
							var applicability = $("#applicability").val();
							var querystring = 'eventName=' + eventName
									+ '&betweenDates=' + betweenDates
									+ '&applicability=' + applicability;
							$("#detailsPlaceholderTemp").empty();
							$("#detailsPlaceholderTemp")
									.append(
											"<table id='jqGrid'></table><div id='jqGridPager'></div>");
							$("#jqGrid")
									.jqGrid(
											{
												url : '/InterviewAssist/getReport?'
														+ querystring,
												type : "GET",
												contentType : "application/json; charset=utf-8",
												datatype : "json",
												colModel : [

														{
															label : 'User Name',
															name : 'candidateCuid',
															key : true,
															width : 30,

														},
														{
															label : 'Event Name',
															name : 'eventName',
															width : 30,

														},
														{
															label : 'Interview date',
															name : 'eventDate',
															width : 30,

														},
														{
															label : 'Role',
															name : 'role',
															width : 30,

														},
														{
															label : 'Total MCQs',
															name : 'totalMCQQuestions',
															width : 30,

														},
														{
															label : 'Correct MCQs',
															name : 'totalCorrectMCQQuestions',
															width : 30,

														},
														{
															label : 'MCQ Score',
															name : 'mcqScore',
															width : 30,

														},
														{
															label : 'Total Programs',
															name : 'totalProgrammingQuestions',
															width : 30,

														},
														{
															label : 'Correct Programs',
															name : 'totalCorrectProgrammingQuestions',
															width : 30,

														},
														{
															label : 'Program Score',
															name : 'programmingScore',
															width : 30,

														} ],
												sortname : 'questionType',
												sortorder : 'asc',
												loadonce : true,
												width : 800,
												height : 210,
												multiselect : false,
												rowNum : 10,
												pager : "#jqGridPager",
												rowList : [ 10, 20, 30 ],
												view : true,
												caption : "Test Report",
												onSelectRow : showDetailsGrid
											});

							$('#jqGrid').navGrid('#jqGridPager', {
								edit : false,
								add : false,
								del : false,
								search : true,
								refresh : true,
								view : true,
								position : "left",
								cloneToTop : false
							}, {
								editCaption : "Edit Question",
								recreateForm : true,
								closeAfterEdit : true,
								errorTextFormat : function(data) {
									return 'Error: ' + data.responseText
								}
							}, {
								closeAfterAdd : true,
								recreateForm : true,
								errorTextFormat : function(data) {
									return 'Error: ' + data.responseText
								}
							}, {

								errorTextFormat : function(data) {
									return 'Error: ' + data.responseText
								}
							}).navButtonAdd('#jqGridPager', {
						        caption: "Export to Excel",
						        buttonicon: "ui-icon-disk",
						        onClickButton: function () {
						            $("#jqGrid").jqGrid("exportToExcel",{
						    			includeLabels : true,
						    			includeGroupHeader : true,
						    			includeFooter: true,
						    			fileName : "jqGridExport.xlsx",
						    			maxlength : 40 // maxlength for visible string data 
						    		});
						        }
						    });

						});

	});

	function showDetailsGrid(rowID, selected) {

		var userName = $('#jqGrid').jqGrid('getCell', rowID, 'candidateCuid');

		$("#detailsPlaceholder").empty();
		$("#detailsPlaceholder")
				.append(
						"<table id='jqGridIndividualDetails'></table><div id='jqGridIndividuaDetailsPager'></div> ");

		$("#jqGridIndividualDetails").jqGrid({
			url : "/InterviewAssist/getReportByCUID?input=" + userName,
			type : "GET",
			//postData:  JSON.stringify(properJsonObj),
			datatype : "json",
			colModel : [

			{
				label : 'CUID',
				name : 'candidateCuid',
				width : 30,

			}, {
				label : 'CUID',
				name : 'candidateCuid',
				width : 30,

			}, {
				label : 'Question NO',
				name : 'qNo',
				hidden : true,
				width : 30,

			}, {
				label : 'Event Name',
				name : 'eventName',
				hidden : true,
				width : 30,

			}, {
				label : 'Event date',
				name : 'eventDate',
				width : 30,

			}, {
				label : 'Candidate Role',
				name : 'role',
				width : 30,

			}, {
				label : 'Question Type',
				name : 'questionType',
				width : 30,

			}, {
				label : 'MCQ Answer Correct',
				name : 'correctFlag',
				width : 30,

			}, {
				label : 'ProgrammingScore',
				name : 'programmingScore',
				width : 30,

			}, {
				label : 'Complexity',
				name : 'complexity',
				width : 30,

			},

			{
				label : 'Total Test Cases',
				name : 'totalTestCases',
				width : 30,

			}, {
				label : 'TestCasesPassed',
				name : 'testCasesPassed',
				width : 30,

			}, {
				label : 'MCQ/Program',
				name : 'complexity',
				width : 30,
				editable : false,
				formatter : codePopUpFormatter

			} ],

			loadonce : true,
			width : 800,
			height : 210,
			multiselect : false,
			rowNum : 10,
			pager : "#jqGridIndividuaDetailsPager",
			rowList : [ 10, 20, 30 ],
			view : true,
			caption : "Test Report"

		});

	}

	function codePopUpFormatter(cellValue, options, rowObject) {
		var querystring = '/InterviewAssist/getMCQProgramQustionAns?qNo='
				+ rowObject.qNo + '&userName=' + rowObject.candidateCuid
				+ '&eventName=' + rowObject.eventName;
		be = "<input type='button' value='Details' style='height:20px;width:50px;' onclick=myFunction('"
				+ querystring + "')>";

		return be;
	}

	function myFunction(querystring) {
		var myWindow = window.open(querystring, "",
				"width=800,height=600,resizable=1,scrollbars=yes,status=yes");
	}
	
	
	function isNotNull(test) {
		if (test != null && test != '') {
			return true;
		} else {
			return false;
		}
	}
	

</script>
<form id="reportForm" method="post" action="#">

<div class="headerDIV">
	REPORT
</div>


		
	<div class="form-row">
		<label class="levels">Event Name</label>
        <input type="text"	name="eventName" id="eventName" placeholder="Event Name" size="31"/> 
    </div>
    <div class="form-row">
    	<label class="levels">From Date</label>
        <input type="text"	name="eventStartDate" id="eventStartDate" size="31" placeholder="From Date "/> 
   </div>
    <div class="form-row">
    	<label class="levels">To Date</label>
        <input type="text"	name="eventEndDate" id="eventEndDate" size="31" placeholder="To Date"/> 
    </div>
    <div class="emptyDIV"></div>
    <div class="form-row">
        <label class="levels">Applicability</label> 
		<select name="applicability"  id="applicability" class="drop_down">
			 	<option value=""></option>
			    <c:forEach items="${roleLST}" var="roleVal" varStatus="status">
			        <option value="${roleVal.role}"><c:out value="${roleVal.role}" /></option>
			    </c:forEach>
			</select>
    </div>
   	 	<input type="button" name="search" value="Search" id="searchReport" /> 
   	 	<input type="submit" name="reset" value="Reset" />

</form>


	<div id="detailsPlaceholderTemp" style="margin-top: 50px;">
		<table id="jqGrid"></table>
		<div id="jqGridPager"></div>
	</div>
	 <div id="detailsPlaceholder" style="margin-top: 50px;">
       
       
       <table id="jqGridIndividualDetails"></table>
	<div id="jqGridIndividuaDetailsPager"></div>
    </div>
</html>