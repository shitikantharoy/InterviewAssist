<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>InCTLI</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/login.css">

</head>
<body>
	<div class="container">
		<div class="mainHeader">
			<div></div>
		</div>
		<h1 align="center">
			<font face=Calibri>Welcome to CenturyLink</font>
		</h1>
		<div align="center">
			<span>Dear <strong>${loggedinuser}</strong>, Thank you for attending the exam.
			<c:if test="${loggedintest != null}"></span>Click <a href="<c:url value='/event?event=${loggedintest}'/>">here</a>  to appear a new Test.<span class="floatRight"></span></c:if>
		</div>
		<div id="footer">
			<br> 2017 CenturyLink India, Inc. All Rights Reserved.
		</div>
	</div>
</body>
</html>
