<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html lang="en">
<head>
<!-- The jQuery library is a prerequisite for all jqSuite products -->
<!-- The jQuery library is a prerequisite for all jqSuite products -->
<!-- Added for Modal dialog -->
  <script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/bootstrap.min.js"></script>	
<!-- Added for Modal dialog -->
<script type="text/ecmascript" src="../../../js/jquery.min.js"></script>
<!-- This is the Javascript file of jqGrid -->
<script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/jquery.jqGrid.js"></script>
<!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- We support more than 40 localizations -->
<script type="text/ecmascript" src="${pageContext.request.contextPath}/resources/grid.locale-en.js"></script>

<!-- The link to the CSS that the grid needs -->
<link rel="stylesheet" type="text/css" media="screen"
	href="${pageContext.request.contextPath}/resources/ui.jqgrid.css" />
<!-- Added for Modal dialog -->	
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap.min.css">
<!-- Added for Modal dialog -->	
<meta charset="utf-8" />
<title>Expand all subgrids on load</title>
</head>
<body>
<body>

<div class="container">
	
	  <!-- Modal -->
	  <div id="messageModal" class="modal fade">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <!-- dialog body -->
	      <div class="modal-body">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <label id="modalText"></label>
	      </div>
	      <!-- dialog buttons -->
	      <div class="modal-footer"><button type="button" class="btn btn-primary"  data-dismiss="modal">OK</button></div>
	    </div>
	  </div>
	</div>
</div>


<div class="headerDIV">
	EDIT EVENTS
	</div>
	<div style="margin-top: 10px;"></div>
	<table id="jqGrid"></table>
	<div id="jqGridPager"></div>

<div id="detailsPlaceholder" style="margin-top: 50px;">
       
       
       <table id="jqGridMCQDetails"></table>
	<div id="jqGridMCQDetailsPager"></div>
    </div>


	<script type="text/javascript">
    
  
		$(document).ready(function() {
			$("#jqGrid").jqGrid({
				url : '/InterviewAssist/getEvents',
				datatype : "json",
				colModel : [

				{
						label: "",
                        name: "actions",
                        width: 20,
                        search : false,
                        formatter: "actions",
                        formatoptions: {
                            keys: true,
                            mtype: 'POST',
                            delbutton: false,
                            url : "/InterviewAssist/editEvent",
                           
                            editOptions: {
                            },
                            addOptions: {},
                            delOptions: {
                            	/* url : '/InterviewAssist/deleteQuestion',
                                mtype: 'POST',
                                reloadAfterSubmit: true,
                                ajaxDelOptions: {
                                    contentType: "application/json"
                                },
                                serializeDelData: function(postdata) {
                                    return JSON.stringify(postdata);
                                } */
                            },
                            onSuccess:function(jqXHR) {
                                $("#modalText").text(jqXHR.responseText);
           						$("#messageModal").modal('show'); 
                                return true;
                            },
                            onError:function(rowid, jqXHR, textStatus) {
                                $("#modalText").text(jqXHR.responseText);
           						$("#messageModal").modal('show'); 
                            }
                        },
                    },{
					label : 'Event Id',
					name : 'interviewEventId',
					width : 30,
					key : true,
					editable : false,
					hidden : true,
					editrules : {
						required : true
					}
				}, {
					label : 'Name',
					name : 'eventName',
					width : 40,
					editable : false,
					searchoptions: {
						sopt : ['cn']
					}
				// must set editable to true if you want to make the field editable
				}, {
					label: 'URL',
					name : 'eventUrl',
					width : 150,
					editable : false,
					search : false
				}, {
					label : 'Technology',
					name : 'eventTechnology',
					width : 35,
					editable : false,
					search : false
				}, {
					label : 'Role',
					name : 'applicability',
					width : 30,
					editable : false,
					search : false
				}, {
					label : 'Start Date',
					name : 'eventStartDate',
					width : 35,
					editable : false,
					search : false
				},{
					label : 'Start Time',
					name : 'eventStartTime',
					width : 20,
					editable : false,
					search : false
				},{
					label : 'End Date',
					name : 'eventEndDate',
					width : 35,
					editable : true,
					search : false,
					editrules:{
						//custom rules
                        custom_func: isValidDate,
                        custom: true,
                        required:true
                    },
					editoptions: {
                       dataInit: function (element) {
                           $(element).datepicker({
                               id: 'orderDate_datePicker',
                               dateFormat: 'yy/mm/dd',
                               maxDate: new Date(2050, 0, 1),
                               showOn: 'focus'
                           });
                       }
					}
				},{
					label : 'End Time',
					name : 'eventEndTime',
					width : 30,
					editable : true,
					editrules:{
						//custom rules
                        custom_func: updateTime,
                        custom: true
                    },
					editoptions : {
						//value : "Simple:Simple;Medium:Medium;Complex:Complex"
						//value : "00:00:00:00;01:00:01:00;02:00:02:00;03:00:03:00;04:00:04:00;05:00:05:00,06:00:06:00,07:00:07:00,08:00:08:00,09:00:09:00,10:00:10:00,11:00:11:00,12:00:12:00,13:00:13:00,14:00:14:00,15:00:15:00,16:00:16:00,17:00:17:00,18:00:18:00,19:00:19:00,20:00:20:00,21:00:21:00,22:00:22:00,23:00:23:00"
						value : "01:01:00;02:02:00;03:03:00;04:04:00;05:05:00;06:06:00;07:07:00;08:08:00;09:09:00;10:10:00;11:11:00;12:12:00;13:13:00;14:14:00;15:15:00;16:16:00;17:17:00;18:18:00;19:19:00;20:20:00;21:21:00;22:22:00;23:23:00"
					},
					edittype : "select",
					search : false
				},{
					label : 'Status',
					name : 'eventStatus',
					width : 20,
					editable : true,
					edittype : "select",
					editoptions : {
						value : "ACTIVE:ACTIVE;IN-ACTIVE:IN-ACTIVE"
					},
					search : false
				} ],
				sortname : 'eventEndDate',
				sortorder : 'asc',
				loadonce : true,
				width : 1145,
				height : 210,
				multiselect : false,
				rowNum : 10,
				pager : "#jqGridPager",
				rowList : [ 10, 20, 30 ],
				view : true,
				onSelectRow: showDetailsGrid,
				caption : "Events"
			});
			
			// activate the toolbar searching
            $('#jqGrid').jqGrid('filterToolbar');

			$('#jqGrid').navGrid('#jqGridPager', {
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				view : true,
				position : "left",
				cloneToTop : false
			}, {
				editCaption : "Edit Event",
				recreateForm : true,
				beforeSubmit : function(postdata, form, oper) {

					 $.ajax({
						type : "POST",
						url : "/InterviewAssist/editEvent",
						data : JSON.stringify(postdata),
						dataType : "json",
						contentType : "application/json; charset=utf-8",
						success : function(response, textStatus, xhr) {
							jQuery('#jqGrid').jqGrid('clearGridData').jqGrid('setGridParam', {
								data : response,
								datatype : 'json'
							}).trigger('reloadGrid');
							return [ true, '', '' ];
						},
						error : function(xhr, textStatus, errorThrown) {
							$("#modalText").text("error");
       						$("#messageModal").modal('show'); 
						}
					});

				},
				closeAfterEdit : true,
				errorTextFormat : function(data) {
					return 'Error: ' + data.responseText
				}
			}, {
				closeAfterAdd : true,
				recreateForm : true,
				errorTextFormat : function(data) {
					return 'Error: ' + data.responseText
				}
			}, {
				beforeSubmit : function(postdata, form, oper) {
					var questionNoArry = "";
					$.makeArray($('#jqGrid').jqGrid('getGridParam', 'selarrrow').map(function(elem) {
						questionNoArry += $('#jqGrid').jqGrid('getCell', elem, 'questionNo') + "-";
						return questionNoArry;
					}));

					/* $.ajax({
						type : "POST",
						url : '/InterviewAssist/deleteQuestion',
						data : questionNoArry,
						dataType : "json",
						contentType : "application/json; charset=utf-8",
						success : function(response, textStatus, xhr) {
							jQuery('#jqGrid').jqGrid('clearGridData').jqGrid('setGridParam', {
								data : response,
								datatype : 'json'
							}).trigger('reloadGrid');
							return [ true, '', '' ];
						},
						error : function(xhr, textStatus, errorThrown) {
							alert("error");
						}
					}); */
				},
				errorTextFormat : function(data) {
					return 'Error: ' + data.responseText
				}
			});

		});
		
		
		function showDetailsGrid(rowID,selected) {
			
			$("#detailsPlaceholder").empty();
			$("#detailsPlaceholder").append(
					"<table id='jqGridMCQDetails'></table><div id='jqGridMCQDetailsPager'></div> ");

			$("#jqGridMCQDetails").jqGrid({
				url : '/InterviewAssist/getEventByID?eventID=' + rowID +'',
				//data: "41",
				type : "GET",
				datatype : "json",
				contentType : "application/json; charset=utf-8",
				colModel : [
				{
						label: "",
                        name: "actions",
                        width: 20,
                        search : false,
                        formatter: "actions",
                        formatoptions: {
                            keys: true,
                            mtype: 'POST',
                            delbutton: false,
                            url : "/InterviewAssist/saveEventQuestions",                           
                            editOptions: {
                            },
                            addOptions: {},
                            delOptions: {
                            	/* url : '/InterviewAssist/deleteQuestion',
                                mtype: 'POST',
                                reloadAfterSubmit: true,
                                ajaxDelOptions: {
                                    contentType: "application/json"
                                },
                                serializeDelData: function(postdata) {
                                    return JSON.stringify(postdata);
                                } */
                            },
                            onSuccess:function(jqXHR) {
                               $("#modalText").text(jqXHR.responseText);
          						$("#messageModal").modal('show'); 
                                return true;
                            },
                            onError:function(rowid, jqXHR, textStatus) {
                                $("#modalText").text(jqXHR.responseText);
          						$("#messageModal").modal('show'); 
                            }
                        },
                    },{
					label : 'Event Id',
					name : 'interviewEventId',
					width : 5,
					key : true,
					editable : false,
					hidden : true,
					editrules : {
						required : true
					}
				}, {
					label : 'Name',
					name : 'eventName',
					width : 40,
					editable : false
				// must set editable to true if you want to make the field editable
				}, {
					label: 'Simple MCQ',
					name : 'simpleMcq',
					width : 40,
					editrules:{
						//custom rules
                        custom_func: validatePositive,
                        custom: true
                    },
					editable : true
				}, {
					label : 'Medium MCQ',
					name : 'mediumMcq',
					width : 40,
					editrules:{
						//custom rules
                        custom_func: validatePositive,
                        custom: true
                    },
					editable : true
				}, {
					label : 'Complex MCQ',
					name : 'complexMcq',
					width : 40,
					editrules:{
						//custom rules
                        custom_func: validatePositive,
                        custom: true
                    },
					editable : true
				}, {
					label : 'Simple Program',
					name : 'simplePgm',
					width : 40,
					editrules:{
						//custom rules
                        custom_func: validatePositive,
                        custom: true
                    },
					editable : true
				},{
					label : 'Medium Program',
					name : 'mediumPgm',
					width : 40,
					editrules:{
						//custom rules
                        custom_func: validatePositive,
                        custom: true
                    },
					editable : true
				},{
					label : 'Complex Program',
					name : 'complexPgm',
					width : 40,
					editrules:{
						//custom rules
                        custom_func: validatePositive,
                        custom: true
                    },
					editable : true
				}],
				width : 800,
				rowNum : 5,
				loadonce : true,
				autowidth: true,
				width : 1145,
				height : 40,
				viewrecords : true,
				caption : 'Edit Event Questions'
			});
			
		}
		
		 function saveRows() {
	            var grid = $("#jqGridMCQDetails");
	            var ids = grid.jqGrid('getDataIDs');
	            
	           $("#jqGridMCQDetails  input").each(function() {
	            	if("" == $(this).val())
	            		$(this).val("empty");
				});
	            
	            for (var i = 0; i < ids.length; i++) {
	                grid.jqGrid('saveRow', ids[i]);
	            }
	            var gridData = $("#jqGridMCQDetails").getRowData();
	            
	            $.ajax({
					type : "POST",
					url : "/InterviewAssist/saveEventQuestions",
					data :JSON.stringify(gridData),
					dataType : "json",
					contentType : "application/json; charset=utf-8",
					success : function(response, textStatus, xhr) {
						jQuery('#jqGridMCQDetails').jqGrid('clearGridData').jqGrid('setGridParam', {
							data : response,
							datatype : 'json'
						}).trigger('reloadGrid');
						return [ true, '', '' ];
					},
					error : function(xhr, textStatus, errorThrown) {
						$("#modalText").text("error");
  						$("#messageModal").modal('show'); 
					}
				});

	        }
		 
		 function startEdit() {
	            var grid = $("#jqGridMCQDetails");
	            var ids = grid.jqGrid('getDataIDs');

	            for (var i = 0; i < ids.length; i++) {
	            	
	                grid.jqGrid('editRow',ids[i]);
	            }
	        }
		 function validatePositive(value, column) {
				if (isNaN(value)) {
					return [ false, column+": Please enter a positive value" ];
				} else {
					return [ true, "" ];
				}
			}
		 
		 function isValidDate(value, column) {
			  var regEx = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
			  
			  if(!(value.match(regEx))){
				  return [ false, column+": Not in yyyy/mm/dd format" ];
			  } else {
					return [ true, "" ];
			  }
		  }
		 
		 function updateTime(value, column){
			 value+=":00";
			 return [ true, "" ];
		 }
		 
	</script>

</body>
</html>