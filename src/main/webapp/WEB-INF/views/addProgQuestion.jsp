<%@page import="java.util.ArrayList"%>
<%@page import="com.centurylink.spring.model.Role"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<script src="${pageContext.request.contextPath}/resources/ace.js" type="text/javascript" charset="utf-8"></script>    
<script src="${pageContext.request.contextPath}/resources/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
</head>

<style>
.ErrorField {
	border-color: #D00;
	color: #D00;
	background: #FFFFFE;
}

span.ValidationErrors {
	display: block !important;
	min-height: 19px;
	font-size: 12px;
	color: #D00;
	padding-left: 10px;
	font-family: sans-serif;
}
</style>
<script>

$(function() {
	
	
	$("#questionComplexityID").selectmenu();
		$("#questionTechnologyID").selectmenu();
		$("#multipleOptions").hide();
		$("#singleOptions").hide();
		$("#addOptions").hide();

		var finalTestValues = [];
		
		$("#accordion").accordion({
			collapsible : true
		});

		$("#resetID").click(function() {
			$("div.error").hide();
			var editor = ace.edit("editor");
			editor.setValue("\n"+"\n"+"/* Use eclipse to format the code and then paste it here */");
		});

		$("#addTestCaseID")
				.click(
						function() {
							var validationJunit = new Boolean(true);
							
							$("#testCaseSectionID textarea, #testCaseSectionID input").each(
									function() {
										if (!(isNotNull($(this).val()))) {
											$("div.error").show();
											$("div.error").focus();
											$("div.error span")
													.html(
															"Please enter Test Code, File Name and Weightage and proceed for the next test case.");
											validationJunit = false;
											return validationJunit;
										}
									});
							
							
							if(validationJunit){
									$("#testCaseSectionID")
									.append(
											'<div class="emptyDIV"></div><div class="form-row"><textarea name="testCode" id="testCodeID" class="field-long field-textarea" placeholder="Unit Test Code" cols="76" rows="2"></textarea></div><div class="emptyDIV"></div><div class="form-row"><input type="text" name="testFileNM" id="testFileNMID" class="field-divided" placeholder="Test File Name" /></div><div class="form-row"><input type="text" name="testWt" id="testWtID" class="field-divided" placeholder="Test Weightage" /></div><div class="emptyDIV"></div>');
							}

								/* if (isNotNull($('#testCodeID').val()) && isNotNull($('#testFileNMID').val())
										&& isNotNull($('#testWtID').val())) {
									$("#testCaseSectionID")
											.append(
													'<div class="emptyDIV"></div><div class="form-row"><textarea name="testCode" id="testCodeID" class="field-long field-textarea" placeholder="Unit Test Code" cols="76" rows="2"></textarea></div><div class="emptyDIV"></div><div class="form-row"><input type="text" name="testFileNM" id="testFileNMID" class="field-divided" placeholder="Test File Name" /></div><div class="form-row"><input type="text" name="testWt" id="testWtID" class="field-divided" placeholder="Test Weightage" /></div><div class="emptyDIV"></div>');
								} else {
									$("div.error").show();
									$("div.error").focus();
									$("div.error span")
											.html(
													"Please enter Test Code, File Name and Weightage and proceed for the next test case.");
									return false;
								} */
						});

	});

	function isNotNull(test) {
		if (test != null && test != '') {
			return true;
		} else {
			return false;
		}
	}

	$(document).ready(
			function() {

				$("#addQuestion").click(
						function() {
							$("div.error").hide();
							var returnVal = new Boolean(true);

							$("#questionSection textarea, #questionSection input, #questionSection select").each(
									function() {
										if (!(isNotNull($(this).val())) && $(this).attr("name") !='example' ) {
											$("div.error").show();
											$("div.error").focus();
											$("div.error span").html(
													"Please enter all the fields in question section is manadatory.");
											returnVal = false;
											return returnVal;
										}
							});
							
							if($("#validCheckBox input:checked").length == 0 && returnVal){
								$("div.error").show();
								$("div.error").focus();
								$("div.error span").html("Please selected the applicability.");
								returnVal = false;
								return returnVal;
							}
							
							if(returnVal){
							
								var editor = ace.edit("editor");
								if (isNotNull(editor.getSession().getValue()) && isNotNull($('#fileNameID').val())) {
	
									var sampleCode = editor.getSession().getValue();
									if (sampleCode.indexOf($('#fileNameID').val()) != -1) {
										$("#templateCode").val(sampleCode);
										$("div.error").hide();
									} else {
										$("div.error").show();
										$("div.error").focus();
										$("div.error span").html(
												"Please enter correct file name similar to the sample code.");
										returnVal = false;
										return returnVal;
									}
	
								}else {
									$("div.error").show();
									$("div.error").focus();
									$("div.error span").html("Please enter the Pseudo Code and file name for it.");
									returnVal = false;
									return returnVal;
								}
							}
							
							if(returnVal){
								if (isNotNull($('#testCodeID').val()) && isNotNull($('#testFileNMID').val())
										&& isNotNull($('#testWtID').val())) {
									$("div.error").hide();
									if(!($.isNumeric($("#testWtID").val()))){
										$("div.error").show();
										$("div.error").focus();
										$("div.error span").html(
												"Please enter the junit weightage as numeric value.");
										returnVal = false;
										return returnVal;
									}
								} else {
									$("div.error").show();
									$("div.error").focus();
									$("div.error span").html("Please enter Test Code, File Name and Weightage.");
									returnVal = false;
									return returnVal;
								}
							}
							
					/* 		if (isNotNull($("#testWtID").val()) && !($.isNumeric($("#testWtID").val()))) {
								
							} */
							
							
							if(returnVal){
								$('#questionType').val("Programming");
								var carDataString = JSON.stringify($('#questionForm').serializeObject());
								var testWtValue = jQuery.parseJSON(carDataString).testWt;
								var weightage = testWtValue.toString().split(",");
							    var count = 0;
							    for (var i = 0; i < weightage.length; i++) {
								  if(weightage[i] != null && weightage[i] != " " && weightage[i] != "")
							      {
							      	count = count + parseInt(weightage[i]);
							      } 
								}
							    if (count == 100) {
							    	$.ajax({
										type : "POST",
										url : "/InterviewAssist/addQuestionSet",
										data : carDataString,
										contentType : "application/json; charset=utf-8",
										dataType : "json",
										success : function(data) {
											if (data.indexOf("Error") != -1) {
												$("div.error").show();
												$("div.error").focus();
												$("div.error span").html(data);
												returnVal = false;
												return returnVal;
											}else{
												
												alert(data);
												var editor = ace.edit("editor");
												editor.setValue("\n"+"\n"+"/* Use eclipse to format the code and then paste it here */");
												$("#questionForm").trigger("reset");
												$("div.error").hide();
											}
										},
										failure : function(errMsg) {
											alert(errMsg);//handle it in a proper way
										}
									});
							    } else {
							    	alert("Sum of all testcase weightage should be 100");
							    }
							}
						});

			});
</script>

<form action="#" id="questionForm">
<div class="error" tabindex="1" style="display:none;color: red;margin-top: 10px;">
	<img src="/InterviewAssist/resources/warning.gif" alt="Warning!" width="24" height="24" style="float:left; margin: -5px 10px 0px 0px;">
	<span></span>
	<br clear="all">
</div>
	<div id="accordion" style="margin-top: 10px;">
		<h3 style="margin-bottom: 2px;">Program Question</h3>
		<div id="questionSection" >

			<div class="form-row">
				<label class="levels">Programming Question &nbsp; <span class="required">*</span></label> 
				<textarea name="questionDesc" id="questionDescID" placeholder="Enter The Programming Question" cols="76" rows="1"></textarea>
			</div>
			<div class="emptyDIV"></div>
			<div class="form-row">
				<label class="levels">Example &nbsp;</label> 
				<textarea name="example" id="exampleID" placeholder="Enter The Example" cols="76" rows="1"></textarea>
			</div>

			<div class="emptyDIV"></div>
			<div class="form-row">
				<label class="levels">Technology &nbsp; <span class="required">*</span></label> <select name="questionTechnology" id="questionTechnologyID" class="drop_down">
					<option></option>
					<option>Java</option>
					<option>C</option>
					<option>C++</option>
					<option>C#</option>
					<option>.net</option>
				</select>
			</div>
			<div class="form-row">
				<label class="levels">Complexity &nbsp;<span class="required">*</span></label> <select name="questionComplexity" id="questionComplexityID" class="drop_down">
					<option></option>
					<option>Simple</option>
					<option>Medium</option>
					<option>Complex</option>
				</select>
			</div>
		<div class="emptyDIV"></div>
			<div class="form-row">
				<span id="validCheckBox" class="levels"> 
				<label class="levels">Applicability <span class="required">*</span></label> 
				<c:forEach items="${roleLST}" var="roleVal" varStatus="status">
					
					<input type="checkbox" id="validCheckBox_${status.index}" name="role" value="${roleVal.role}" placeholder="Software Engineeer"><c:out value="${roleVal.role}" />&nbsp;
				</c:forEach>
					<!-- <input type="checkbox" id="validCheckBox_1" name="role" value="SE" placeholder="Software Engineeer">SE 
					<input type="checkbox" id="validCheckBox_2" name="role"	value="SSE" placeholder="Senior Software Engineeer">SSE 
					<input type="checkbox" id="validCheckBox_3" name="role" value="ML" placeholder="Module Lead">ML 
					<input type="checkbox" id="validCheckBox_4" name="role" value="TL" placeholder="Technical Lead">TL 
					<input type="checkbox" id="validCheckBox_5" name="role" value="PL" placeholder="Project Lead">PL 
					<input type="checkbox" id="validCheckBox_6"	name="role" value="PM" placeholder="Project Manager">PM -->
				</span>
			</div>
			<!-- <div class="form-row">
				<input type="text" name="questionTime" id="questionTimeID" size="31" placeholder="Time Per Question" />
			</div> -->
</div>
		
		<h3>Add Pseudo Code</h3>
		<div>
		<div class="form-row">
				<label class="levels">File Name &nbsp; <span class="required">*</span></label> 
				<input type="text" name="fileName" id="fileNameID" class="field-divided" size="31" placeholder="Enter File Name" />
			</div>
		<div class="emptyDIV"></div>
		<div class="form-row">
			<label class="levels">Pseudo Code &nbsp; <span class="required">*</span></label>
			<div id="editor" style="overflow: scroll; position: relative; height: 220px; width: 700px;">
			/* Use eclipse to format the code and then paste it here */
			</div>
		</div>
		<input type="hidden" name="templateCode" id="templateCode" value="">
			
		</div>
		<h3>Add Test Case</h3>
			<div id="testCaseSection" >
				<div ID="testCaseSectionID">
					<div class="form-row">
						<label class="levels">Junit Test Case &nbsp; <span class="required">*</span></label> 
						<textarea name="testCode" id="testCodeID" class="field-long field-textarea" placeholder="Unit Test Code" cols="76" rows="2"></textarea>
					</div>
					<div class="emptyDIV"></div>
					<div class="form-row">
						<label class="levels">Junit File Name &nbsp; <span class="required">*</span></label>
						<input type="text" name="testFileNM" id="testFileNMID" class="field-divided" placeholder="Test File Name" />
					</div>
					<div class="form-row">
						<label class="levels">Junit Weightage &nbsp; <span class="required">*</span></label>
						<input type="text" name="testWt" id="testWtID" class="field-divided" placeholder="Test Weightage" />
					</div>
				</div>
				<div class="emptyDIV"></div>
					<input type="hidden" name="testCode" id="testCode" value="">
					<input type="hidden" name="testFileNM" id="testFileNM" value="">
					<input type="hidden" name="testWt" id="testWt" value="">
				<div>
					<input type="button" name="addTestCase" value="Add TestCase" id="addTestCaseID" />
				</div>
			</div>


	</div>
	<div class="emptyDIV"></div>
	<div>
		<input type="button" name="addQuestion" value="Add Question" id="addQuestion" /> 
		<input type="reset" name="cancel" value="Reset" id="resetID"/>
	</div>

	<!-- <input type="hidden" name="testCaseList" id="testCaseList" value=""> -->

<input type="hidden" name="totalMCQOpt" id="totalMCQOpt" value="2">
<input type="hidden" name="role" id="role"/>
<input type="hidden" name="questionType" id="questionType" value="Programming"/>
</form>


<script>
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/textmate");
    editor.session.setMode("ace/mode/java");
    
    
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
 	
</script>

</html>