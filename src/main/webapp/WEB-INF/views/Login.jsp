<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <title>InCTLI</title>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/login.css">
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/formElement.css">
 
 
 <script src="${pageContext.request.contextPath}/resources/jquery.js"></script>
 <script src="${pageContext.request.contextPath}/resources/jquery-ui.js"></script>
 <style>
.error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}

.msg {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #31708f;
	background-color: #d9edf7;
	border-color: #bce8f1;
}
</style>
      
      
      <script type="text/javascript">
        function reset()
        {
         /*    var cuid = document.getElementById('loginForm:cuid');
            cuid.select();
            cuid.focus();
            document.getElementById('reset').click(); */
        }
        
        $(document).ready(function () {
        	$('[name=login]').on('click', function() {
        	    valid = true;   
        	    if (valid && $('#uNameId').val() == '') {
        	        $('#validateuserfdiv').show();
        	        $('#validatediv').hide();
        	        $('#validate').text('Please enter your name');
        	        valid = false;
        	    }

        	    if (valid && $('#passwordId').val() == '') {
        	        $('#validateuserfdiv').show();
        	        $('#validatediv').hide();
        	        $('#validate').text('Please enter your password');
        	         valid = false;
        	    }    
        	    
        	    return valid;
        	}); 

        });
      </script>
    </head>
    <body onload="reset()">
    <header>
    	<div class="container">
			<img src="/InterviewAssist/resources/logo.png" alt="logo" />
		</div>
	</header>
	      <h1 align="center" class="htext"><font face=Calibri>Welcome to CenturyLink</font></h1>
			
				<section id="content">
		  <form id="loginForm" method="post" action="<c:url value='j_spring_security_check' />" enctype="application/x-www-form-urlencoded">

			<h1>InterviewAssit</h1>
			<c:if test="${not empty error}">
				<div class="error" id="validatediv"><p>${error}</p></div>
			</c:if>
			<c:if test="${not empty msg}">
				<div class="msg"><p>${msg}</p></div>
			</c:if>
			
			<div class="error" style="display:none" id="validateuserfdiv"><p id="validate"></p></div>

			<div class="form-row-login">
				<label >Username</label> 
				<input type="text" name="uName" id ="uNameId"  size="13"/>
			</div>

			<div class="form-row-login">
				<label>Password</label> 
                <input type="password" name="password" size="13" id="passwordId"/>                        
			</div>

			<input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
			<div class="form-row-login">			
				 <input type="submit" name="login" value="Login" />
				 <input type="reset" name="clear" value="Clear" />
				
			</div>
                    
                 
        
       </form></section>
       <footer>
       	<div id="footer" >
				<br> 2017 CenturyLink India, Inc. All Rights Reserved. 
		</div>
		</footer>    
       </div>
      	
      	
    
</body>
  
</html>