package com.centurylink.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

import org.apache.log4j.Logger;

public class CompileJava {

	private static Logger log = Logger.getLogger(CompileJava.class.getName());

	public static String compileCode(String FullfilePath, String path,
			boolean isTest, String progFileName, String contextpath)
			throws Exception {
		int index = FullfilePath.lastIndexOf("\\");
		String classPath = FullfilePath.substring(0, index);
		String fileName = FullfilePath.substring(index + 1,
				FullfilePath.length());
		index = fileName.lastIndexOf(".");
		String testFileName = null;
		String command = null;
		if (!isTest)
			command = "javac -cp src " + FullfilePath + " -d " + classPath;
		else {
			testFileName = fileName.substring(0, index);
			command = "javac -cp " + contextpath
					+ "WEB-INF\\lib\\junit-4.12.jar;src " + classPath + "\\"
					+ progFileName + " " + classPath + "\\" + testFileName
					+ ".java -d " + classPath;
		}
		System.out.println(command);
		Process pro = Runtime.getRuntime().exec(command);
		pro.waitFor();
		if (pro.exitValue() == 0) {
			return "SUCCESS";
		} else {
			return errorLines(pro.getErrorStream(), classPath);
		}

	}

	protected static String errorLines(InputStream ins, String classPath)
			throws Exception {
		String line = null;
		String errorList = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(ins));
		while ((line = in.readLine()) != null) {
			System.out.println(classPath);
			int index = line.lastIndexOf("\\");
			line = line.substring(index + 1, line.length());
			if (line.contains("Picked up"))
				line = null;
			if (errorList == null)
				errorList = line;
			if (line != null) {
				errorList = errorList + ", " + line;
			}
			System.out.println(line);
		}
		return errorList;
	}

	public static String compileCode(String relativeFilePath,
			Iterable<? extends JavaFileObject> compilationUnits)
			throws Exception {
		String error = null;
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
		StandardJavaFileManager stdFileManager = compiler
				.getStandardFileManager(null, null, null);
		try {
			stdFileManager.setLocation(StandardLocation.CLASS_OUTPUT,
					Arrays.asList(new File(relativeFilePath)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		JavaCompiler.CompilationTask task;
		task = compiler.getTask(null, stdFileManager, diagnostics, null, null,
				compilationUnits);
		if (!task.call()) {

			for (Diagnostic<?> diagnostic : diagnostics.getDiagnostics()) {
				log.error(String.format("Error on line %d in %s",
						diagnostic.getLineNumber(), diagnostic));
				error = error
						+ String.format("Error on line %d in %s",
								diagnostic.getLineNumber(), diagnostic);
			}
		} else {
			error = "SUCCESS";
		}
		return error;
	}

}