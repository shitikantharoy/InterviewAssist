package com.centurylink.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import javax.tools.JavaFileObject;

import org.apache.log4j.Logger;

import com.centurylink.constants.PortalConstants;

public class UploadDownloadFileHelper implements PortalConstants {

	private static Logger log = Logger.getLogger(UploadDownloadFileHelper.class
			.getName());

	private static final String basePath = "C:\\ICL\\Test_Files\\extractedfiles\\";

	public boolean writeFileToServer(String fileName, String fileContent) {
		log.info("inside writeFileToServer");
		FileWriter fw = null;
		PrintWriter pw = null;
		try {
			fw = new FileWriter(fileName);
			pw = new PrintWriter(fw);
			pw.write(fileContent);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pw != null)
				pw.close();

		}
		return false;

	}

	public String compileCode(String cuid, Integer qno, String fileName,
			String progrmCode, String fullFilePath) throws Exception {
		String output = "Compilation Failed";

		if (!writeFileToServer(fullFilePath, progrmCode)) {
			return "File Upload Failure";
		} else {
			int index = fullFilePath.lastIndexOf(File.separator);
			fullFilePath = fullFilePath.substring(0, index);
			if (fileName.contains(".java")) {
				index = fileName.indexOf(".java");
				fileName = fileName.substring(0, index);
			}
			JavaFileObject compilationUnit = new StringJavaFileObject(fileName,
					progrmCode);
			output = CompileJava.compileCode(fullFilePath,
					Arrays.asList(compilationUnit));
		}
		return output;
	}

	/*
	 * public String writeFile(String cuid, Integer qno, String fileName, String
	 * skeletonCode) { String uniquePath = cuid; int index = cuid.indexOf("_");
	 * String event = cuid.substring(index + 1); String fullFilePath = basePath
	 * + event + File.separator + uniquePath + File.separator + qno +
	 * File.separator + "src" + File.separator + fileName; File file2 = new
	 * File(fullFilePath); if (!writeFileToServer(fullFilePath, new
	 * String(skeletonCode))) log.info("File Upload Failure"); return
	 * fullFilePath; }
	 */

	public String createFile(String cuid, String event, Integer qno,
			String fileName, String skeletonCode, String testFileName,
			String junitCode, boolean isTest, String contextpath) {
		String uniquePath = cuid;
		if (!fileName.contains(".java")) {
			fileName = fileName + ".java";
		}
		if (isTest && !testFileName.contains(".java")) {
			testFileName = testFileName + ".java";
		}
		
		String fullFilePath = generateFilePath(event, uniquePath, fileName, qno);
		 /*basePath + event + File.separator + uniquePath
				+ File.separator + qno + File.separator + "src"
				+ File.separator + fileName;*/

		log.info(fullFilePath);
		File file = new File(basePath + event + File.separator + uniquePath
				+ File.separator + qno + File.separator + "src");
		if (file.mkdirs())
			log.info("File directory created ");

		try {
			File file2 = new File(fullFilePath);

			if (isTest) {
				String testFilePath = basePath + event + File.separator
						+ uniquePath + File.separator + qno + File.separator
						+ "src" + File.separator + testFileName;
				File file3 = new File(testFilePath);
				if (file3.createNewFile())
					log.info("File Created");
				if (!writeFileToServer(testFilePath, junitCode))
					log.info("Test File written");
				if (!compileTestClass(testFilePath, fileName, contextpath))
					log.info("Test Class compilation failed");
			} else {
				if (file2.createNewFile())
					log.info("File Created");
				if (!writeFileToServer(fullFilePath, new String(skeletonCode)))
					log.info("File Upload Failure");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fullFilePath;
	}

	public boolean compileTestClass(String fullFilePath, String progFileName,
			String contextpath) throws Exception {
		String output = null;
		output = CompileJava.compileCode(fullFilePath, basePath, true,
				progFileName, contextpath);
		if (SUCCESS.equalsIgnoreCase(output)) {
			return true;
		}
		return false;
	}
	
	public String generateFilePath(String eventName, String cuid, String fileName, int qNo){
		
		String fullFilePath = basePath + eventName + File.separator + cuid
				+ File.separator + qNo + File.separator + "src"
				+ File.separator + fileName;
		
		return fullFilePath;
	}
	
	
	public String readFile(String filePath) {
		BufferedReader br = null;
		FileReader fr = null;

		String sCurrentLine = "";
		StringBuffer buffer = new StringBuffer();
		try {

			fr = new FileReader(filePath);
			br = new BufferedReader(fr);

			br = new BufferedReader(new FileReader(filePath));
			while ((sCurrentLine = br.readLine()) != null) {
				buffer.append(sCurrentLine).append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return buffer.toString();
	}

	public String readCodeFromFile(String filePath) {
		BufferedReader br = null;
		FileReader fr = null;

		String sCurrentLine = "";
		StringBuffer buffer = new StringBuffer();
		try {

			fr = new FileReader(filePath);
			br = new BufferedReader(fr);

			br = new BufferedReader(new FileReader(filePath));
			while ((sCurrentLine = br.readLine()) != null) {
				buffer.append(sCurrentLine).append("<br>");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return buffer.toString();
	}
}
