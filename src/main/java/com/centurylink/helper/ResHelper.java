package com.centurylink.helper;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

/**
 * Static class used to retrieve property values
 *
 *
 */
public class ResHelper {

	private static Logger log = Logger.getLogger(ResHelper.class.getName());

	private static HashMap<String, String> resourceMap = new HashMap<String, String>();

	static {

		String key = null;

		ResourceBundle fBundle = ResourceBundle.getBundle("autograder");

		log.debug("ResHelper loaded:");

		for (Enumeration<?> keys = fBundle.getKeys(); keys.hasMoreElements();) {
			key = (String) keys.nextElement();

			log.debug("key - value " + key + "- " + fBundle.getObject(key));

			resourceMap.put(key, (String) fBundle.getObject(key));
		}

	}

	/**
	 * getResource
	 *
	 * @param propName
	 * @return property value
	 */
	public static String getResource(String propName) {
		return (String) resourceMap.get(propName);
	}

	/**
	 * getKeys
	 *
	 * @return all property keys in Iterator
	 */
	public static Iterator<String> getKeys() {
		return resourceMap.keySet().iterator();
	}

	/**
	 * ResHelper constructor
	 *
	 */
	public ResHelper() {
	}

}
