package com.centurylink.helper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.centurylink.constants.PortalConstants;
import com.centurylink.spring.controller.QuestionFormBean;

public class ReadQuestionExcel implements PortalConstants {

	public List<QuestionFormBean> readData(InputStream excelFile) {
		List<QuestionFormBean> questionList = new ArrayList<QuestionFormBean>();
		try {
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			iterator.next();
			while (iterator.hasNext()) {
				QuestionFormBean question = new QuestionFormBean();
				Row currentRow = iterator.next();
				question.setQuestionDesc(getValue(currentRow.getCell(0)));
				question.setQuestionComplexity(getValue(currentRow.getCell(1)));
				question.setQuestionTechnology(getValue(currentRow.getCell(2)));
				question.setQuestionType("MCQ");

				question.setMultipleOptionTxt1(getValue(currentRow.getCell(4)));
				question.setMultipleOptionTxt2(getValue(currentRow.getCell(5)));
				question.setMultipleOptionTxt3(getValue(currentRow.getCell(6)));
				question.setMultipleOptionTxt4(getValue(currentRow.getCell(7)));
				question.setMultipleOptionTxt5(getValue(currentRow.getCell(8)));
				question.setSelectedMultiAnsOpt(getValue(currentRow.getCell(9)));
				question.setOptionTypSelect(getValue(currentRow.getCell(10)));

				List<String> roleList = new ArrayList<String>();
				if (getValue(currentRow.getCell(9)).trim()
						.equalsIgnoreCase("Y")) {
					roleList.add("Trainee");
				}
				if (getValue(currentRow.getCell(10)).trim().equalsIgnoreCase(
						"Y")) {
					roleList.add("SE");
				}
				if (getValue(currentRow.getCell(11)).trim().equalsIgnoreCase(
						"Y")) {
					roleList.add("SSE");
				}
				if (getValue(currentRow.getCell(12)).trim().equalsIgnoreCase(
						"Y")) {
					roleList.add("ML");
				}
				if (getValue(currentRow.getCell(13)).trim().equalsIgnoreCase(
						"Y")) {
					roleList.add("TL");
				}
				if (getValue(currentRow.getCell(14)).trim().equalsIgnoreCase(
						"Y")) {
					roleList.add("PL");
				}
				String[] roleArray = new String[roleList.size()];
				for (int i = 0; i < roleList.size(); i++) {
					roleArray[i] = roleList.get(i);
				}
				question.setRole(roleArray);
				questionList.add(question);
				workbook.close();

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return questionList;
	}

	private String getValue(Cell cell) {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();
		case Cell.CELL_TYPE_BOOLEAN:
			return String.valueOf(cell.getBooleanCellValue());
		case Cell.CELL_TYPE_NUMERIC:
			return String.valueOf(Double.valueOf(cell.getNumericCellValue())
					.intValue());
		}
		return "";
	}

}