package com.centurylink.spring.form;

import java.util.Calendar;
import java.util.List;

import com.centurylink.spring.model.Answer;
import com.centurylink.spring.model.Question;

public class CandidateForm {

	private String name;

	private String email;

	private String phone;
	
	private String action;
	
	private String event;
	
	private String optionType;
	
	private Question displayQuestion;
	
	private List<OptionForm> options;
	
	private boolean lastQuestion;
	
	private int currentIndex;
	
	private String answerOptions;
	
	private String programCode;
	
	private int displayIndex;
	
	private Calendar loginTime = Calendar.getInstance();
	
	public int getDisplayIndex() {
		return currentIndex + 1;
	}

	public void setDisplayIndex(int displayIndex) {
		this.displayIndex = displayIndex;
	}

	public int getLoginTimeHour() {
		return loginTime.get(Calendar.HOUR_OF_DAY);
	}
	
	public int getLoginTimeMinutes() {
		return loginTime.get(Calendar.MINUTE);
	}
	
	public int getLoginTimeSecs() {
		return loginTime.get(Calendar.SECOND);
	}
	
	public Calendar getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Calendar loginTime) {
		this.loginTime = loginTime;
	}

	public String getAnswerOptions() {
		return answerOptions;
	}

	public void setAnswerOptions(String answerOptions) {
		this.answerOptions = answerOptions;
	}

	public Question getDisplayQuestion() {
		return displayQuestion;
	}

	public void setDisplayQuestion(Question displayQuestion) {
		this.displayQuestion = displayQuestion;
	}

	public boolean isLastQuestion() {
		return lastQuestion;
	}

	public void setLastQuestion(boolean lastQuestion) {
		this.lastQuestion = lastQuestion;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	public String getOptionType() {
		return optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	List<TestForm> testFormList;

	public List<TestForm> getTestFormList() {
		return testFormList;
	}

	public void setTestFormList(List<TestForm> testFormList) {
		this.testFormList = testFormList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public List<OptionForm> getOptions() {
		return options;
	}

	public void setOptions(List<OptionForm> options) {
		this.options = options;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	
}
