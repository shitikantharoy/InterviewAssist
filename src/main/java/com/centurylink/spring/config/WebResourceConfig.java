package com.centurylink.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.centurylink.spring.security.SecurityInterceptor;

@Configuration
@EnableWebMvc
public class WebResourceConfig extends WebMvcConfigurerAdapter {
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(new String[] { "/resources/**" })
				.addResourceLocations(
						new String[] { "/WEB-INF/resources/css/",
								"/WEB-INF/resources/images/",
								"/WEB-INF/resources/script/",
								"/WEB-INF/resources/others/" });
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new SecurityInterceptor())
				.addPathPatterns("/*")
				.excludePathPatterns("/", "/login", "/event",
						"/renderquestions", "/starttest", "/instructions");
	}

	@Bean(name = "multipartResolver")
	public StandardServletMultipartResolver resolver() {
		return new StandardServletMultipartResolver();
	}

}