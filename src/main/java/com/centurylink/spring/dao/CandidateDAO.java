package com.centurylink.spring.dao;

import com.centurylink.spring.model.Candidate;
import com.centurylink.spring.model.User;


public interface CandidateDAO {

	public Candidate get(String uniqueid);
	
	public Candidate get(String uniqueid, String event);
	
	public void saveOrUpdate(Candidate candidate);
	
	public void create(Candidate candidate);
	
	public void delete(String uniqueid);

	
}
