package com.centurylink.spring.dao;

import com.centurylink.spring.model.Question;

import java.util.List;

public interface QuestionDAO {
	List<Question> findAllQuestions();

	Question get(int arg0);

	void saveOrUpdate(Question arg0);

	void insert(Question arg0);

	void delete(int arg0);
	
	public List<Question> getQuestionsByCustomQuery(String hql);
	
	public List<Question> getQuestionsByCriteria(Question question);
}