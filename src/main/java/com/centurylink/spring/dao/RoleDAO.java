package com.centurylink.spring.dao;

import java.util.List;

import com.centurylink.spring.model.Role;

public interface RoleDAO {
	Role get(String arg0);

	void saveOrUpdate(Role arg0);

	void delete(String arg0);
	
	public List<Role> list();
	
	Role getRoleById(int roleID);
}