package com.centurylink.spring.dao;

import java.util.List;

import com.centurylink.spring.model.CandidateAnswer;


public interface CandidateAnswerDAO {

	//public Answer get(Integer qNo);
	
	public List<CandidateAnswer> get(Integer candidateId);
	
	public void saveOrUpdate(CandidateAnswer answer);
	
	public void insert(CandidateAnswer candidateAnswer);
	
	//public void delete(Integer qNo);
	
}
