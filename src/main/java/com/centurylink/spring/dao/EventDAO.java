package com.centurylink.spring.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.model.Event;


public interface EventDAO {

	public Event get(String eventName);
	
	public Event getEventByID(int eventID);
	
	public void saveOrUpdate(Event event);
	
	public void create(Event event);
	
	public void delete(Event eventName);
	
	public List<Event> findAllEvents();
	
	public ArrayList<String> getEventNames(String eventName );
	
}
