package com.centurylink.spring.dao;

import com.centurylink.spring.model.Customer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CustomerDAO {
	private static List<Customer> customers;

	public CustomerDAO() {
		customers = new ArrayList();
		customers.add(new Customer(101L, "John", "Doe", "djohn@gmail.com", "121-232-3435"));
		customers.add(new Customer(201L, "Russ", "Smith", "sruss@gmail.com", "343-545-2345"));
		customers.add(new Customer(301L, "Kate", "Williams", "kwilliams@gmail.com", "876-237-2987"));
		customers.add(new Customer(System.currentTimeMillis(), "Viral", "Patel", "vpatel@gmail.com", "356-758-8736"));
	}

	/* Testing file commit */
	
	public List list() {
		return customers;
	}

	public Customer get(Long id) {
		Iterator i$ = customers.iterator();

		Customer c;
		do {
			if (!i$.hasNext()) {
				return null;
			}

			c = (Customer) i$.next();
		} while (!c.getId().equals(id));

		return c;
	}

	public Customer create(Customer customer) {
		customer.setId(Long.valueOf(System.currentTimeMillis()));
		customers.add(customer);
		return customer;
	}

	public Long delete(Long id) {
		Iterator i$ = customers.iterator();

		Customer c;
		do {
			if (!i$.hasNext()) {
				return null;
			}

			c = (Customer) i$.next();
		} while (!c.getId().equals(id));

		customers.remove(c);
		return id;
	}

	public Customer update(Long id, Customer customer) {
		Iterator i$ = customers.iterator();

		Customer c;
		do {
			if (!i$.hasNext()) {
				return null;
			}

			c = (Customer) i$.next();
		} while (!c.getId().equals(id));

		customer.setId(c.getId());
		customers.remove(c);
		customers.add(customer);
		return customer;
	}
}