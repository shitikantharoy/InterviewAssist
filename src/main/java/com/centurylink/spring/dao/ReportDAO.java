package com.centurylink.spring.dao;

import java.util.List;
import java.util.Map;

import com.centurylink.spring.model.Report;

public interface ReportDAO {
	public List<Report> getReportByCustomQuery(String hql, String user,
			String event, String role, String eventStartDate,
			String eventEndDate);

	public Map<String, Integer> getComplexitymap();
}
