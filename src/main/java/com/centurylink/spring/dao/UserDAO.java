package com.centurylink.spring.dao;

import java.util.List;

import com.centurylink.spring.model.User;

public interface UserDAO {
	User get(String arg0);

	void saveOrUpdate(User arg0);

	void delete(String arg0);
	
	List<User> findAllUsers();
	
	User getUserByID(int userID);
}