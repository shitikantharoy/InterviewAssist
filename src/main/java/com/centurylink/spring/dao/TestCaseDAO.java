package com.centurylink.spring.dao;

import com.centurylink.spring.model.TestCase;
import java.util.List;

public interface TestCaseDAO {
	List<TestCase> get(Integer arg0);
	
	TestCase getTestCaseByID(Integer ID);

	void saveOrUpdate(TestCase arg0);

	void insert(TestCase arg0);

	void delete(Integer arg0);
	
	void update(TestCase testCase);
}