package com.centurylink.spring.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.centurylink.spring.dao.CandidateAnswerDAO;
import com.centurylink.spring.model.CandidateAnswer;

public class CandidateAnswerDAOImpl implements CandidateAnswerDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public CandidateAnswerDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public void saveOrUpdate(CandidateAnswer answer) {
		sessionFactory.getCurrentSession().saveOrUpdate(answer);

	}

	@Override
	@Transactional
	public void insert(CandidateAnswer candidateAnswer) {
		sessionFactory.getCurrentSession().persist(candidateAnswer);

	}

	@Override
	@Transactional
	public List<CandidateAnswer> get(Integer candidateId) {
		String hql = "from CandidateAnswer where candidate_id=" + candidateId
				+ " order by total_test_case asc";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		List<CandidateAnswer> listCandidates = query.list();
		return listCandidates;
	}

}
