package com.centurylink.spring.dao.impl;

import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.model.Event;
import com.centurylink.spring.model.Question;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NamedQuery;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class QuestionDAOImpl implements QuestionDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public QuestionDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public List<Question> findAllQuestions() {
		Query query = this.sessionFactory.getCurrentSession().getNamedQuery("Question.findAll");
		List questionList = query.list();
		return questionList;
	}

	@Transactional
	public Question get(int id) {
		String hql = "from Question where questionNo=" + id;
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		List listQuestion = query.list();
		return listQuestion != null && !listQuestion.isEmpty() ? (Question) listQuestion.get(0) : null;
	}
	

	@Transactional
	public void saveOrUpdate(Question question) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(question); //Roy's code
		//sessionFactory.getCurrentSession().persist(question);
	}

	@Transactional
	public void insert(Question question) {
		this.sessionFactory.getCurrentSession().persist(question);
	}

	@Transactional
	public void delete(int questionNo) {
		Question questionToDelete = new Question();
		questionToDelete.setQuestionNo(questionNo);
		this.sessionFactory.getCurrentSession().delete(questionToDelete);
	}
	
	@Override
	@Transactional
	public List<Question> getQuestionsByCriteria(Question question) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Question.class);
		Example questionE = Example.create(question);
		questionE.excludeZeroes();
		criteria.add(questionE);
		List<Question> questionList = criteria.list();
		
		return questionList;
	}
	
	@Override
	@Transactional
	public List<Question> getQuestionsByCustomQuery(String hql) {
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<Question> listQuestion = (List<Question>) query.list();
		
		return listQuestion;
	}
}