package com.centurylink.spring.dao.impl;

import com.centurylink.spring.dao.ApplicabilityDAO;
import com.centurylink.spring.model.Applicability;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ApplicabilityDAOImpl implements ApplicabilityDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public ApplicabilityDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public Applicability get(Integer qNo) {
		String hql = "FROM Applicability A WHERE A.questionNo = :questionNo";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("questionNo", qNo);
		List applicabilityLst = query.list();
		return applicabilityLst != null && !applicabilityLst.isEmpty() ? (Applicability) applicabilityLst.get(0) : null;
	}

	@Transactional
	public void saveOrUpdate(Applicability applicability) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(applicability);
	}

	@Transactional
	public void insert(Applicability applicability) {
		this.sessionFactory.getCurrentSession().persist(applicability);
	}

	@Transactional
	public void delete(Integer qNo) {
		Applicability applicability = new Applicability();
		//applicability.setQNo(qNo.intValue());
		this.sessionFactory.getCurrentSession().delete(applicability);
	}
}