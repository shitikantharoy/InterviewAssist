package com.centurylink.spring.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.centurylink.spring.dao.ReportDAO;
import com.centurylink.spring.model.Report;

@Repository
public class ReportDAOImpl implements ReportDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private Map<String, Integer> complexityMap;

	public ReportDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public List<Report> getReportByCustomQuery(String hql, String user,
			String event, String role, String eventStartDate,
			String eventEndDate) {
		this.complexityMap = createComplexityMap();
		Report report = null;
		Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
		if (!StringUtils.isEmpty(user)) {
			query.setParameter("user", user);
		}
		if (!StringUtils.isEmpty(event)) {
			query.setParameter("event", event);
		}
		if (!StringUtils.isEmpty(role)) {
			query.setParameter("role", role);
		}
	/*	if (!StringUtils.isEmpty(eventStartDate)) {
			query.setParameter("eventStartDate",getFormattedDate(eventStartDate));
		}
		if (!StringUtils.isEmpty(eventEndDate)) {
			query.setParameter("eventEndDate", getFormattedDate(eventEndDate));
		}*/

		query.setReadOnly(true);

		/*
		 * List outputQueryList = query.list(); for (int i = 0; i <
		 * outputQueryList.size(); i++) {
		 * 
		 * }
		 */

		ScrollableResults results = query.scroll(ScrollMode.SCROLL_SENSITIVE);
		List<Report> listReport = new ArrayList<Report>();

		// iterate over results

		while (results.next()) {
			report = new Report();
			report.setCandidateCuid(results.get()[0].toString());
			report.setEventName(results.get()[1].toString());
			report.setEventDate(results.get()[2].toString());
			report.setRole(results.get()[3].toString());
			if (results.get().length > 4) {
				
				report.setqNo(results.get()[5].toString());
				if (results.get()[7] != null)
					report.setTestCasesPassed(String.valueOf(results.get()[7]));
				;
				if (results.get()[8] != null)
					report.setTotalTestCases(String.valueOf(results.get()[8]));
				if (results.get()[6] != null
						&& complexityMap.containsKey(results.get()[6]
								.toString()))
					report.setComplexity(String.valueOf(results.get()[6]));
				if (results.get()[10] != null) {
					report.setProgramIndicator(true);
					report.setQuestionType("Programming Question");
					report.setCorrectFlag("NA");
				}
				if (results.get()[10] == null) {
					report.setQuestionType("MCQ Question");
					report.setTotalTestCases("NA");
					report.setTestCasesPassed("NA");

				}
				if (results.get()[4] != null && results.get()[4].equals("Yes")) {
					report.setCorrectFlag("Yes");
				}
				if (results.get()[4] != null && !results.get()[4].equals("Yes")) {
					report.setCorrectFlag("No");
				}
				if (results.get()[9] != null
						&& results.get()[10] != null
						&& Double.parseDouble(results.get()[9].toString()) != 0d) {
					report.setProgrammingScore((Double.parseDouble(results
							.get()[10].toString()) * 100)
							/ Double.parseDouble(results.get()[9].toString()));
				}
			}
			listReport.add(report);
			report = null;
		}
		results.close();
		return listReport;
	}

	private Map<String, Integer> createComplexityMap() {
		Map<String, Integer> comMap = new HashMap<String, Integer>();
		String queryGetWeightage = "Select * from complexity_map";
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				queryGetWeightage);
		query.setReadOnly(true);
		ScrollableResults results = query.scroll(ScrollMode.SCROLL_SENSITIVE);
		// iterate over results

		while (results.next()) {
			comMap.put(results.get()[0].toString(),
					Integer.parseInt(results.get()[1].toString()));
		}
		results.close();
		return comMap;
	}

	@Override
	public Map<String, Integer> getComplexitymap() {
		return this.complexityMap;
	}

	private String getFormattedDate(String date) {

		final String NEW_FORMAT = "yyyy/MM/dd";

		SimpleDateFormat sdf = new SimpleDateFormat(NEW_FORMAT);
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sdf.format(d);

	}

}
