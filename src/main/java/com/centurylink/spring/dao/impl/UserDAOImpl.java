package com.centurylink.spring.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.UserDAO;
import com.centurylink.spring.model.User;

@Repository
public class UserDAOImpl implements UserDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public UserDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public User get(String cuid) {
		String hql = "from User where cuid='" + cuid + "'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		List listQuestion = query.list();
		return listQuestion != null && !listQuestion.isEmpty() ? (User) listQuestion.get(0) : null;
	}

	@Transactional
	public void saveOrUpdate(User user) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(user);
	}

	@Transactional
	public void delete(String cuid) {
		User user = new User();
		user.setCuid(cuid);
		this.sessionFactory.getCurrentSession().delete(user);
	}
	
	@Transactional
	public List<User> findAllUsers() {
		Query query = this.sessionFactory.getCurrentSession().getNamedQuery("user.findAll");
		List eventList = query.list();
		return eventList;
	}
	
	@Transactional
	public User getUserByID(int userID) {
				
		Query query = sessionFactory.getCurrentSession().getNamedQuery("user.findByUserID");
		query.setParameter("user_id", userID);
		
		@SuppressWarnings("unchecked")
		List<User> listUser = (List<User>) query.list();
		
		if (listUser != null && !listUser.isEmpty()) {
			return listUser.get(0);
		}
		
		return null;
	}
}