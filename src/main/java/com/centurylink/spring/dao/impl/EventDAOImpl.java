package com.centurylink.spring.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.EventDAO;
import com.centurylink.spring.model.Candidate;
import com.centurylink.spring.model.Event;
import com.centurylink.spring.model.Question;

public class EventDAOImpl implements EventDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EventDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public Event get(String eventName) {
				
		Query query = sessionFactory.getCurrentSession().getNamedQuery("event.findByEventName");
		query.setParameter("event_name", eventName);
		
		@SuppressWarnings("unchecked")
		List<Event> listEvent = (List<Event>) query.list();
		
		if (listEvent != null && !listEvent.isEmpty()) {
			return listEvent.get(0);
		}
		
		return null;
	}
	
	
	@Override
	@Transactional
	public Event getEventByID(int eventID) {
				
		Query query = sessionFactory.getCurrentSession().getNamedQuery("event.findByEventID");
		query.setParameter("interview_event_id", eventID);
		
		@SuppressWarnings("unchecked")
		List<Event> listEvent = (List<Event>) query.list();
		
		if (listEvent != null && !listEvent.isEmpty()) {
			return listEvent.get(0);
		}
		
		return null;
	}
	

	@Override
	@Transactional
	public void saveOrUpdate(Event event) {
		sessionFactory.getCurrentSession().saveOrUpdate(event);
		
	}

	@Override
	@Transactional
	public void create(Event event) {
		sessionFactory.getCurrentSession().persist(event);
	}

	@Override
	@Transactional
	public void delete(Event event) {
		sessionFactory.getCurrentSession().delete(event);
	}
	
	
	@Transactional
	public List<Event> findAllEvents() {
		Query query = this.sessionFactory.getCurrentSession().getNamedQuery("event.findAll");
		List eventList = query.list();
		return eventList;
	}
	
	
	@Transactional
	public ArrayList<String> getEventNames(String eventName ) {
		ArrayList<String> eventNameLst = new ArrayList<String>();
		String hql = "from Event where eventStatus='ACTIVE' and  event_name like '"+eventName+"%'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		List<Event> listEvent = query.list();
		for(Event event:listEvent){
			eventNameLst.add(event.getEventName());
		}
		
		return eventNameLst;
	}

}
