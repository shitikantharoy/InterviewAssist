package com.centurylink.spring.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.TestCaseDAO;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.TestCase;

@Repository
public class TestCaseDAOImpl implements TestCaseDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public TestCaseDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public List<TestCase> get(Integer qNo) {
		String hql = "FROM TestCase T WHERE T.QNo = :questionNo";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("questionNo", qNo);
		List listQuestion = query.list();
		return listQuestion;
	}
	

	@Transactional
	public TestCase getTestCaseByID(Integer ID) {
		
		TestCase testCase = new TestCase();
		String hql = "FROM TestCase T WHERE T.testCaseId = :testCaseID";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("testCaseID", ID);
		if(query.list() != null)
			testCase = (TestCase)query.list().get(0);
		return testCase;
	}

	@Transactional
	public void saveOrUpdate(TestCase testCase) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(testCase);
	}

	@Transactional
	public void insert(TestCase testCase) {
		this.sessionFactory.getCurrentSession().persist(testCase);
		this.sessionFactory.getCurrentSession().flush();
	}

	@Transactional
	public void delete(Integer qNo) {
		Question question = new Question();
		question.setQuestionNo(qNo.intValue());
		TestCase testCase = new TestCase();
		testCase.setQuestion(question);
		this.sessionFactory.getCurrentSession().delete(testCase);
	}
	
	@Transactional
	public void update(TestCase testCase) {
		this.sessionFactory.getCurrentSession().update(testCase);
	}
}