package com.centurylink.spring.dao.impl;

import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.model.Role;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class RoleDAOImpl implements RoleDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public RoleDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public Role get(String role) {
		String hql = "FROM Role R WHERE R.role = :role";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("role", role);
		List roleLst = query.list();
		return roleLst != null && !roleLst.isEmpty() ? (Role) roleLst.get(0) : null;
	}
	
	@Transactional
	public Role getRoleById(int roleId) {
		String hql = "FROM Role R WHERE R.roleId = :roleId";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("roleId", roleId);
		List roleLst = query.list();
		return roleLst != null && !roleLst.isEmpty() ? (Role) roleLst.get(0) : null;
	}

	@Transactional
	public void saveOrUpdate(Role role) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(role);
	}

	@Transactional
	public void delete(String role) {
		Role roleObj = new Role();
		roleObj.setRole(role);
		this.sessionFactory.getCurrentSession().delete(roleObj);
	}
	
	@Override
	@Transactional
	public List<Role> list() {
		String hql = "from Role";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<Role> listRole = (List<Role>) query.list();
		return listRole;
	}
}