package com.centurylink.spring.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.AnswerDAO;
import com.centurylink.spring.model.Answer;
import com.centurylink.spring.model.Question;

@Repository
public class AnswerDAOImpl implements AnswerDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public AnswerDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public Answer get(Integer qNo) {
		String hql = "FROM Answer A WHERE A.QNo = :questionNo";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger("questionNo", qNo.intValue());
		List applicabilityLst = query.list();
		return applicabilityLst != null && !applicabilityLst.isEmpty() ? (Answer) applicabilityLst
				.get(0) : null;
	}

	@Transactional
	public void saveOrUpdate(Answer answer) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(answer);
	}

	@Transactional
	public void insert(Answer answer) {
		this.sessionFactory.getCurrentSession().persist(answer);
	}

	@Transactional
	public void delete(Integer qNo) {
		Answer answer = new Answer();
		Question question = new Question();
		question.setQuestionNo(qNo.intValue());
		answer.setQuestion(question);
		this.sessionFactory.getCurrentSession().delete(answer);
	}

}