package com.centurylink.spring.controller;

public class EventFormBean {

	private String eventStartDate;
	private String eventEndDate;
	private String eventStartTime;
	private String eventEndTime;
	private String eventName;
	private String eventTechnology;
	private String eventStatus;
	private String simpleMcq;
	private String mediumMcq;
	private String complexMcq;
	private String simplePgm;
	private String mediumPgm;
	private String complexPgm;
	private String applicability;
	private String eventDuration;

	private String interviewEventId;
	private String eventUrl;
	private String jqGrid_id;

	public String getJqGrid_id() {
		return jqGrid_id;
	}

	public void setJqGrid_id(String jqGrid_id) {
		this.jqGrid_id = jqGrid_id;
	}

	public String getEventStartTime() {
		return eventStartTime;
	}

	public void setEventStartTime(String eventStartTime) {
		this.eventStartTime = eventStartTime;
	}

	public String getEventEndTime() {
		return eventEndTime;
	}

	public void setEventEndTime(String eventEndTime) {
		this.eventEndTime = eventEndTime;
	}

	public String getInterviewEventId() {
		return interviewEventId;
	}

	public void setInterviewEventId(String interviewEventId) {
		this.interviewEventId = interviewEventId;
	}

	public String getEventUrl() {
		return eventUrl;
	}

	public void setEventUrl(String eventUrl) {
		this.eventUrl = eventUrl;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public String getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public String getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventTechnology() {
		return eventTechnology;
	}

	public void setEventTechnology(String eventTechnology) {
		this.eventTechnology = eventTechnology;
	}

	public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

	public String getSimpleMcq() {
		return simpleMcq;
	}

	public void setSimpleMcq(String simpleMcq) {
		this.simpleMcq = simpleMcq;
	}

	public String getMediumMcq() {
		return mediumMcq;
	}

	public void setMediumMcq(String mediumMcq) {
		this.mediumMcq = mediumMcq;
	}

	public String getComplexMcq() {
		return complexMcq;
	}

	public void setComplexMcq(String complexMcq) {
		this.complexMcq = complexMcq;
	}

	public String getSimplePgm() {
		return simplePgm;
	}

	public void setSimplePgm(String simplePgm) {
		this.simplePgm = simplePgm;
	}

	public String getMediumPgm() {
		return mediumPgm;
	}

	public void setMediumPgm(String mediumPgm) {
		this.mediumPgm = mediumPgm;
	}

	public String getComplexPgm() {
		return complexPgm;
	}

	public void setComplexPgm(String complexPgm) {
		this.complexPgm = complexPgm;
	}

	public String getEventDuration() {
		return eventDuration;
	}

	public void setEventDuration(String eventDuration) {
		this.eventDuration = eventDuration;
	}

}