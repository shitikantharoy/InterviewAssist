package com.centurylink.spring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.centurylink.helper.UploadDownloadFileHelper;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.Report;
import com.centurylink.spring.model.Role;
import com.centurylink.spring.model.User;
import com.centurylink.spring.security.CustomUserDetailsService;

@Controller
public class HomeController {

	@Autowired
	private QuestionDAO questionDAO;

	@Autowired
	private RoleDAO roleDAO;

	@RequestMapping({ "/" })
	public ModelAndView handleRequest() {
		ModelAndView model = new ModelAndView("Login");
		return model;
	}

	@RequestMapping({ "/validate" })
	public String lDapLogin(HttpServletRequest request, ModelMap model) {
		String viewName = null;
		User user = CustomUserDetailsService.getCurrentUser();

		HttpSession httpSession = request.getSession();
		if (user != null) {
			httpSession.setAttribute("user", user);
		}
		model.addAttribute("userSelect", "");
		viewName = "index";

		return viewName;
	}

	@RequestMapping({ "/user" })
	public ModelAndView user(
			@RequestParam(value = "val", required = false) String val) {
		ModelAndView model = new ModelAndView("index");
		model.addObject("userSelect", val);
		return model;
	}

	@RequestMapping({ "/question" })
	public ModelAndView question(
			@RequestParam(value = "val", required = false) String val) {
		ModelAndView model = new ModelAndView("index");
		List<Role> roleLst = roleDAO.list();
		model.addObject("userSelect", val);
		model.addObject("roleLST", roleLst);
		return model;
	}

	@RequestMapping({ "/report" })
	public ModelAndView report(
			@RequestParam(value = "val", required = false) String val) {
		ModelAndView model = new ModelAndView("index");
		List<Role> roleLst = roleDAO.list();
		model.addObject("roleLST", roleLst);
		model.addObject("userSelect", val);
		return model;
	}

	/*
	 * @RequestMapping({ "/reportDetails" }) public ModelAndView reportDetails(
	 * 
	 * @RequestParam(value = "val", required = false) String val) { ModelAndView
	 * model = new ModelAndView("reportDetails"); model.addObject("userSelect",
	 * val); return model; }
	 */

	@RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("loggedinuser", getPrincipal());
		return "accessDenied";
	}

	@GetMapping({ "/getMCQProgramQustionAns" })
	public ModelAndView getMCQProgramQustionAns(
			@RequestParam(value = "qNo", required = false) String qNo,
			@RequestParam(value = "userName", required = false) String userName,
			@RequestParam(value = "eventName", required = false) String eventName) {

		Report report = new Report();
		try {
			Question question = new Question();
			if (!StringUtils.isEmpty(qNo)) {
				question = questionDAO.get(new Integer(qNo).intValue());
			}

			report.setQuestionDesc(question.getQuestionDesc());
			report.setQuestionType(question.getQuestionType());
			UploadDownloadFileHelper downloadFileHelper = new UploadDownloadFileHelper();
			String filePath = downloadFileHelper.generateFilePath(eventName,
					userName, question.getFileName(), question.getQuestionNo());
			if (!filePath.contains(".java")) {
				filePath = filePath + ".java";
			}
			String code = downloadFileHelper.readCodeFromFile(filePath);

			report.setCode(code);

		} catch (Exception e) {

		}
		ModelAndView model = new ModelAndView("reportDetails");
		model.addObject("report", report);
		return model;
	}

	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();

		if (error != null && error.equals("sessionExpiredDuplicateLogin")) {
			model.addObject("error", "Your session is expired!");
		} else if (error != null && error.equals("alreadyLogin")) {
			model.addObject("error", "User already login!");
		} else if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			HttpSession session = request.getSession(false);
			if (session != null) {
				User user = (User) session.getAttribute("user");
				if (user != null) {
					CustomUserDetailsService.removeUser(user.getCuid());
				}
				session.invalidate();
			}
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("Login");

		return model;
	}
}