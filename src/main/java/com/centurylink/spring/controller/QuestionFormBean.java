package com.centurylink.spring.controller;

import java.util.List;

public class QuestionFormBean {
	private String id;
	private String jqGrid_id;
	private String questionDesc;
	private String questionType;
	private String questionComplexity;
	private String questionTechnology;
	private String questionTime;
	private String questionActive;
	private String fileName;
	private String[] role;
	private String[] testCode;
	private String[] testFileNM;
	private String[] testWt;
	private String multipleOptionTxt1;
	private String multipleOptionTxt2;
	private String multipleOptionTxt3;
	private String multipleOptionTxt4;
	private String multipleOptionTxt5;
	private String singleOptionTxt1;
	private String singleOptionTxt2;
	private String singleOptionTxt3;
	private String singleOptionTxt4;
	private String singleOptionTxt5;
	private String singleChoiceAns;
	private String optionTypSelect;
	private String selectedMultiAnsOpt;
	private String example;
	private String templateCode;
	private String testCase;
	private String testFileName;
	private String testCaseWeightage;
	private String Qno;
	
	private String question;
	private String questionComp;
	private String questionTech;
	private String qTime;
	private String questionAct;
	private String oper;
	
	private List <TestCaseFormBean> testCaseList;
	
	
	

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getQno() {
		return this.Qno;
	}

	public void setQno(String qno) {
		this.Qno = qno;
	}

	public String getTestCase() {
		return this.testCase;
	}

	public void setTestCase(String testCase) {
		this.testCase = testCase;
	}

	public String getTestFileName() {
		return this.testFileName;
	}

	public void setTestFileName(String testFileName) {
		this.testFileName = testFileName;
	}

	public String getTestCaseWeightage() {
		return this.testCaseWeightage;
	}

	public void setTestCaseWeightage(String testCaseWeightage) {
		this.testCaseWeightage = testCaseWeightage;
	}

	public String getJqGrid_id() {
		return this.jqGrid_id;
	}

	public void setJqGrid_id(String jqGrid_id) {
		this.jqGrid_id = jqGrid_id;
	}

	public String getExample() {
		return this.example;
	}

	public void setExample(String example) {
		this.example = example;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOptionTypSelect() {
		return this.optionTypSelect;
	}

	public void setOptionTypSelect(String optionTypSelect) {
		this.optionTypSelect = optionTypSelect;
	}

	public String getMultipleOptionTxt1() {
		return this.multipleOptionTxt1;
	}

	public void setMultipleOptionTxt1(String multipleOptionTxt1) {
		this.multipleOptionTxt1 = multipleOptionTxt1;
	}

	public String getMultipleOptionTxt2() {
		return this.multipleOptionTxt2;
	}

	public void setMultipleOptionTxt2(String multipleOptionTxt2) {
		this.multipleOptionTxt2 = multipleOptionTxt2;
	}

	public String getMultipleOptionTxt3() {
		return this.multipleOptionTxt3;
	}

	public void setMultipleOptionTxt3(String multipleOptionTxt3) {
		this.multipleOptionTxt3 = multipleOptionTxt3;
	}

	public String getMultipleOptionTxt4() {
		return this.multipleOptionTxt4;
	}

	public void setMultipleOptionTxt4(String multipleOptionTxt4) {
		this.multipleOptionTxt4 = multipleOptionTxt4;
	}

	public String getMultipleOptionTxt5() {
		return this.multipleOptionTxt5;
	}

	public void setMultipleOptionTxt5(String multipleOptionTxt5) {
		this.multipleOptionTxt5 = multipleOptionTxt5;
	}

	public String getSingleOptionTxt1() {
		return this.singleOptionTxt1;
	}

	public void setSingleOptionTxt1(String singleOptionTxt1) {
		this.singleOptionTxt1 = singleOptionTxt1;
	}

	public String getSingleOptionTxt2() {
		return this.singleOptionTxt2;
	}

	public void setSingleOptionTxt2(String singleOptionTxt2) {
		this.singleOptionTxt2 = singleOptionTxt2;
	}

	public String getSingleOptionTxt3() {
		return this.singleOptionTxt3;
	}

	public void setSingleOptionTxt3(String singleOptionTxt3) {
		this.singleOptionTxt3 = singleOptionTxt3;
	}

	public String getSingleOptionTxt4() {
		return this.singleOptionTxt4;
	}

	public void setSingleOptionTxt4(String singleOptionTxt4) {
		this.singleOptionTxt4 = singleOptionTxt4;
	}

	public String getSingleOptionTxt5() {
		return this.singleOptionTxt5;
	}

	public void setSingleOptionTxt5(String singleOptionTxt5) {
		this.singleOptionTxt5 = singleOptionTxt5;
	}

	public String getSingleChoiceAns() {
		return this.singleChoiceAns;
	}

	public void setSingleChoiceAns(String singleChoiceAns) {
		this.singleChoiceAns = singleChoiceAns;
	}

	public String getSelectedMultiAnsOpt() {
		return this.selectedMultiAnsOpt;
	}

	public void setSelectedMultiAnsOpt(String selectedMultiAnsOpt) {
		this.selectedMultiAnsOpt = selectedMultiAnsOpt;
	}

	public String[] getTestCode() {
		return this.testCode;
	}

	public void setTestCode(String[] testCode) {
		this.testCode = testCode;
	}

	public String[] getTestFileNM() {
		return this.testFileNM;
	}

	public void setTestFileNM(String[] testFileNM) {
		this.testFileNM = testFileNM;
	}

	public String[] getTestWt() {
		return this.testWt;
	}

	public void setTestWt(String[] testWt) {
		this.testWt = testWt;
	}

	public String getQuestionType() {
		return this.questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String[] getRole() {
		return this.role;
	}

	public void setRole(String[] role) {
		this.role = role;
	}

	public String getTemplateCode() {
		return this.templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getQuestionDesc() {
		return this.questionDesc;
	}

	public void setQuestionDesc(String questionDesc) {
		this.questionDesc = questionDesc;
	}

	public String getQuestionComplexity() {
		return this.questionComplexity;
	}

	public void setQuestionComplexity(String questionComplexity) {
		this.questionComplexity = questionComplexity;
	}

	public String getQuestionTechnology() {
		return this.questionTechnology;
	}

	public void setQuestionTechnology(String questionTechnology) {
		this.questionTechnology = questionTechnology;
	}

	public String getQuestionTime() {
		return this.questionTime;
	}

	public void setQuestionTime(String questionTime) {
		this.questionTime = questionTime;
	}

	public String getQuestionActive() {
		return this.questionActive;
	}

	public void setQuestionActive(String questionActive) {
		this.questionActive = questionActive;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQuestionComp() {
		return questionComp;
	}

	public void setQuestionComp(String questionComp) {
		this.questionComp = questionComp;
	}

	public String getQuestionTech() {
		return questionTech;
	}

	public void setQuestionTech(String questionTech) {
		this.questionTech = questionTech;
	}

	public String getqTime() {
		return qTime;
	}

	public void setqTime(String qTime) {
		this.qTime = qTime;
	}

	public String getQuestionAct() {
		return questionAct;
	}

	public void setQuestionAct(String questionAct) {
		this.questionAct = questionAct;
	}

	public List<TestCaseFormBean> getTestCaseList() {
		return testCaseList;
	}

	public void setTestCaseList(List<TestCaseFormBean> testCaseList) {
		this.testCaseList = testCaseList;
	}
	
	
}