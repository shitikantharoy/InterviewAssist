package com.centurylink.spring.controller;

public class MCQFormBean {
	private int answerId;
	private String optionTxt = "";
	private String optionAns;
	private String Qno;
	private String optionType;
	private int mcqID;

	public int getMcqID() {
		return this.mcqID;
	}

	public void setMcqID(int mcqID) {
		this.mcqID = mcqID;
	}

	public String getOptionTxt() {
		return this.optionTxt;
	}

	public void setOptionTxt(String optionTxt) {
		this.optionTxt = optionTxt;
	}

	public String getQno() {
		return this.Qno;
	}

	public void setQno(String qno) {
		this.Qno = qno;
	}

	public String getOptionType() {
		return this.optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public String getOptionAns() {
		return this.optionAns;
	}

	public void setOptionAns(String optionAns) {
		this.optionAns = optionAns;
	}

	public int getAnswerId() {
		return this.answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}
}