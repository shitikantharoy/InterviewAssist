package com.centurylink.spring.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.centurylink.spring.dao.UserDAO;
import com.centurylink.spring.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class UserRestController {
	@Autowired
	private UserDAO userDAO;

	@PostMapping({"/addUserDtls"})
	public ResponseEntity addUser(@RequestBody UserFormBean user) {
		
		String resposneStr = "";
		User user2 = new User();
		try {
			user2.setCuid(user.getCuid());
			user2.setName(user.getUname());
			user2.setPrivilege(user.getPrivilege());
			this.userDAO.saveOrUpdate(user2);
			resposneStr = "User sucessfully added.";
		} catch (Exception arg6) {
			resposneStr = "Error occured in " + arg6.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(resposneStr);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}
	
	@GetMapping({"/getUsers"})
	public ResponseEntity getUsers() {
		String resposneStr = "";
		List<User> userList = new ArrayList<User>();
		List<UserFormBean> userLstNew = new ArrayList<UserFormBean>();

		try {
			userList = userDAO.findAllUsers();
			
			if(userList!=null && !(userList.isEmpty())){
				for(User user:userList){
					UserFormBean userFormBean = new UserFormBean();
					userFormBean.setUserId(user.getUserId() + "");
					userFormBean.setCuid(user.getCuid());
					userFormBean.setUname(user.getName());
					userFormBean.setPrivilege(user.getPrivilege());
					userLstNew.add(userFormBean);
				}
			}
			
		} catch (Exception arg6) {
			resposneStr = "Error occured in " + arg6.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(userLstNew);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}
	
	@PostMapping({"/editUser"})
	public ResponseEntity editUser(@RequestParam Map<String, String> userEvent) {
		String resposneStr = "";

		try {
			if(userEvent !=null && userEvent.get("userId") != null){
				User user = userDAO.getUserByID((new Integer(userEvent.get("userId")).intValue()));
				user.setCuid(userEvent.get("cuid"));
				user.setName(userEvent.get("uname"));
				user.setPrivilege(userEvent.get("privilege"));
				userDAO.saveOrUpdate(user);
				resposneStr="User is sucessfully updated.";
			}else{
				resposneStr="Error occured while update.";
			}
		} catch (Exception arg7) {
			resposneStr = "Error occured in " + arg7.getMessage();
		}
		
		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(resposneStr);
		} catch (JsonProcessingException arg6) {
			arg6.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}
}