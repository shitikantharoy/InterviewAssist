package com.centurylink.spring.controller;

public class TestCaseFormBean {
	private String testCode;
	private String testFileNM;
	private int testWt;
	private int questionNo;

	public String getTestCode() {
		return this.testCode;
	}

	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}

	public String getTestFileNM() {
		return this.testFileNM;
	}

	public void setTestFileNM(String testFileNM) {
		this.testFileNM = testFileNM;
	}

	public int getTestWt() {
		return this.testWt;
	}

	public void setTestWt(int testWt) {
		this.testWt = testWt;
	}

	public int getQuestionNo() {
		return this.questionNo;
	}

	public void setQuestionNo(int questionNo) {
		this.questionNo = questionNo;
	}
}