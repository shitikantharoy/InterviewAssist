package com.centurylink.spring.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.centurylink.spring.dao.AnswerDAO;
import com.centurylink.spring.dao.ApplicabilityDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.dao.TestCaseDAO;
import com.centurylink.spring.model.Answer;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.TestCase;
import com.centurylink.transaction.facade.SessionFacade;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.jdbc.StringUtils;

@Controller
public class QuestionRestController {
	@Autowired
	HttpServletRequest request;
	@Autowired
	private QuestionDAO questionDAO;
	@Autowired
	private SessionFacade sessionFacade;
	@Autowired
	private ApplicabilityDAO applicabilityDAO;
	@Autowired
	private RoleDAO roleDAO;
	@Autowired
	private TestCaseDAO testCaseDAO;
	@Autowired
	private AnswerDAO answerDAO;

	@GetMapping({ "/questions/{id}" })
	public ResponseEntity getQuestion(@PathVariable("id") int id) {
		Question question = this.questionDAO.get(id);
		return question == null ? new ResponseEntity(
				"No Question found for ID " + id, HttpStatus.NOT_FOUND)
				: new ResponseEntity(question, HttpStatus.OK);
	}

	@PostMapping({ "/addQuestionSet" })
	public ResponseEntity addQuestion(@RequestBody QuestionFormBean questionForm) {
		String resposneStr = "";

		try {
			this.sessionFacade.insertQuestion(questionForm);
			resposneStr = "Question sucessfully added.";
		} catch (Exception arg6) {
			resposneStr = "Error occured in " + arg6.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(resposneStr);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}

	@GetMapping({ "/getQuestionSet" })
	public ResponseEntity getQuestion() {
		String resposneStr = "";
		Object questionLst = new ArrayList();

		try {
			questionLst = this.questionDAO.findAllQuestions();
		} catch (Exception arg6) {
			resposneStr = "Error occured in " + arg6.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(questionLst);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}

	@PostMapping({ "/editQuestion" })
	public ResponseEntity editQuestion(@RequestParam Map<String, String> editQuestion) {
		String resposneStr = "";
		new Question();
		try {
			
			if(editQuestion !=null && editQuestion.get("questionNo") != null){
				Question question = this.questionDAO.get((new Integer(editQuestion.get("questionNo")).intValue()));
				question.setQuestionActive(editQuestion.get("questionActive"));
				question.setExample(editQuestion.get("example")); 
				question.setQuestionComplexity(editQuestion.get("questionComplexity"));
				this.questionDAO.saveOrUpdate(question);
				resposneStr="Question is sucessfully updated.";
			}else{
				resposneStr="Error occured while update.";
			}
			
		} catch (Exception arg7) {
			resposneStr = "Error occured in " + arg7.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(resposneStr);
		} catch (JsonProcessingException arg6) {
			arg6.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}

	@PostMapping({ "/deleteQuestion" })
	public ResponseEntity deleteQuestion(@RequestBody QuestionFormBean questionForm) {
		String resposneStr = "";

		try {
			if (!StringUtils.isNullOrEmpty(questionForm.getId())) {
				//String[] test = questionIds.split("-");

				//for (int objectMapper = 0; objectMapper < test.length; ++objectMapper) {
					this.questionDAO.delete((new Integer(questionForm.getId())).intValue());
				//}
					resposneStr ="Question sucessfully deleted";
			}
		} catch (Exception arg6) {
			resposneStr = "Error occured in " + arg6.getMessage();
		}

		String arg7 = "";
		ObjectMapper arg8 = new ObjectMapper();

		try {
			arg7 = arg8.writeValueAsString(resposneStr);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}

		return new ResponseEntity(arg7, HttpStatus.OK);
	}

	@GetMapping({ "/getTestCases" })
	public ResponseEntity getTestCases(
			@RequestParam(value = "questionNo", required = false) String questionNo) {
		String resposneStr = "";
		List<TestCase> testCase = new ArrayList();

		try {
			if (!StringUtils.isNullOrEmpty(questionNo)) {
				testCase = this.testCaseDAO.get(new Integer(questionNo));
				TestCase testCase2 = new TestCase();
				testCase2.setQNo(new Integer(questionNo));
				testCase.add(testCase2);
			}
		} catch (Exception arg7) {
			resposneStr = "Error occured in " + arg7.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(testCase);
		} catch (JsonProcessingException arg6) {
			arg6.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}

	@GetMapping({ "/getMCQDetails" })
	public ResponseEntity getMCQDetails(
			@RequestParam(value = "questionNo", required = false) String questionNo) {
		String resposneStr = "";
		ArrayList mcqFormBeanslst = new ArrayList();
		new Answer();

		try {
			if (!StringUtils.isNullOrEmpty(questionNo)) {
				Answer answer = this.answerDAO.get(new Integer(questionNo));
				String[] test = answer.getCorrectAnswer().split(",");

				for (int objectMapper = 0; objectMapper < 5; ++objectMapper) {
					MCQFormBean e = new MCQFormBean();
					e.setMcqID(objectMapper + 1);
					e.setOptionType(answer.getOptionType());
					e.setQno(String.valueOf(answer.getQNo()));
					e.setAnswerId(answer.getAnswerId());
					if (objectMapper == 0
							&& !StringUtils.isNullOrEmpty(answer.getOption1())) {
						e.setOptionTxt(answer.getOption1());
						e.setOptionAns(this.setAnswer("1", test));
					}

					if (objectMapper == 1
							&& !StringUtils.isNullOrEmpty(answer.getOption2())) {
						e.setOptionTxt(answer.getOption2());
						e.setOptionAns(this.setAnswer("2", test));
					}

					if (objectMapper == 2
							&& !StringUtils.isNullOrEmpty(answer.getOption3())) {
						e.setOptionTxt(answer.getOption3());
						e.setOptionAns(this.setAnswer("3", test));
					}

					if (objectMapper == 3
							&& !StringUtils.isNullOrEmpty(answer.getOption4())) {
						e.setOptionTxt(answer.getOption4());
						e.setOptionAns(this.setAnswer("4", test));
					}

					if (objectMapper == 4
							&& !StringUtils.isNullOrEmpty(answer.getOption5())) {
						e.setOptionTxt(answer.getOption5());
						e.setOptionAns(this.setAnswer("5", test));
					}

					mcqFormBeanslst.add(e);
				}
			}
		} catch (Exception arg8) {
			resposneStr = "Error occured in " + arg8.getMessage();
		}

		String arg9 = "";
		ObjectMapper arg10 = new ObjectMapper();

		try {
			arg9 = arg10.writeValueAsString(mcqFormBeanslst);
		} catch (JsonProcessingException arg7) {
			arg7.printStackTrace();
		}

		return new ResponseEntity(arg9, HttpStatus.OK);
	}

	public String setAnswer(String rowNum, String[] answers) {
		String answer = "";

		for (int i = 0; i < answers.length; ++i) {
			if (!StringUtils.isNullOrEmpty(answers[i])
					&& rowNum.equals(answers[i])) {
				answer = "true";
			}
		}

		return answer;
	}

	@PostMapping({ "/insertTestCase" })
	public ResponseEntity insertTestCase(
			@RequestBody List<TestCase> testCaseLst) {
		String resposneStr = "";
		Question question = null;
		try {
			for(TestCase testCase2 :testCaseLst){
				if (question == null) {
					question = questionDAO.get(testCase2.getQNo());
				}
				if(testCase2.getTestCaseId() > 0){
					
					TestCase testCase = testCaseDAO.getTestCaseByID(testCase2.getTestCaseId());
					testCase.setQuestion(question);
					testCase.setTestCase(testCase2.getTestCase());
					testCase.setTestCaseWeightage(testCase2.getTestCaseWeightage());
					testCase.setTestFileName(testCase2.getTestFileName());
					testCaseDAO.saveOrUpdate(testCase);
				}else{
					if (testCase2.getTestCase() != null && !"".equals(testCase2.getTestCase()) && testCase2.getTestFileName() != null && !"".equals(testCase2.getTestFileName())) {
						TestCase testCase = new TestCase();
						question.setQuestionNo(testCase2.getQNo());
						testCase.setQuestion(question);
						testCase.setTestCase(testCase2.getTestCase());
						testCase.setTestFileName(testCase2.getTestFileName());
						testCase.setTestCaseWeightage(testCase2.getTestCaseWeightage());
						testCaseDAO.insert(testCase);
					}
				}
			}

			resposneStr = "Test Case added sucessfully";
		} catch (Exception arg8) {
			resposneStr = "Error occured in " + arg8.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(resposneStr);
		} catch (JsonProcessingException arg7) {
			arg7.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}

	@PostMapping({ "/saveMCQDetails" })
	public ResponseEntity saveMCQDetails(@RequestBody List<MCQFormBean> mcqLst) {
		String resposneStr = "";
		Answer answer = null;

		try {
			if (!CollectionUtils.isEmpty(mcqLst)) {
				int test = 0;
				String objectMapper = "";

				for (int e = 0; e < mcqLst.size(); ++e) {
					MCQFormBean mcqFormBeanDt = (MCQFormBean) mcqLst.get(e);
					if (answer == null) {
						new Answer();
						answer = this.answerDAO.get(new Integer(mcqFormBeanDt
								.getQno()));
					}

					String optionTxt = mcqFormBeanDt.getOptionTxt();
					String optionAns = mcqFormBeanDt.getOptionAns();
					if ("empty".equals(mcqFormBeanDt.getOptionTxt())) {
						optionTxt = "";
						optionAns = "";
					}

					if (answer != null) {
						if (e == 0) {
							answer.setOption1(optionTxt);
							if ("Yes".equals(optionAns)) {
								objectMapper = objectMapper + "1,";
								++test;
							}
						}

						if (e == 1) {
							answer.setOption2(optionTxt);
							if ("Yes".equals(optionAns)) {
								objectMapper = objectMapper + "2,";
								++test;
							}
						}

						if (e == 2) {
							answer.setOption3(optionTxt);
							if ("Yes".equals(optionAns)) {
								objectMapper = objectMapper + "3,";
								++test;
							}
						}

						if (e == 3) {
							answer.setOption4(optionTxt);
							if ("Yes".equals(optionAns)) {
								objectMapper = objectMapper + "4,";
								++test;
							}
						}

						if (e == 4) {
							answer.setOption5(optionTxt);
							if ("Yes".equals(optionAns)) {
								objectMapper = objectMapper + "5,";
								++test;
							}
						}
					}

					if (test == 1) {
						answer.setOptionType("Single");
					} else {
						answer.setOptionType("Multiple");
					}

					answer.setCorrectAnswer(objectMapper);
				}

				this.answerDAO.saveOrUpdate(answer);
			}

			resposneStr = "MCQ is sucessfully edidted.";
		} catch (Exception arg10) {
			resposneStr = "Error occured in " + arg10.getMessage();
		}

		String arg11 = "";
		ObjectMapper arg12 = new ObjectMapper();

		try {
			arg11 = arg12.writeValueAsString(resposneStr);
		} catch (JsonProcessingException arg9) {
			arg9.printStackTrace();
		}

		return new ResponseEntity(arg11, HttpStatus.OK);
	}
}