package com.centurylink.spring.controller;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.internal.util.StringHelper;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.centurylink.constants.PortalConstants;
import com.centurylink.helper.UploadDownloadFileHelper;
import com.centurylink.spring.dao.AnswerDAO;
import com.centurylink.spring.dao.CandidateAnswerDAO;
import com.centurylink.spring.dao.CandidateDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.TestCaseDAO;
import com.centurylink.spring.form.CandidateForm;
import com.centurylink.spring.form.OptionForm;
import com.centurylink.spring.model.Answer;
import com.centurylink.spring.model.Candidate;
import com.centurylink.spring.model.CandidateAnswer;
import com.centurylink.spring.model.CandidateAnswerId;
import com.centurylink.spring.model.Event;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.TestCase;
import com.centurylink.spring.service.EventService;
import com.centurylink.spring.service.QuestionService;
import com.mysql.jdbc.StringUtils;

@Controller
public class InterviewEventController implements PortalConstants {

	private final String Total_Test_Cases = "Total_Test_Cases";
	private final String Total_Passed_Test_Cases = "Total_Passed_Test_Cases";
	private final String Total_Test_Weightage = "Total_Test_Weightage";
	private final String Total_Passed_Test_Weightage = "Total_Passed_Test_Weightage";

	private static Logger log = Logger.getLogger(InterviewEventController.class.getName());

	@Autowired
	HttpServletRequest request;

	@Autowired
	CandidateDAO candidateDAO;

	@Autowired
	CandidateAnswerDAO candidateAnswerDAO;

	@Autowired
	QuestionDAO questionDAO;

	@Autowired
	AnswerDAO answerDAO;

	@Autowired
	QuestionService questionService;

	@Autowired
	EventService eventService;

	@Autowired
	TestCaseDAO testCaseDAO;

	private Map<Integer, String> questioniFileMap;

	@RequestMapping(value = "/event/**", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView interviewLoginPage(@RequestParam(value = "event", required = false) String event) {

		ModelAndView model = new ModelAndView();

		System.out.println("Event .... " + event);
		// validate the event

		boolean isValid = eventService.validateEvent(event);

		if (isValid) {
			model.setViewName(INTERVIEW_LOGIN);
		} else {
			model.setViewName(INVALID_EVENT);
		}

		request.setAttribute("event", event);

		// model.setViewName(INTERVIEW_LOGIN);
		return model;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/renderquestions", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView renderQuestions(@ModelAttribute("candidateForm") CandidateForm candidateForm) {

		ModelAndView model = new ModelAndView();
		model.setViewName(INTERVIEW_LOGIN);

		HttpSession session = request.getSession();

		String action = request.getParameter("action");
		String currentIndexStr = request.getParameter("currentIndex");
		UploadDownloadFileHelper uploadDownloadFileHelper = new UploadDownloadFileHelper();
		// String answer = request.getParameter("answer");

		int currentIndex = 0;

		if (!StringUtils.isNullOrEmpty(currentIndexStr)) {
			currentIndex = Integer.valueOf(currentIndexStr);
		}

		// System.out.println(session.getMaxInactiveInterval());
		// System.out.println(candidateForm.getTestFormList() != null ?
		// candidateForm.getTestFormList().get(0) : null);

		if (EXIT.equalsIgnoreCase(action) ) {
			model = logOffCandidate();
			return model;
		}
		else if (SAVE.equalsIgnoreCase(action) ) {
			model = saveCandidate();
			return model;
		}
		else if (COMPILE.equalsIgnoreCase(action)) {
			String uniqueuser = (String) session.getAttribute("uniqueuser");

			log.debug("returning user ...");
			log.debug("uniqueuser ..." + uniqueuser);

			if (StringUtils.isNullOrEmpty(uniqueuser)) {
				return model;
			}
			List<Question> questionList = (List<Question>) session.getAttribute("questionList");
			questioniFileMap = (HashMap<Integer, String>) session.getAttribute("questioniFileMap");
			Question questionOnScreen = questionList.get(currentIndex);
			try {
				String compileMsg = uploadDownloadFileHelper.compileCode(uniqueuser, questionOnScreen.getQuestionNo(),
						questionOnScreen.getFileName(), candidateForm.getProgramCode(),
						questioniFileMap.get(questionOnScreen.getQuestionNo()));
				session.setAttribute("CompilationMessage", compileMsg);
				session.setAttribute("ValidationMessage", null);
				candidateForm.setLoginTime((Calendar) session.getAttribute("candidateLoginTime"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.error(e.getStackTrace());
				e.printStackTrace();
			}
			questionOnScreen.setTemplateCode(candidateForm.getProgramCode());
			renderCurrentQuestion(candidateForm, questionList, currentIndex, action, null, questioniFileMap);
		}

		else if (VALIDATE.equalsIgnoreCase(action)) {
			String uniqueuser = (String) session.getAttribute("uniqueuser");

			log.debug("returning user ...");
			log.debug("uniqueuser ..." + uniqueuser);

			if (StringUtils.isNullOrEmpty(uniqueuser)) {
				return model;
			}
			List<Question> questionList = (List<Question>) session.getAttribute("questionList");
			Question questionOnScreen = questionList.get(currentIndex);
			String compileMsg;
			try {
				compileMsg = uploadDownloadFileHelper.compileCode(uniqueuser, questionOnScreen.getQuestionNo(),
						questionOnScreen.getFileName(), candidateForm.getProgramCode(),
						questioniFileMap.get(questionOnScreen.getQuestionNo()));
				session.setAttribute("CompilationMessage", compileMsg);

				if (SUCCESS.equalsIgnoreCase(compileMsg)) {
					Map<String, Integer> testCount = fetchCountOfTestCasesPassed(
							testCaseDAO.get(questionOnScreen.getQuestionNo()), questionOnScreen.getQuestionNo(), false);

					session.setAttribute("CompilationMessage", null);
					session.setAttribute("ValidationMessage", "Number of test cases passed is "
							+ testCount.get(Total_Passed_Test_Cases) + " Out of : " + testCount.get(Total_Test_Cases));

					Map<Integer, Object> userQuestionAnswerMap = (Map<Integer, Object>) session
							.getAttribute("userQuestionAnswerMap");
					userQuestionAnswerMap.put(questionOnScreen.getQuestionNo(), testCount);
					session.setAttribute("userQuestionAnswerMap", userQuestionAnswerMap);

				} else {
					session.setAttribute("ValidationMessage", null);
				}
				candidateForm.setLoginTime((Calendar) session.getAttribute("candidateLoginTime"));
				questionOnScreen.setTemplateCode(candidateForm.getProgramCode());
			} catch (Exception e) {
				log.error(e.getStackTrace());
				e.printStackTrace();
			}

			renderCurrentQuestion(candidateForm, questionList, currentIndex, action, null,
					(HashMap<Integer, String>) session.getAttribute("questioniFileMap"));
		} else if (session.getAttribute("uniqueuser") != null ) {

			String uniqueuser = (String) session.getAttribute("uniqueuser");

			log.debug("returning user ...");
			log.debug("uniqueuser ..." + uniqueuser);

			if (StringUtils.isNullOrEmpty(uniqueuser)) {
				return model;
			}
			List<Question> questionList = (List<Question>) session.getAttribute("questionList");

			String answerOptions = candidateForm.getAnswerOptions();

			Map<Integer, String> recordedAnswerFromScreen = (Map<Integer, String>) session
					.getAttribute("recordedAnswerFromScreen");

			if (!StringUtils.isNullOrEmpty(answerOptions)) {
				Question questionOnScreen = questionList.get(currentIndex);
				Map<Integer, Object> userQuestionAnswerMap = (Map<Integer, Object>) session
						.getAttribute("userQuestionAnswerMap");

				List<Object> userVsDBMCQAnsList = new ArrayList<Object>();
				userVsDBMCQAnsList.add(getAnswer(questionOnScreen.getQuestionNo()).getCorrectAnswer()); // correctAns
				userVsDBMCQAnsList.add(answerOptions); // User Answer while
														// loading the quiz

				userQuestionAnswerMap.put(questionOnScreen.getQuestionNo(), userVsDBMCQAnsList);

				recordedAnswerFromScreen.put(questionOnScreen.getQuestionNo(), answerOptions);

				session.setAttribute("userQuestionAnswerMap", userQuestionAnswerMap);
				session.setAttribute("recordedAnswerFromScreen", recordedAnswerFromScreen);
			}
			session.setAttribute("CompilationMessage", null);
			session.setAttribute("ValidationMessage", null);
			renderCurrentQuestion(candidateForm, questionList, currentIndex, action, recordedAnswerFromScreen,
					(HashMap<Integer, String>) session.getAttribute("questioniFileMap"));
			candidateForm.setLoginTime((Calendar) session.getAttribute("candidateLoginTime"));
			// renderCurrentQuestion(candidateForm, questionList,
			// currentIndex, action);

			/*
			 * int listSize = questionList.size(); int navigatedIndex =
			 * getNavigatedIndex(listSize, currentIndex, action); Question
			 * displayQuestion = displayQuestion(questionList, navigatedIndex,
			 * action); request.setAttribute("displayQuestion",
			 * displayQuestion); request.setAttribute("currentIndex",
			 * navigatedIndex); request.setAttribute("isLastQuestion",
			 * isLastQuestion(navigatedIndex, listSize));
			 */

		}
		model.setViewName("renderquestions");
		return model;

	}

	private String getFilepath(Question question, String event, String uniqueuser) {
		UploadDownloadFileHelper helper = new UploadDownloadFileHelper();
		String fileName = question.getFileName();
		if (!fileName.contains(".java")) {
			fileName = fileName + ".java";
		}
		String generateFilePath = helper.generateFilePath(event, uniqueuser, fileName, question.getQuestionNo());
		return generateFilePath;
	}

	private boolean checkUserIsNull(CandidateForm candidateForm) {
		String uniqueuser = candidateForm.getName() + "_" + candidateForm.getPhone();
		return candidateDAO.get(uniqueuser, candidateForm.getEvent()) == null;
	}

	private int getCalculatedMins(Date loginTime, Date logoutTime, long eventTime) {
		Long diff = 0l;
		if (loginTime != null && logoutTime != null) {
			diff = (logoutTime.getTime() - loginTime.getTime()) / (1000 * 60);
		}
		diff = eventTime - diff;
		return diff.intValue();
	}

	private int getCalculatedSecs(Date loginTime, Date logoutTime, long eventTime) {
		Long diff = 0l;
		if (loginTime != null && logoutTime != null) {
			diff = (logoutTime.getTime() - loginTime.getTime()) / (1000);
		}
		diff = diff % 60;
		return diff.intValue();
	}

	private void renderCurrentQuestion(CandidateForm candidateForm, List<Question> questionList, int currentIndex,
			String action, Map<Integer, String> recordedAnswerFromScreen, Map<Integer, String> fileMap) {

		if (candidateForm != null) {

			Question currentQuestion = questionList.get(currentIndex);
			UploadDownloadFileHelper uploadDownloadFileHelper = new UploadDownloadFileHelper();

			int listSize = questionList.size();
			int navigatedIndex = getNavigatedIndex(listSize, currentIndex, action);
			Question displayQuestion = displayQuestion(questionList, navigatedIndex, action);

			if (currentQuestion.getQuestionType().equalsIgnoreCase(PROGRAMMING) && (!VALIDATE.equalsIgnoreCase(action)
					&& !COMPILE.equalsIgnoreCase(action) && !StringHelper.isEmpty(action))
					&& fileMap.containsKey(currentQuestion.getQuestionNo())) {
				uploadDownloadFileHelper.writeFileToServer(fileMap.get(currentQuestion.getQuestionNo()),
						candidateForm.getProgramCode());
			}

			if (displayQuestion.getQuestionType().equalsIgnoreCase(PROGRAMMING)) {
				displayQuestion.setTemplateCode(
						uploadDownloadFileHelper.readFile(fileMap.get(displayQuestion.getQuestionNo())));
			}

			candidateForm.setDisplayQuestion(displayQuestion);
			candidateForm.setCurrentIndex(navigatedIndex);
			candidateForm.setLastQuestion(isLastQuestion(navigatedIndex, listSize));
			Answer answer = null;
			if (!displayQuestion.getQuestionType().equalsIgnoreCase(PROGRAMMING)) {
				answer = getAnswer(displayQuestion.getQuestionNo());
			}
			if (answer != null) {
				candidateForm.setOptionType(answer.getOptionType());
				List<OptionForm> optionList = new ArrayList<OptionForm>();

				String selectedAnswer = "";

				if (recordedAnswerFromScreen != null) {
					selectedAnswer = recordedAnswerFromScreen.get(displayQuestion.getQuestionNo());
				}

				if (!StringUtils.isNullOrEmpty(answer.getOption1())) {
					OptionForm option1 = new OptionForm();
					option1.setOption(answer.getOption1());
					if (!StringUtils.isNullOrEmpty(selectedAnswer) && selectedAnswer.indexOf("1") != -1) {
						option1.setSelected(CHECKED);
					}
					optionList.add(option1);
				}
				if (!StringUtils.isNullOrEmpty(answer.getOption2())) {
					OptionForm option2 = new OptionForm();
					option2.setOption(answer.getOption2());
					if (!StringUtils.isNullOrEmpty(selectedAnswer) && selectedAnswer.indexOf("2") != -1) {
						option2.setSelected(CHECKED);
					}
					optionList.add(option2);
				}
				if (!StringUtils.isNullOrEmpty(answer.getOption3())) {
					OptionForm option3 = new OptionForm();
					option3.setOption(answer.getOption3());
					if (!StringUtils.isNullOrEmpty(selectedAnswer) && selectedAnswer.indexOf("3") != -1) {
						option3.setSelected(CHECKED);
					}
					optionList.add(option3);
				}
				if (!StringUtils.isNullOrEmpty(answer.getOption4())) {
					OptionForm option4 = new OptionForm();
					option4.setOption(answer.getOption4());
					if (!StringUtils.isNullOrEmpty(selectedAnswer) && selectedAnswer.indexOf("4") != -1) {
						option4.setSelected(CHECKED);
					}
					optionList.add(option4);
				}
				if (!StringUtils.isNullOrEmpty(answer.getOption5())) {
					OptionForm option5 = new OptionForm();
					option5.setOption(answer.getOption5());
					if (!StringUtils.isNullOrEmpty(selectedAnswer) && selectedAnswer.indexOf("5") != -1) {
						option5.setSelected(CHECKED);
					}
					optionList.add(option5);
				}

				candidateForm.setOptions(optionList);
			}

			// request.setAttribute("displayQuestion", displayQuestion);
			// request.setAttribute("currentIndex", navigatedIndex);
			// request.setAttribute("isLastQuestion",
			// isLastQuestion(navigatedIndex, listSize));
		}

	}

	private Answer getAnswer(int questionNo) {

		Answer answer = answerDAO.get(questionNo);

		return answer;
	}

	private boolean isLastQuestion(int navigatedIndex, int listSize) {

		boolean isLastQuestion = false;

		if (navigatedIndex == (listSize - 1)) {
			isLastQuestion = true;
		}

		return isLastQuestion;
	}

	private int getNavigatedIndex(int size, int currentIndex, String action) {

		int updatedIndex = 0;

		if (NEXT.equalsIgnoreCase(action) && currentIndex + 1 < size) {
			updatedIndex = currentIndex + 1;
		} else if (PREVIOUS.equalsIgnoreCase(action) && currentIndex - 1 >= 0) {
			updatedIndex = currentIndex - 1;
		} else {
			return currentIndex;
		}

		return updatedIndex;
	}

	private Question displayQuestion(List<Question> questionList, int navigatedIndex, String action) {

		Question question = null;

		if (questionList != null) {
			int size = questionList.size();

			if (NEXT.equalsIgnoreCase(action) && navigatedIndex < size) {
				question = questionList.get(navigatedIndex);
			} else if (PREVIOUS.equalsIgnoreCase(action) && navigatedIndex >= 0) {
				question = questionList.get(navigatedIndex);
			} else if (StringUtils.isNullOrEmpty(action)) {
				question = questionList.get(0);
			} else if (!StringUtils.isNullOrEmpty(action) && navigatedIndex < size && navigatedIndex >= 0) {
				question = questionList.get(navigatedIndex);
			}
		}
		return question;
	}

	private String createCandidate(CandidateForm candidateForm) {

		String uniqueuser = candidateForm.getName() + "_" + candidateForm.getPhone();

		Candidate candidate = new Candidate();
		candidate.setEventNm(candidateForm.getEvent());
		candidate.setLogin(new java.util.Date());
		candidate.setName(uniqueuser);

		candidateDAO.create(candidate);

		return uniqueuser;

	}

	private List<Question> populateQuestions(String uniqueuser, String eventName) {

		List<Question> questionList = new ArrayList<Question>();

		Event event = eventService.getEvent(eventName);

		int mcqCount = eventService.totalMCQ(event);
		int programCount = eventService.totalProgram(event);

		if (mcqCount > 0) {
			List<Question> tempList1 = questionService.getQuestionsForEvent(event, MCQ);
			Collections.shuffle(tempList1, new Random());

			/*
			 * if (tempList1 != null) { if (tempList1.size() > mcqCount) {
			 * tempList1 = tempList1.subList(0, mcqCount); } }
			 */
			questionList.addAll(tempList1);
			Collections.shuffle(questionList, new Random());
		}

		if (programCount > 0) {
			List<Question> tempList2 = questionService.getQuestionsForEvent(event, PROGRAMMING);
			Collections.shuffle(tempList2, new Random());

			if (tempList2 != null) {
				if (tempList2.size() > programCount) {
					tempList2 = tempList2.subList(0, programCount);
				}
			}
			questionList.addAll(tempList2);
		}

		generateFile(questionList, uniqueuser);
		return questionList;
	}

	private Calendar getCandidateLogin(String uniqueuser, Long eventTime, int minsLeft, int secsLeft) {
		Calendar loginTime = Calendar.getInstance();
		Candidate candidate = candidateDAO.get(uniqueuser);
		if (eventTime.doubleValue() != minsLeft) {
			int timeDiffernce = eventTime.intValue() - minsLeft;
			loginTime.add(Calendar.MINUTE, -timeDiffernce);
			loginTime.add(Calendar.SECOND, -secsLeft);
		} else {
			loginTime.setTime(candidate.getLogin());
		}
		System.out.println(loginTime.getTime());
		return loginTime;
	}

	private void generateFile(List<Question> questions, String uniqueuser) {
		for (Question question : questions) {
			if (question.getQuestionType().equalsIgnoreCase(PROGRAMMING)) {
				UploadDownloadFileHelper helper = new UploadDownloadFileHelper();
				String fileFullPath = helper.createFile(uniqueuser, request.getParameter("event"),
						question.getQuestionNo(), question.getFileName(), question.getTemplateCode(), null, null, false,
						null);
				List<TestCase> list = testCaseDAO.get(question.getQuestionNo());
				for (TestCase testCase : list) {
					helper.createFile(uniqueuser, request.getParameter("event"), question.getQuestionNo(),
							question.getFileName(), question.getTemplateCode(), testCase.getTestFileName(),
							testCase.getTestCase(), true, request.getServletContext().getRealPath("/"));
				}
				if (questioniFileMap == null)
					questioniFileMap = new HashMap<Integer, String>();
				questioniFileMap.put(question.getQuestionNo(), fileFullPath);
			}
		}
	}

	private boolean isValidUser(CandidateForm candidateForm) {

		boolean isValidUser = false;
		String uniqueuser = candidateForm.getName() + "_" + candidateForm.getPhone();

		Candidate candidate = candidateDAO.get(uniqueuser, candidateForm.getEvent());

		if (candidate == null) {
			isValidUser = true;
		}

		return isValidUser;
	}

	@PostMapping("/logoffUser")
	public ModelAndView logOffCandidate() {

		log.debug("Logging out user !!");

		HttpSession session = request.getSession(false);

		String uniqueuser = (String) session.getAttribute("uniqueuser");
		String event = (String) session.getAttribute("event");
		
		@SuppressWarnings("unchecked")
		Map<Integer, Object> userQuestionAnswerMap = (Map<Integer, Object>) session
				.getAttribute("userQuestionAnswerMap");
		session.invalidate();
		Candidate candidate = candidateDAO.get(uniqueuser);
		candidate.setLogout(new java.util.Date());	
		candidate.setIsClickedExit(YES);
		
		candidateDAO.saveOrUpdate(candidate);

		saveCandidateAnswersToDB(userQuestionAnswerMap, candidate.getCandidateId());

		ModelAndView modelAndView = new ModelAndView("thankyou");
		modelAndView.addObject("loggedinuser", uniqueuser.split("_")[0]);
		modelAndView.addObject("loggedintest", event);
		return modelAndView;

	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView  saveCandidate() {

		log.debug("Save user Data !!");

		HttpSession session = request.getSession(false);

		String uniqueuser = (String) session.getAttribute("uniqueuser");
		String event = (String) session.getAttribute("event");
		@SuppressWarnings("unchecked")
		Map<Integer, Object> userQuestionAnswerMap = (Map<Integer, Object>) session.getAttribute("userQuestionAnswerMap");
		
		Candidate candidate = candidateDAO.get(uniqueuser);
		candidate.setLogout(new java.util.Date());
		candidateDAO.saveOrUpdate(candidate);
		
        saveCandidateAnswersToDB(userQuestionAnswerMap, candidate.getCandidateId());
        
        ModelAndView modelAndView = new ModelAndView("thankyou");
		modelAndView.addObject("loggedinuser", uniqueuser.split("_")[0]);
		modelAndView.addObject("loggedintest", event);
		return modelAndView;
        
	}

	@SuppressWarnings("unchecked")
	private void saveCandidateAnswersToDB(Map<Integer, Object> userQuestionAnswerMap, int candidateId) {

		if (userQuestionAnswerMap != null) {

			Set<Integer> answerSet = userQuestionAnswerMap.keySet();

			for (Integer questionNo : answerSet) {
				CandidateAnswerId candidateAnswerId = new CandidateAnswerId(candidateId, questionNo);
				CandidateAnswer candidateAnswer = new CandidateAnswer(candidateAnswerId);

				/*
				 * if(userQuestionAnswerMap.get(questionNo)==null){//null
				 * denotes MCQ question is not answered
				 * 
				 * candidateAnswer.setCandidateAnswer(null);
				 * candidateAnswer.setIsAnswerCorrect("No"); }
				 */
				if (userQuestionAnswerMap.get(questionNo) instanceof ArrayList<?>) {
					ArrayList<Object> candidateVsDbAnsData = (ArrayList<Object>) userQuestionAnswerMap.get(questionNo);

					String ansFromDBWithComma = String.valueOf(candidateVsDbAnsData.get(0));
					if (ansFromDBWithComma.endsWith(",")) {
						ansFromDBWithComma = ansFromDBWithComma.substring(0, ansFromDBWithComma.length() - 1);

					}
					String candidateAns = String.valueOf(candidateVsDbAnsData.get(1));
					String isAnsweredCorrectly = ansFromDBWithComma.equals(candidateAns) ? YES : NO;

					candidateAnswer.setCandidateAnswer(candidateAns);
					candidateAnswer.setIsAnswerCorrect(isAnsweredCorrectly);
				} else {
					Map<String, Integer> programQuesRunDetails = (Map<String, Integer>) userQuestionAnswerMap
							.get(questionNo);

					for (Entry<String, Integer> entry : programQuesRunDetails.entrySet()) {
						System.out.println(entry.getKey() + "/" + entry.getValue());

						if (Total_Test_Cases.equalsIgnoreCase(entry.getKey()))
							candidateAnswer.setTotalTestCases(entry.getValue().toString());
						if (Total_Passed_Test_Cases.equalsIgnoreCase(entry.getKey()))
							candidateAnswer.setPassedTestCases(entry.getValue().toString());

						if (Total_Test_Weightage.equalsIgnoreCase(entry.getKey()))
							candidateAnswer.setTotalWeightage(entry.getValue().toString());
						if (Total_Passed_Test_Weightage.equalsIgnoreCase(entry.getKey()))
							candidateAnswer.setAchievedWeightage(entry.getValue().toString());
					}
				}
				candidateAnswerDAO.saveOrUpdate(candidateAnswer);
			}
		}
	}

	/**
	 * @param testCasesAndOutput
	 * @return
	 * @throws Exception
	 */
	private Map<String, Integer> fetchCountOfTestCasesPassed(List<TestCase> testCases, int questionNum,
			boolean whileLoadingquiz) throws Exception {
		Map<String, Integer> results = new HashMap<String, Integer>();
		List<Integer> testResult = new ArrayList<Integer>();
		for (TestCase testcase : testCases) {

			if (testResult.size() == 0) {
				testResult = runProcess(questioniFileMap.get(questionNum), testcase.getTestFileName());
				testResult.add(testResult.get(0) * testcase.getTestCaseWeightage());
				testResult.add(testResult.get(1) * testcase.getTestCaseWeightage());
			} else {
				List<Integer> result = runProcess(questioniFileMap.get(questionNum), testcase.getTestFileName());
				testResult.set(0, testResult.get(0) + result.get(0));
				testResult.set(1, testResult.get(1) + result.get(1));
				testResult.set(2, testResult.get(2) + (testResult.get(0) * testcase.getTestCaseWeightage()));
				testResult.add(3, testResult.get(3) + (testResult.get(1) * testcase.getTestCaseWeightage()));
			}
		}
		results.put(Total_Test_Cases, testResult.get(0));
		results.put(Total_Test_Weightage, testResult.get(2));

		if (whileLoadingquiz) {
			results.put(Total_Passed_Test_Cases, 0);
			results.put(Total_Passed_Test_Weightage, 0);
		} else {
			results.put(Total_Passed_Test_Cases, testResult.get(1));
			results.put(Total_Passed_Test_Weightage, testResult.get(3));
		}
		return results;
	}

	/**
	 * 
	 * @param filePath
	 * @param fileName
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private List<Integer> runProcess(String filePath, String fileName) throws IOException, InterruptedException {
		List<Integer> testResult = new ArrayList<Integer>();
		int index = filePath.lastIndexOf(FORWARD_SLASH_DELIMETTER);
		filePath = filePath.substring(0, index);
		if (!fileName.contains(DOT_JAVA)) {
			fileName = fileName + DOT_JAVA;
		}
		index = fileName.lastIndexOf(DOT);
		fileName = fileName.substring(0, index);
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		URLClassLoader urlClassLoader = URLClassLoader
				.newInstance(new URL[] { new URL("file:///" + filePath + FORWARD_SLASH_DELIMETTER) }, classLoader);
		try {
			Class clazz = urlClassLoader.loadClass(fileName);
			Result result = JUnitCore.runClasses(clazz);
			testResult.add(result.getRunCount());
			if (result.getFailures().isEmpty()) {
				System.out.println("Test Suite successFul");
			}
			testResult.add(result.getRunCount() - result.getFailures().size());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return testResult;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/starttest", method = { RequestMethod.GET })
	public ModelAndView startTest(@ModelAttribute("candidateForm") CandidateForm candidateForm) {

		ModelAndView model = new ModelAndView();
		model.setViewName(INTERVIEW_LOGIN);

		HttpSession session = request.getSession();

		String action = request.getParameter("action");

		String currentIndexStr = request.getParameter("currentIndex");
		// String answer = request.getParameter("answer");

		int currentIndex = 0;

		if (!StringUtils.isNullOrEmpty(currentIndexStr)) {
			currentIndex = Integer.valueOf(currentIndexStr);
		}

		// System.out.println(session.getMaxInactiveInterval());
		// System.out.println(candidateForm.getTestFormList() != null ?
		// candidateForm.getTestFormList().get(0) : null);
		if (action == null) {
			model.setViewName("thankyou");
			return model;
		} else {
			String uniqueuser = candidateForm.getName() + "_" + candidateForm.getPhone();
			List<CandidateAnswer> answers = (List<CandidateAnswer>) session.getAttribute("candidateanswers");
			List<Question> questionList = (List<Question>) session.getAttribute("questionList");
			if (questionList == null) {
				questionList = new ArrayList<Question>();
				Candidate candidate = candidateDAO.get(uniqueuser, candidateForm.getEvent());
				String[] split = !(StringUtils.isNullOrEmpty(uniqueuser)) ? uniqueuser.split("_") : null;
				Map<Integer, Object> userQuestionAnswerMap = new HashMap<Integer, Object>();
				Map<Integer, String> recordedAnswerFromScreen = new HashMap<Integer, String>();
				Map<Integer, String> fileMap = new HashMap<Integer, String>();
				for (CandidateAnswer answer : answers) {
					questionList.add(answer.getQuestion());
					if (StringHelper.isEmpty(answer.getTotalTestCases())) {
						List<Object> userVsDBMCQAnsList = new ArrayList<Object>();
						userVsDBMCQAnsList.add(getAnswer(answer.getQuestion().getQuestionNo()).getCorrectAnswer()); // correctAns
						userVsDBMCQAnsList.add(answer.getCandidateAnswer());
						userQuestionAnswerMap.put(answer.getQuestion().getQuestionNo(), userVsDBMCQAnsList);
						recordedAnswerFromScreen.put(answer.getQuestion().getQuestionNo(), answer.getCandidateAnswer());
					} else {
						Map<String, Integer> programQuesRunDetails = new HashMap<String, Integer>();
						programQuesRunDetails.put("Total_Test_Cases", Integer.valueOf(answer.getTotalTestCases()));
						programQuesRunDetails.put("Total_Passed_Test_Cases",
								Integer.valueOf(answer.getPassedTestCases()));
						programQuesRunDetails.put("Total_Test_Weightage", Integer.valueOf(answer.getTotalWeightage()));
						programQuesRunDetails.put("Total_Passed_Test_Weightage",
								Integer.valueOf(answer.getAchievedWeightage()));
						userQuestionAnswerMap.put(answer.getQuestion().getQuestionNo(), programQuesRunDetails);
						fileMap.put(answer.getQuestion().getQuestionNo(),
								getFilepath(answer.getQuestion(), candidateForm.getEvent(), uniqueuser));
					}

				}
				session.setAttribute("uniqueuser", uniqueuser);
				session.setAttribute("name", split[0]);
				session.setAttribute("totalquestions", answers.size());
				session.setAttribute("questionList", questionList);
				session.setAttribute("questioniFileMap", fileMap);
				session.setAttribute("userQuestionAnswerMap", userQuestionAnswerMap);
				Event event = eventService.getEvent(candidateForm.getEvent());
				long eventTime = event.getEventDuration();
				session.setAttribute("eventTime", eventTime);
				int minLeft = getCalculatedMins(candidate.getLogin(), candidate.getLogout(), eventTime);
				int secLeft = getCalculatedSecs(candidate.getLogin(), candidate.getLogout(), eventTime);
				session.setAttribute("totalTime", eventTime);
				session.setAttribute("CompilationMessage", null);
				session.setAttribute("ValidationMessage", null);
				session.setAttribute("recordedAnswerFromScreen", recordedAnswerFromScreen);
				session.setAttribute("event", candidateForm.getEvent());
				renderCurrentQuestion(candidateForm, questionList, currentIndex, action, recordedAnswerFromScreen,
						fileMap);
				Calendar candidateLogin = getCandidateLogin(uniqueuser, eventTime, minLeft, secLeft);
				session.setAttribute("candidateLoginTime", candidateLogin);
				candidateForm.setLoginTime(candidateLogin);
			} else {
				Map<Integer, Object> userQuestionAnswerMap = new HashMap<Integer, Object>();
				Map<Integer, String> recordedAnswerFromScreen = new HashMap<Integer, String>();

				/** Added by Piyush Starts **/
				for (Question question : questionList) {

					int questionNumber = question.getQuestionNo();

					if (MCQ.equalsIgnoreCase(question.getQuestionType())) {

						List<Object> userVsDBMCQAnsList = new ArrayList<Object>();
						userVsDBMCQAnsList.add(getAnswer(questionNumber).getCorrectAnswer()); // correctAns
						userVsDBMCQAnsList.add(null); // User Answer while
														// loading the quiz
						userQuestionAnswerMap.put(questionNumber, userVsDBMCQAnsList);
						// recordedAnswerFromScreen.put(questionNumber,
						// candidateForm.getAnswerOptions());
					} else if (PROGRAMMING.equalsIgnoreCase(question.getQuestionType())) {
						try {
							Map<String, Integer> testCount = fetchCountOfTestCasesPassed(
									testCaseDAO.get(questionNumber), questionNumber, true);
							userQuestionAnswerMap.put(questionNumber, testCount);
						} catch (Exception testCaseFetchedException) {
							testCaseFetchedException.printStackTrace();
						}
					}
				}
				/** Added by Piyush Ends **/

				String[] split = !(StringUtils.isNullOrEmpty(uniqueuser)) ? uniqueuser.split("_") : null;

				session.setAttribute("uniqueuser", uniqueuser);
				session.setAttribute("name", split[0]);
				session.setAttribute("totalquestions", questionList.size());
				session.setAttribute("questioniFileMap", questioniFileMap);
				session.setAttribute("userQuestionAnswerMap", userQuestionAnswerMap);
				Event event = eventService.getEvent(candidateForm.getEvent());
				long eventTime = event.getEventDuration();
				session.setAttribute("eventTime", eventTime);
				int minLeft = getCalculatedMins(null, null, eventTime);
				int secLeft = getCalculatedSecs(null, null, eventTime);
				session.setAttribute("totalTime", eventTime);
				session.setAttribute("CompilationMessage", null);
				session.setAttribute("ValidationMessage", null);
				session.setAttribute("recordedAnswerFromScreen", recordedAnswerFromScreen);
				session.setAttribute("event", candidateForm.getEvent());
				renderCurrentQuestion(candidateForm, questionList, currentIndex, action, recordedAnswerFromScreen,
						(HashMap<Integer, String>) session.getAttribute("questioniFileMap"));
				Calendar candidateLogin = getCandidateLogin(uniqueuser, eventTime, minLeft, secLeft);
				session.setAttribute("candidateLoginTime", candidateLogin);
				candidateForm.setLoginTime(candidateLogin);
			}
		}

		model.setViewName("renderquestions");
		return model;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/instructions", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView getInstructions(@ModelAttribute("candidateForm") CandidateForm candidateForm) {

		boolean isUserClickedExit =false;
		ModelAndView model = new ModelAndView();
		model.setViewName(INTERVIEW_LOGIN);
		HttpSession session = request.getSession();
		String action = candidateForm.getAction();

		// String answer = request.getParameter("answer");
		/*
		 * if (action == null) { model.setViewName("thankyou"); return model; }
		 */

		// else {

		if (checkUserIsNull(candidateForm)) {
			if (candidateForm.getEmail() == null) {
				return model;
			}
			log.debug("new Session ...");

			session.setMaxInactiveInterval(-1);

			String uniqueuser = createCandidate(candidateForm);
			List<Question> questionList = populateQuestions(uniqueuser, candidateForm.getEvent());
			session.setAttribute("questionList", questionList);
			session.setAttribute("candidateanswers", null);
		} else {
			String uniqueuser = candidateForm.getName() + "_" + candidateForm.getPhone();
			Candidate candidate = candidateDAO.get(uniqueuser, candidateForm.getEvent());
			if (candidate != null && YES.equalsIgnoreCase(candidate.getIsClickedExit()))
			{
				isUserClickedExit= true;
			}
			List<CandidateAnswer> answers = candidateAnswerDAO.get(candidate.getCandidateId());
			if (answers != null && answers.size() > 0) {
				session.setAttribute("questionList", null);
			}
			session.setAttribute("candidateanswers", answers);
			if (isUserClickedExit)
            {
				model.addObject("errorMessage","This User already taken test");
                return model;
            }

		}
		model.setViewName("testinstructions");
		return model;

	}
	// }
}
