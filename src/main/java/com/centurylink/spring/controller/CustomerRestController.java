package com.centurylink.spring.controller;

import com.centurylink.spring.dao.CustomerDAO;
import com.centurylink.spring.model.Customer;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerRestController {
	@Autowired
	private CustomerDAO customerDAO;

	@GetMapping({"/customers"})
	public List getCustomers() {
		return this.customerDAO.list();
	}

	@GetMapping({"/customers/{id}"})
	public ResponseEntity getCustomer(@PathVariable("id") Long id) {
		Customer customer = this.customerDAO.get(id);
		return customer == null
				? new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND)
				: new ResponseEntity(customer, HttpStatus.OK);
	}

	@PostMapping({"/customers"})
	public ResponseEntity createCustomer(@RequestBody Customer customer) {
		this.customerDAO.create(customer);
		return new ResponseEntity(customer, HttpStatus.OK);
	}

	@DeleteMapping({"/customers/{id}"})
	public ResponseEntity deleteCustomer(@PathVariable Long id) {
		return null == this.customerDAO.delete(id)
				? new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND)
				: new ResponseEntity(id, HttpStatus.OK);
	}

	@PutMapping({"/customers/{id}"})
	public ResponseEntity updateCustomer(@PathVariable Long id, @RequestBody Customer customer) {
		customer = this.customerDAO.update(id, customer);
		return null == customer
				? new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND)
				: new ResponseEntity(customer, HttpStatus.OK);
	}
}