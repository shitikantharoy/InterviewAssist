package com.centurylink.spring.controller;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.centurylink.constants.PortalConstants;
import com.centurylink.spring.dao.EventDAO;
import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.model.Event;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.Role;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.jdbc.StringUtils;


@Controller
public class EventRestController {
	@Autowired
	private EventDAO eventDAO;
	@Autowired
	private RoleDAO roleDAO;


	@PostMapping({"/addEvent"})
	public ResponseEntity addEvent(@RequestBody EventFormBean eventFormBean) {
		String resposneStr = "";

		try {
			
			Event event = new Event();
			
			if(!StringUtils.isNullOrEmpty(eventFormBean.getEventEndDate())){
				String [] EndDt = eventFormBean.getEventEndDate().split(" ");
				event.setEventEndDate(EndDt[0]);
				event.setEventEndTime(EndDt[1]);
			}
			
			if(!StringUtils.isNullOrEmpty(eventFormBean.getEventStartDate())){
				String [] startDt= eventFormBean.getEventStartDate().split(" ");
				event.setEventStartDate(startDt[0]);
				event.setEventStartTime(startDt[1]);
			}
			
			event.setSimpleMcq(eventFormBean.getSimpleMcq());
			event.setMediumMcq(eventFormBean.getMediumMcq());
			event.setComplexMcq(eventFormBean.getComplexMcq());
			
			event.setSimplePgm(eventFormBean.getSimplePgm());
			event.setMediumPgm(eventFormBean.getMediumPgm());
			event.setComplexPgm(eventFormBean.getComplexPgm());
			event.setEventName(eventFormBean.getEventName());
			
			//event.setEventStatus(eventFormBean.getEventStatus());
			event.setEventDuration(Integer.parseInt(eventFormBean.getEventDuration()));
			event.setEventStatus(PortalConstants.ACTIVE_EVENT);
			event.setEventTechnology(eventFormBean.getEventTechnology());
			
			java.util.Date date = new java.util.Date();
			java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
			event.setModifiedDt(timestamp);
			
			Event eventTemp = eventDAO.get(event.getEventName());
			Role role = roleDAO.get(eventFormBean.getApplicability());
			event.setRoleId(role.getRoleId());
			
			if(eventTemp != null && eventTemp.getEventName()!= null){
				resposneStr = "Error Occured:: Event name is already in use, please try a diffrent event name.";
			}else{
				event=generateUrl(event);
				eventDAO.create(event);
				resposneStr = "Event sucessfully added.";
			}
			
		} catch (Exception arg6) {
			resposneStr = "Error occured in " + arg6.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(resposneStr);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}
		return new ResponseEntity(test, HttpStatus.OK);
	}
	
	private Event generateUrl(Event event) {
		try {
			InetAddress ip;
			if (event != null) {
				ip = InetAddress.getLocalHost();
				String url = "http://" + ip.getHostAddress()
						+ ":8080/InterviewAssist/event?event="+ event.getEventName();
				event.setEventUrl(url);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return event;
	}
	
	
	@GetMapping({"/getEvents"})
	public ResponseEntity getEvents() {
		String resposneStr = "";
		List<Event> eventLst = new ArrayList<Event>();
		List<EventFormBean> eventLstNew = new ArrayList<EventFormBean>();

		try {
			eventLst = eventDAO.findAllEvents();
			
			if(eventLst!=null && !(eventLst.isEmpty())){
				for(Event eventTemp:eventLst){
					EventFormBean eventFormBean = new EventFormBean();
					eventFormBean.setInterviewEventId(String.valueOf(eventTemp.getInterviewEventId()));
					eventFormBean.setEventUrl(eventTemp.getEventUrl());
					eventFormBean.setEventName(eventTemp.getEventName());
					eventFormBean.setEventTechnology(eventTemp.getEventTechnology());
					
					eventFormBean.setEventStartDate(eventTemp.getEventStartDate());
					eventFormBean.setEventStartTime(eventTemp.getEventStartTime());
					eventFormBean.setEventEndDate(eventTemp.getEventEndDate());
					eventFormBean.setEventEndTime(eventTemp.getEventEndTime());
					Role role = roleDAO.getRoleById(eventTemp.getRoleId());
					eventFormBean.setApplicability(role.getRole());
					eventFormBean.setEventStatus(eventTemp.getEventStatus());
					
					eventLstNew.add(eventFormBean);
				}
			}
			
		} catch (Exception arg6) {
			resposneStr = "Error occured in " + arg6.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(eventLstNew);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}
	
	
	@PostMapping({"/editEvent"})
	public ResponseEntity editEvent(@RequestParam Map<String, String> editEvent) {
		String resposneStr = "";

		try {
			if(editEvent !=null && editEvent.get("interviewEventId") != null){
				Event event = eventDAO.getEventByID((new Integer(editEvent.get("interviewEventId")).intValue()));
				event.setEventEndDate(editEvent.get("eventEndDate"));
				if(editEvent.get("eventEndTime") !=null){
					event.setEventEndTime(editEvent.get("eventEndTime")+":00");
				}else{
					event.setEventEndTime("00:00");
				}
				event.setEventStatus(editEvent.get("eventStatus"));
				eventDAO.saveOrUpdate(event);
				resposneStr="Event is sucessfully updated.";
			}else{
				resposneStr="Error occured while update.";
			}
			System.out.println("sdfsdfsd"+editEvent);
			/*if (!StringUtils.isNullOrEmpty(eventFormBean.getJqGrid_id())) {
				Event event = eventDAO.getEventByID((new Integer(eventFormBean.getJqGrid_id())).intValue());
				event.setEventEndDate(eventFormBean.getEventEndDate());
				event.setEventEndTime(eventFormBean.getEventEndTime());
				event.setEventStatus(eventFormBean.getEventStatus());
				eventDAO.saveOrUpdate(event);
				
				resposneStr = "Event sucessfully updated.";
			}*/
		} catch (Exception arg7) {
			resposneStr = "Error occured in " + arg7.getMessage();
		}
		
		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(resposneStr);
		} catch (JsonProcessingException arg6) {
			arg6.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}
	
	
	@GetMapping({"/getEventByID"})
	public ResponseEntity getEventByID(@RequestParam(value = "eventID", required = false) String eventID) {
		String resposneStr = "";
		ArrayList<EventFormBean> eventFormLst = new ArrayList<EventFormBean>();
		try {
			if (!StringUtils.isNullOrEmpty(eventID)) {
				Event event = eventDAO.getEventByID((new Integer(eventID)).intValue());
				EventFormBean bean = new EventFormBean();
				bean.setSimpleMcq(event.getSimpleMcq());
				bean.setSimplePgm(event.getSimplePgm());
				
				bean.setMediumMcq(event.getMediumMcq());
				bean.setMediumPgm(event.getMediumPgm());
				
				bean.setComplexMcq(event.getComplexMcq());
				bean.setComplexPgm(event.getComplexPgm());
				
				bean.setInterviewEventId(String.valueOf(event.getInterviewEventId()));
				bean.setEventName(event.getEventName());
				eventFormLst.add(bean);
			}
		} catch (Exception arg7) {
			resposneStr = "Error occured in " + arg7.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(eventFormLst);
		} catch (JsonProcessingException arg6) {
			arg6.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}
	
	
	@PostMapping({"/saveEventQuestions"})
	public ResponseEntity saveEventQuestions(@RequestParam Map<String, String>  evenQuestions) {
		String resposneStr = "";

		try {
			if(evenQuestions !=null && evenQuestions.get("interviewEventId") != null){
				Event event = eventDAO.getEventByID((new Integer(evenQuestions.get("interviewEventId")).intValue()));
				event.setSimpleMcq(evenQuestions.get("simpleMcq"));
				event.setSimplePgm(evenQuestions.get("simplePgm"));
				
				event.setMediumMcq(evenQuestions.get("mediumMcq"));
				event.setMediumPgm(evenQuestions.get("mediumPgm"));
				
				event.setComplexMcq(evenQuestions.get("complexMcq"));
				event.setComplexPgm(evenQuestions.get("complexPgm"));
				eventDAO.saveOrUpdate(event);
				
				resposneStr = "No of question sucessfully updated for the event.";
			}
		} catch (Exception arg7) {
			resposneStr = "Error occured in " + arg7.getMessage();
		}

		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(resposneStr);
		} catch (JsonProcessingException arg6) {
			arg6.printStackTrace();
		}

		return new ResponseEntity(test, HttpStatus.OK);
	}
	
	
}