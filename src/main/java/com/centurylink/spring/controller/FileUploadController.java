package com.centurylink.spring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.centurylink.helper.FileValidator;
import com.centurylink.helper.ReadQuestionExcel;
import com.centurylink.spring.model.FileBucket;
import com.centurylink.transaction.facade.SessionFacade;

@Controller
public class FileUploadController {

	@Autowired
	FileValidator fileValidator;
	@Autowired
	private SessionFacade sessionFacade;
	@Autowired
	HttpServletRequest request;
	private static Logger log = Logger.getLogger(FileUploadController.class
			.getName());

	@InitBinder("fileBucket")
	protected void initBinderFileBucket(WebDataBinder binder) {
		binder.setValidator(fileValidator);
	}

	@RequestMapping(value = "/importQuestions", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String getSingleUploadPage(ModelMap model) {
		FileBucket fileModel = new FileBucket();
		model.addAttribute("fileBucket", fileModel);
		model.addAttribute("response", null);
		model.addAttribute("userSelect", "importQuestions");
		return "index";
	}

	@PostMapping(value = "/importQuestionToDB")
	public String singleFileUpload(@Valid FileBucket fileBucket,
			BindingResult result, ModelMap model) {
		String resposneStr = "Exception Occured While Importing Questions";
		if (result.hasErrors()) {
			resposneStr = "validation errors";

		} else {
			System.out.println("Fetching file");
			MultipartFile multipartFile = fileBucket.getFile();
			ReadQuestionExcel readQuestionExcel = new ReadQuestionExcel();
			try {
				List<QuestionFormBean> readData = readQuestionExcel
						.readData(multipartFile.getInputStream());
				for (QuestionFormBean questionFormBean : readData) {
					this.sessionFacade.insertQuestion(questionFormBean);
				}

				// Now do something with file...
				resposneStr = "Question Successfully added";
			} catch (Exception ex) {
				log.error(ex.getStackTrace());
			}
		}
		FileBucket fileModel = new FileBucket();
		model.addAttribute("fileBucket", fileModel);
		model.addAttribute("response", resposneStr);
		model.addAttribute("userSelect", "importQuestions");
		return "index";
	}

}
