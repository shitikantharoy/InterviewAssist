package com.centurylink.spring.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.centurylink.helper.UploadDownloadFileHelper;
import com.centurylink.spring.dao.EventDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.ReportDAO;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.Report;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class ReportEventController {
	@Autowired
	private ReportDAO reportDAO;
	
	@Autowired
	private EventDAO eventDAO;
	
	/*@Autowired
	private QuestionDAO qustionDAO;*/

	@Autowired
	HttpServletRequest request;

	@GetMapping({ "/getReport" })
	public ResponseEntity report(
			@RequestParam(value = "eventName", required = false) String eventName, 
			@RequestParam(value = "betweenDates", required = false) String betweenDates,
			@RequestParam(value = "applicability", required = false) String applicability) {
		StringBuilder query = new StringBuilder();
		String eventStartDate = "";
		String eventEndDate = "";
		if (!StringUtils.isEmpty(betweenDates)){
		String[] dateRanges = betweenDates.split(" ");
		eventStartDate = dateRanges[0];
		eventEndDate = dateRanges[1];
		}
		
		
		System.out.println("Get Report Event is called");
		query.append("select candidate.Name as Candidate_id, e.event_name as Event_Name, candidate.Login as Test_Time,");
		query.append(" role.Description as Employee_Role from candidate, interview_event e, candidate_answer, role");
		query.append(" where candidate.Event_NM = e.event_name and candidate_answer.candidate_id = candidate.Candidate_ID");
		if (!StringUtils.isEmpty(eventName))
			query.append(" and e.event_name = :event");
		if (!StringUtils.isEmpty(eventStartDate) && !StringUtils.isEmpty(eventEndDate))
			query.append(" and str_to_date(login, '%Y-%m-%d') BETWEEN str_to_date('"+eventStartDate+"', '%Y-%m-%d')and str_to_date('"+eventEndDate+"', '%Y-%m-%d')");
//		if (!StringUtils.isEmpty(eventEndDate))
//			query.append(" and str_to_date(event_end_dt, '%Y/%m/%d') <= str_to_date('"+eventEndDate+"', '%Y/%m/%d')");
		if (!StringUtils.isEmpty(applicability))
			query.append(" and role.role = :role");
		
		
//		 AND str_to_date (login, '%Y-%m-%d') BETWEEN str_to_date ('2017-09-15','%Y-%m-%d')AND str_to_date ('2017-09-30',
//                 '%Y-%m-%d')

		query.append(" and e.role_id = role.Role_ID group by candidate.Candidate_id order by candidate.Candidate_id ");
		List<Report> reportByCustomQuery = reportDAO.getReportByCustomQuery(
				query.toString(), null, eventName, applicability, eventStartDate,
				eventEndDate);
		Map<String, Integer> complexitymap = reportDAO.getComplexitymap();
		for (Report report : reportByCustomQuery) {
			List<Report> reportforGivenCuid = getReportforGivenCuid(report.getCandidateCuid());
			int totalMCQQuestion = 0;
			int correctMCQQuestions = 0;
			int totalProgQuestions = 0;
			int totalCorrectPorgQuestions = 0;
			double progAchievedScore = 0;
			int mcqAchievedScore = 0;
			int progTotalScore = 0;
			int mcqTotalScore = 0;
			for (Report report2 : reportforGivenCuid) {
				if (report2.isProgramIndicator()) {
					totalProgQuestions++;
					progTotalScore = progTotalScore + 100
							* complexitymap.get(report2.getComplexity());
					progAchievedScore = progAchievedScore
							+ report2.getProgrammingScore()
							* complexitymap.get(report2.getComplexity());
					if (report2.getProgrammingScore() == 100d) {
						totalCorrectPorgQuestions++;

					}

				} else {
					totalMCQQuestion++;
					mcqTotalScore = mcqTotalScore
							+ complexitymap.get(report2.getComplexity());
					if ("Yes".equalsIgnoreCase(report2.getCorrectFlag())) {
						mcqAchievedScore = mcqAchievedScore
								+ complexitymap.get(report2.getComplexity());
						correctMCQQuestions++;
					}

				}
			}
			// report.setCandidateReport(reportforGivenCuid);
			report.setTotalMCQQuestions(totalMCQQuestion);
			report.setTotalCorrectMCQQuestions(correctMCQQuestions);
			report.setTotalProgrammingQuestions(totalProgQuestions);
			report.setTotalCorrectProgrammingQuestions(totalCorrectPorgQuestions);
			if (report.getTotalMCQQuestions() > 0 && mcqTotalScore > 0)
				report.setMcqScore((mcqAchievedScore * 100)
						/ (mcqTotalScore));
			if (report.getTotalProgrammingQuestions() > 0 && progTotalScore > 0) {
				DecimalFormat df = new DecimalFormat("#.##");
				report.setProgrammingScore(Double.parseDouble(df
						.format((progAchievedScore * 100)
								/ (progTotalScore))));
			}
			// report.setProgrammingScore((progAchievedScore * 100)/
			// (progTotalScore * report.getTotalProgrammingQuestions()));
		}

		for (Report report : reportByCustomQuery) {
			System.out.println(report.toString());
		}
		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(reportByCustomQuery);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}
		return new ResponseEntity(test, HttpStatus.OK);
	}

	@GetMapping({ "/getReportByCUID" })
	public ResponseEntity reportByCuid(
			@RequestParam(value = "input", required = false) String userName) {
		StringBuilder query = new StringBuilder();
		List<Report> reportByCustomQuery = getReportforGivenCuid(userName);
		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(reportByCustomQuery);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}
		return new ResponseEntity(test, HttpStatus.OK);
	}
	
	@GetMapping({ "/getEventNames" })
	public ResponseEntity getEventNames(
			@RequestParam(value = "input", required = false) String eventName) {
		StringBuilder query = new StringBuilder();
		List<String> reportByCustomQuery = eventDAO.getEventNames(eventName);
		String test = "";
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			test = objectMapper.writeValueAsString(reportByCustomQuery);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}
		return new ResponseEntity(test, HttpStatus.OK);
	}
	
	
	/*@GetMapping({ "/getMCQProgramQustionAns" })
	public ResponseEntity getMCQProgramQustionAns(
			@RequestParam(value = "qNo", required = false) String qNo, @RequestParam(value = "userName", required = false) String userName
			, @RequestParam(value = "eventName", required = false) String eventName) {
		
		Report report = new Report();
		try {
		Question question= new Question();
		if(!StringUtils.isEmpty(qNo)){
			question = qustionDAO.get(new Integer(qNo).intValue());
		}
		
		report.setQuestionDesc(question.getQuestionDesc());
		UploadDownloadFileHelper downloadFileHelper = new UploadDownloadFileHelper();
		String filePath = downloadFileHelper.generateFilePath(eventName, userName, question.getFileName(), question.getQuestionNo());
		
		String code = downloadFileHelper.readFile(filePath);
		
		report.setCode(code);
		
		}catch(Exception e){
			
		}
		String response="";
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			
			response = objectMapper.writeValueAsString(report);
		} catch (JsonProcessingException arg5) {
			arg5.printStackTrace();
		}
		return new ResponseEntity(response, HttpStatus.OK);
	}*/

	private Map<String, String> generateMap(String data) {
		Map<String, String> dataMap = new HashMap<String, String>();
		data = subString(data);
		String[] pairs = data.split(",");
		for (int i = 0; i < pairs.length; i++) {
			String pair = pairs[i];
			String[] keyValue = pair.split(":");
			dataMap.put(subString(keyValue[0]), subString(keyValue[1]));
		}
		return dataMap;
	}

	private String subString(String content) {
		return content.substring(1, content.length() - 1);
	}

	private List<Report> getReportforGivenCuid(String user) {
		StringBuilder query = new StringBuilder();
		query.append("select candidate.Name as Candidate_id, e.event_name as Event_Name,");
		query.append(" candidate.Login  as Test_Time, role.Description as Employee_Role, candidate_answer.is_correct_answer");
		query.append(" as MCQ_Answer, candidate_answer.qno as Qno, question.Question_Complexity as Question_Complexity,");
		query.append(" candidate_answer.test_cases_passed as Test_Cases_Passed, candidate_answer.Total_Test_Case,");
		query.append(" candidate_answer.Total_Weightage as Total_Weightage, candidate_answer.weightage_achieved as weightage_achieved");
		query.append(" from interview_event e, role, candidate, candidate_answer, question where");
		query.append(" e.role_id = role.Role_ID and e.event_name = candidate.Event_NM and");
		query.append(" candidate.Candidate_ID = candidate_answer.candidate_id and candidate_answer.qno = question.QNo");
		query.append(" and candidate.Name = :user order by candidate.Candidate_id ");
		List<Report> reportByCustomQuery = reportDAO.getReportByCustomQuery(
				query.toString(), user, null, null, null, null);
		return reportByCustomQuery;
	}
	
	private List<Report> getQuestionAns(String user) {
		StringBuilder query = new StringBuilder();
		query.append("select candidate.Name as Candidate_id, e.event_name as Event_Name,");
		query.append(" candidate.Login  as Test_Time, role.Description as Employee_Role, candidate_answer.is_correct_answer");
		query.append(" as MCQ_Answer, candidate_answer.qno as Qno, question.Question_Complexity as Question_Complexity,");
		query.append(" candidate_answer.test_cases_passed as Test_Cases_Passed, candidate_answer.Total_Test_Case,");
		query.append(" candidate_answer.Total_Weightage as Total_Weightage, candidate_answer.weightage_achieved as weightage_achieved");
		query.append(" from interview_event e, role, candidate, candidate_answer, question where");
		query.append(" e.role_id = role.Role_ID and e.event_name = candidate.Event_NM and");
		query.append(" candidate.Candidate_ID = candidate_answer.candidate_id and candidate_answer.qno = question.QNo");
		query.append(" and candidate.Name = :user order by candidate.Candidate_id ");
		List<Report> reportByCustomQuery = reportDAO.getReportByCustomQuery(
				query.toString(), user, null, null, null, null);
		return reportByCustomQuery;
	}

}
