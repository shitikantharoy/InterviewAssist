package com.centurylink.spring.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "testcase_evaluation")
public class TestCaseEvaluation {
	private int testCaseId;
	private int totalTestCase;
	private int testCasePassed;
	private int questionNo;
}