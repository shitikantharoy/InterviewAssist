package com.centurylink.spring.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "candidate_answer")
public class CandidateAnswer {

	@Column(name = "candidate_answer")
	private String candidateAnswer;

	@ManyToOne
	@JoinColumn(name = "qno", referencedColumnName = "qno", insertable = false, updatable = false)
	private Question question;

	@ManyToOne
	@JoinColumn(name = "candidate_id", referencedColumnName = "candidate_id", insertable = false, updatable = false)
	private Candidate candidate;

	@EmbeddedId
	private CandidateAnswerId candidateAnswerId;
	
	@Column(name = "is_correct_answer")
	private String isAnswerCorrect;
	
	@Column(name = "total_test_case")
	private String totalTestCases;
	
	@Column(name = "test_cases_passed")
	private String passedTestCases;

	@Column(name = "total_weightage")
	private String totalWeightage;

	@Column(name = "weightage_achieved")
	private String achievedWeightage;

	public CandidateAnswer(CandidateAnswerId candidateAnswerId) {
		this.candidateAnswerId = candidateAnswerId;
	}

	public String getCandidateAnswer() {
		return candidateAnswer;
	}

	public void setCandidateAnswer(String candidateAnswer) {
		this.candidateAnswer = candidateAnswer;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public CandidateAnswerId getCandidateAnswerId() {
		return candidateAnswerId;
	}

	public void setCandidateAnswerId(CandidateAnswerId candidateAnswerId) {
		this.candidateAnswerId = candidateAnswerId;
	}

	public String getPassedTestCases() {
		return passedTestCases;
	}

	public void setPassedTestCases(String passedTestCases) {
		this.passedTestCases = passedTestCases;
	}

	public String getAchievedWeightage() {
		return achievedWeightage;
	}

	public void setAchievedWeightage(String achievedWeightage) {
		this.achievedWeightage = achievedWeightage;
	}

	public String getIsAnswerCorrect() {
		return isAnswerCorrect;
	}

	public void setIsAnswerCorrect(String isAnswerCorrect) {
		this.isAnswerCorrect = isAnswerCorrect;
	}

	public String getTotalTestCases() {
		return totalTestCases;
	}

	public void setTotalTestCases(String totalTestCases) {
		this.totalTestCases = totalTestCases;
	}

	public String getTotalWeightage() {
		return totalWeightage;
	}

	public void setTotalWeightage(String totalWeightage) {
		this.totalWeightage = totalWeightage;
	}
	
	public CandidateAnswer() {
		
	}

}
