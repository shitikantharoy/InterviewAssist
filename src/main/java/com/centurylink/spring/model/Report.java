package com.centurylink.spring.model;

import java.util.Date;

public class Report {
	private String candidateCuid;
	private String eventName;
	private String eventDate;
	private String role;
	private int totalMCQQuestions;
	private int totalCorrectMCQQuestions;
	private double mcqScore;
	private int totalProgrammingQuestions;
	private int totalCorrectProgrammingQuestions;
	private double programmingScore;
	private String complexity;
	private String code;
	private String correctFlag;
	private boolean programIndicator;
	private String questionType;
	private String totalTestCases;
	private String testCasesPassed;
	private Date eventStartDate;
	private Date eventEndDate;
	
	private String qNo;
	
	private String questionDesc;
	
	
	

	public String getQuestionDesc() {
		return questionDesc;
	}

	public void setQuestionDesc(String questionDesc) {
		this.questionDesc = questionDesc;
	}

	public String getqNo() {
		return qNo;
	}

	public void setqNo(String qNo) {
		this.qNo = qNo;
	}

	public String getCandidateCuid() {
		return candidateCuid;
	}

	public void setCandidateCuid(String candidateCuid) {
		this.candidateCuid = candidateCuid;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getTotalMCQQuestions() {
		return totalMCQQuestions;
	}

	public void setTotalMCQQuestions(int totalMCQQuestions) {
		this.totalMCQQuestions = totalMCQQuestions;
	}

	public int getTotalCorrectMCQQuestions() {
		return totalCorrectMCQQuestions;
	}

	public void setTotalCorrectMCQQuestions(int totalCorrectMCQQuestions) {
		this.totalCorrectMCQQuestions = totalCorrectMCQQuestions;
	}

	public double getMcqScore() {
		return mcqScore;
	}

	public void setMcqScore(double mcqScore) {
		this.mcqScore = mcqScore;
	}

	public int getTotalProgrammingQuestions() {
		return totalProgrammingQuestions;
	}

	public void setTotalProgrammingQuestions(int totalProgrammingQuestions) {
		this.totalProgrammingQuestions = totalProgrammingQuestions;
	}

	public int getTotalCorrectProgrammingQuestions() {
		return totalCorrectProgrammingQuestions;
	}

	public void setTotalCorrectProgrammingQuestions(
			int totalCorrectProgrammingQuestions) {
		this.totalCorrectProgrammingQuestions = totalCorrectProgrammingQuestions;
	}

	public double getProgrammingScore() {
		return programmingScore;
	}

	public void setProgrammingScore(double programmingScore) {
		this.programmingScore = programmingScore;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isProgramIndicator() {
		return programIndicator;
	}

	public void setProgramIndicator(boolean programIndicator) {
		this.programIndicator = programIndicator;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getCorrectFlag() {
		return correctFlag;
	}

	public void setCorrectFlag(String correctFlag) {
		this.correctFlag = correctFlag;
	}

	public String getTotalTestCases() {
		return totalTestCases;
	}

	public void setTotalTestCases(String totalTestCases) {
		this.totalTestCases = totalTestCases;
	}

	public String getTestCasesPassed() {
		return testCasesPassed;
	}

	public void setTestCasesPassed(String testCasesPassed) {
		this.testCasesPassed = testCasesPassed;
	}

	public String getComplexity() {
		return complexity;
	}

	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}

	public Date getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public Date getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

}
