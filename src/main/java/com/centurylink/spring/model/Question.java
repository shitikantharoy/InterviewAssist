package com.centurylink.spring.model;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "question")
@NamedQueries({
		@NamedQuery(name = "Question.findByType", query = "SELECT q from Question q where q.questionType=:question_type"),
		@NamedQuery(name = "Question.findAll", query = "SELECT q from Question q order by q.questionType desc") })
public class Question {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "QNo")
	int questionNo;

	@Column(name = "Question_Desc")
	String questionDesc;

	@Column(name = "Question_Complexity")
	String questionComplexity;

	@Column(name = "Question_Type")
	String questionType;

	@Column(name = "Question_Tech")
	String questionTechnology;

	@Column(name = "Question_Active")
	String questionActive;

	@Column(name = "Question_Time")
	int questionTime;

	@Column(name = "File_Name")
	String fileName;

	@Column(name = "Template_code")
	String templatecode;

	@Column(name = "Example")
	String example;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "question")
	@JsonIgnore
	// Added to resolve spring-mvc-could-not-initialize-proxy issue
	Collection<Applicability> applicability;

	public Collection<Applicability> getApplicability() {
		return applicability;
	}

	public void setApplicability(Collection<Applicability> applicability) {
		this.applicability = applicability;
	}

	public int getQuestionNo() {
		return this.questionNo;
	}

	public void setQuestionNo(int questionNo) {
		this.questionNo = questionNo;
	}

	public String getQuestionDesc() {
		return this.questionDesc;
	}

	public void setQuestionDesc(String questionDesc) {
		this.questionDesc = questionDesc;
	}

	public String getQuestionComplexity() {
		return this.questionComplexity;
	}

	public void setQuestionComplexity(String questionComplexity) {
		this.questionComplexity = questionComplexity;
	}

	public String getQuestionType() {
		return this.questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getQuestionTechnology() {
		return this.questionTechnology;
	}

	public void setQuestionTechnology(String questionTechnology) {
		this.questionTechnology = questionTechnology;
	}

	public String getQuestionActive() {
		return this.questionActive;
	}

	public void setQuestionActive(String questionActive) {
		this.questionActive = questionActive;
	}

	public int getQuestionTime() {
		return this.questionTime;
	}

	public void setQuestionTime(int questionTime) {
		this.questionTime = questionTime;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public static long getSerialversionuid() {
		return 1L;
	}

	public String getExample() {
		return this.example;
	}

	public void setExample(String example) {
		this.example = example;
	}

	public String getTemplateCode() {
		return templatecode;
	}

	public void setTemplateCode(String templatecode) {
		this.templatecode = templatecode;
	}

}