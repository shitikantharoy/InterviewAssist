package com.centurylink.spring.model;

import com.centurylink.spring.model.Question;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "answer")
public class Answer {
	@Id
	@GeneratedValue
	@Column(name = "Answer_ID")
	int answerId;
	@Column(name = "Option_1")
	String option1;
	@Column(name = "Option_2")
	String option2;
	@Column(name = "Option_3")
	String option3;
	@Column(name = "Option_4")
	String option4;
	@Column(name = "Option_5")
	String option5;
	@Column(name = "Correct_Answer")
	String correctAnswer;
	@Column(name = "Option_Type")
	String optionType;
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "QNo", referencedColumnName = "QNo")
	Question question;
	@Column(name = "QNo", insertable = false, updatable = false)
	int QNo;

	public int getAnswerId() {
		return this.answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public String getOption1() {
		return this.option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOption2() {
		return this.option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public String getOption3() {
		return this.option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	}

	public String getOption4() {
		return this.option4;
	}

	public void setOption4(String option4) {
		this.option4 = option4;
	}

	public String getOption5() {
		return this.option5;
	}

	public void setOption5(String option5) {
		this.option5 = option5;
	}

	public String getCorrectAnswer() {
		return this.correctAnswer;
	}

	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public Question getQuestion() {
		return this.question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public String getOptionType() {
		return this.optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public int getQNo() {
		return this.QNo;
	}

	public void setQNo(int qNo) {
		this.QNo = qNo;
	}
}