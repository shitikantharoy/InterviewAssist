package com.centurylink.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "user")
@NamedQueries({
	@NamedQuery(name = "user.findAll", query = "select u from User u"),
	@NamedQuery(name = "user.findByUserID", query = "SELECT u FROM User u WHERE u.userId = :user_id"),})
public class User {
	@Id
	@Column(name = "User_ID")
	private int userId;
	@Column(name = "CUID")
	private String cuid;
	@Column(name = "privilege")
	private String privilege;
	@Column(name = "Name")
	private String name;

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getCuid() {
		return this.cuid;
	}

	public void setCuid(String cuid) {
		this.cuid = cuid;
	}

	public String getPrivilege() {
		return this.privilege;
	}

	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}