package com.centurylink.spring.model;

import com.centurylink.spring.model.Question;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "testcase")
public class TestCase {
	@Id
	@GeneratedValue
	@Column(name = "Test_Case_id")
	private int testCaseId;
	@Column(name = "Test_Case")
	private String testCase;
	@Column(name = "Test_File_Name")
	private String testFileName;
	@Column(name = "Test_Case_Weightage")
	private int testCaseWeightage;
	@ManyToOne
	@JoinColumn(name = "QNo", referencedColumnName = "QNo")
	Question question;
	@Column(name = "QNo", insertable = false, updatable = false)
	int QNo;

	public int getTestCaseId() {
		return this.testCaseId;
	}

	public void setTestCaseId(int testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getTestCase() {
		return this.testCase;
	}

	public void setTestCase(String testCase) {
		this.testCase = testCase;
	}

	public String getTestFileName() {
		return this.testFileName;
	}

	public void setTestFileName(String testFileName) {
		this.testFileName = testFileName;
	}

	public int getTestCaseWeightage() {
		return this.testCaseWeightage;
	}

	public void setTestCaseWeightage(int testCaseWeightage) {
		this.testCaseWeightage = testCaseWeightage;
	}

	public Question getQuestion() {
		return this.question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public int getQNo() {
		return this.QNo;
	}

	public void setQNo(int qNo) {
		this.QNo = qNo;
	}
}