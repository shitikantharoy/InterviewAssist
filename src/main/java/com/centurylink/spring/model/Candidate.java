package com.centurylink.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "candidate")
@NamedQueries(value = {
		@NamedQuery(name = "Candidate.findCandidate", query = "select c from Candidate c where c.name=:uniqueid"),
		@NamedQuery(name ="Candidate.findCandidateByEvent", query = "select c from Candidate c where c.name=:uniqueid and c.eventNm=:eventNm") })

public class Candidate {
	@Id
	@Column(name = "Candidate_ID")
	private int candidateId;
	@Column(name = "Event_NM")
	private String eventNm;
	@Column(name = "Name")
	private String name;
	@Column(name = "Login")
	private Date login;
	@Column(name = "Logout")
	private Date logout;
	@Column(name = "isClickedExit")
	private String isClickedExit;

	

	public int getCandidateId() {
		return this.candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public String getEventNm() {
		return this.eventNm;
	}

	public void setEventNm(String eventNm) {
		this.eventNm = eventNm;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getLogin() {
		return this.login;
	}

	public void setLogin(Date login) {
		this.login = login;
	}

	public Date getLogout() {
		return this.logout;
	}

	public void setLogout(Date logout) {
		this.logout = logout;
	}
	
	public String getIsClickedExit() {
		return isClickedExit;
	}

	public void setIsClickedExit(String isClickedExit) {
		this.isClickedExit = isClickedExit;
	}
	
}