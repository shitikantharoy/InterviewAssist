package com.centurylink.spring.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.centurylink.ldap.LoginAuthenticator;
import com.centurylink.spring.dao.UserDAO;
import com.centurylink.spring.model.User;

@Service("customUserDetailsService")
public class CustomUserDetailsService  implements AuthenticationProvider {
	
	private static HashMap<String, User> map = new HashMap<String, User>();
	
	@Autowired
	private UserDAO userDAO;

	public static User getCurrentUser() {
		String key = (String) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		if (map.containsKey(key.toLowerCase())) {
			return map.get(key.toLowerCase());
		}
		return null;
	}
	
	public static void removeUser(String cuid) {
		map.remove(cuid);
	}
	
	@Override
	public Authentication authenticate(Authentication arg0)
			throws AuthenticationException {
		String cuid = arg0.getName();
		String password = arg0.getCredentials().toString();
		Authentication auth = null;
		boolean isLoginSuccessful = false;
		isLoginSuccessful = LoginAuthenticator.doAuthenticate(cuid.toLowerCase(), password, false);
		User user = userDAO.get(cuid);
		if (user != null && isLoginSuccessful) {
			map.put(user.getCuid().toLowerCase(), user);
			List<GrantedAuthority> grantedAuths = new ArrayList();
			grantedAuths.add(new SimpleGrantedAuthority("ROLE_" + user.getPrivilege().toUpperCase()));
			
			auth = new UsernamePasswordAuthenticationToken(cuid, password, grantedAuths);

			return auth;
		}
		else
		{
			throw new UsernameNotFoundException("Username not found");
		}
	}

	@Override
	public boolean supports(Class<?> arg0) {
		return arg0.equals(UsernamePasswordAuthenticationToken.class);
	}
}
