package com.centurylink.spring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(customUserDetailsService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
		.antMatchers("/renderquestions").permitAll().and()
		.authorizeRequests()
		.antMatchers("/resources/**").permitAll().and()
		.authorizeRequests()
		.antMatchers("/user").access("hasRole('ROLE_ADMIN')").and()
		.authorizeRequests()
		.antMatchers("/question").access("hasRole('ROLE_ADMIN')")
        .and().formLogin()
        .loginPage("/login").failureUrl("/login?error")
        .usernameParameter("uName")
        .passwordParameter("password")
		.loginProcessingUrl("/j_spring_security_check")
		.defaultSuccessUrl("/validate")
		.and().logout().logoutSuccessUrl("/login?logout")
		.and().csrf().disable()
		.exceptionHandling().accessDeniedPage("/Access_Denied");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(customUserDetailsService);
	}
}
