package com.centurylink.spring.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.centurylink.spring.model.User;


public class SecurityInterceptor implements HandlerInterceptor{
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		HttpSession session=request.getSession(false);
		if (session != null) {
			User user=(User) session.getAttribute("user");
			if (user != null) {
				return true;
			} else {
				if(user==null) {
					user = CustomUserDetailsService.getCurrentUser();
					if(user==null) {
						response.sendRedirect("/InterviewAssist/");
						return false;
					}
					return true;
				}
			}
		} else {
			response.sendRedirect("/InterviewAssist/");
			return false;
		}
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}
 
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
	}
   
}
