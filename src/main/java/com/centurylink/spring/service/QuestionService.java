package com.centurylink.spring.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.centurylink.constants.PortalConstants;
import com.centurylink.spring.dao.EventDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.model.Event;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.Role;
import com.mysql.jdbc.StringUtils;

public class QuestionService implements PortalConstants {

	private static Map<String, Integer> roleMap = null;

	private static Logger log = Logger.getLogger(QuestionService.class.getName());

	@Autowired
	RoleDAO roleDAO;

	@Autowired
	QuestionDAO questionDAO;

	@Autowired
	EventDAO eventDAO;

	static {
		/*
		 * List<Role> roleList = roleDAO.list(); roleMap = new
		 * HashMap<Integer,String>(); if (roleList != null && roleList.size()
		 * >0) { for(Role role : roleList) { roleMap.put(role.getRoleId(),
		 * role.getRole()); } }
		 */

	}

	private void populateRoleMap() {
		if (roleMap == null) {
			List<Role> roleList = roleDAO.list();
			roleMap = new HashMap<String, Integer>();
			if (roleList != null && roleList.size() > 0) {
				for (Role role : roleList) {
					roleMap.put(role.getRole(), role.getRoleId());
				}
			}
		}
	}

	private int getRoleIdForRole(String role) {

		log.debug("getRoleIdForRole role : " + role);

		int roleId = -1;

		if (roleMap == null) {
			populateRoleMap();
		}

		if (!StringUtils.isNullOrEmpty(role)) {
			roleId = roleMap.get(role.toUpperCase());
		}

		log.debug("getRoleIdForRole roleid :" + roleId);
		return roleId;

	}

	public List<Question> getQuestionByEvent(String eventName) {

		List<Question> questionList = new ArrayList<Question>();

		if (!StringUtils.isNullOrEmpty(eventName)) {
			Event event = eventDAO.get(eventName);
		}

		return questionList;

	}
	
	private List<Question> getFilteredQuestions(int numberOfQuestion, List<Question> questionList) {

		Collections.shuffle(questionList, new Random());

		if (questionList != null) {
			if (questionList.size() > numberOfQuestion) {
				questionList = questionList.subList(0, numberOfQuestion);
			}
		}
		
		return questionList;
	}

	public List<Question> identifyQuestionsForEvent(Event event, String questionType) {

		List<Question> questionList = new ArrayList<Question>();

		Question questionTemplate = new Question();
		questionTemplate.setQuestionTechnology(event.getEventTechnology());

		if (MCQ.equalsIgnoreCase(questionType)) {
			if (!StringUtils.isNullOrEmpty(event.getSimpleMcq())) {
				int simplemcq = Integer.parseInt(event.getSimpleMcq());
				if (simplemcq > 0) {
					questionTemplate.setQuestionComplexity(QUESTION_SIMPLE);
					questionTemplate.setQuestionType(MCQ);

					List<Question> questions = questionDAO.getQuestionsByCustomQuery(getQuestionsHQLQueryByRole(
							questionTemplate, event.getRoleId()).toString());
					questionList.addAll(getFilteredQuestions(simplemcq, questions));
				}
			}

			if (!StringUtils.isNullOrEmpty(event.getMediumMcq())) {
				int meidummcq = Integer.parseInt(event.getMediumMcq());
				if (meidummcq > 0) {
					questionTemplate.setQuestionComplexity(QUESTION_MEDIUM);
					questionTemplate.setQuestionType(MCQ);

					List<Question> questions = questionDAO.getQuestionsByCustomQuery(getQuestionsHQLQueryByRole(
							questionTemplate, event.getRoleId()).toString());
					questionList.addAll(getFilteredQuestions(meidummcq, questions));
				}
			}

			if (!StringUtils.isNullOrEmpty(event.getComplexMcq())) {
				int complexmcq = Integer.parseInt(event.getComplexMcq());
				if (complexmcq > 0) {
					questionTemplate.setQuestionComplexity(QUESTION_COMPLEX);
					questionTemplate.setQuestionType(MCQ);

					List<Question> questions = questionDAO.getQuestionsByCustomQuery(getQuestionsHQLQueryByRole(
							questionTemplate, event.getRoleId()).toString());
					questionList.addAll(getFilteredQuestions(complexmcq, questions));
				}
			}
		}

		if (PROGRAMMING.equalsIgnoreCase(questionType)) {

			if (!StringUtils.isNullOrEmpty(event.getSimplePgm())) {
				int simplepgm = Integer.parseInt(event.getSimplePgm());
				if (simplepgm > 0) {
					questionTemplate.setQuestionComplexity(QUESTION_SIMPLE);
					questionTemplate.setQuestionType(PROGRAMMING);

					List<Question> questions = questionDAO.getQuestionsByCustomQuery(getQuestionsHQLQueryByRole(
							questionTemplate, event.getRoleId()).toString());
					questionList.addAll(getFilteredQuestions(simplepgm, questions));
					
				}
			}

			if (!StringUtils.isNullOrEmpty(event.getMediumPgm())) {
				int mediumpgm = Integer.parseInt(event.getMediumPgm());
				if (mediumpgm > 0) {
					questionTemplate.setQuestionComplexity(QUESTION_MEDIUM);
					questionTemplate.setQuestionType(PROGRAMMING);
					
					List<Question> questions = questionDAO.getQuestionsByCustomQuery(getQuestionsHQLQueryByRole(
							questionTemplate, event.getRoleId()).toString());
					questionList.addAll(getFilteredQuestions(mediumpgm, questions));

				}
			}

			if (!StringUtils.isNullOrEmpty(event.getComplexPgm())) {
				int complexpgm = Integer.parseInt(event.getComplexPgm());
				if (complexpgm > 0) {
					questionTemplate.setQuestionComplexity(QUESTION_COMPLEX);
					questionTemplate.setQuestionType(PROGRAMMING);
					
					List<Question> questions = questionDAO.getQuestionsByCustomQuery(getQuestionsHQLQueryByRole(
							questionTemplate, event.getRoleId()).toString());
					questionList.addAll(getFilteredQuestions(complexpgm, questions));
				}
			}
		}

		return questionList;
	}

	public List<Question> getQuestionsForEvent(Event event, String questionType) {

		List<Question> questionList = null;

		questionList = identifyQuestionsForEvent(event, questionType);
		// StringBuffer questionQuery = new StringBuffer();

		/*
		 * if (questionTemplate != null && questionTemplate.size() > 0) { int
		 * listSize = questionTemplate.size(); for (int i = 0; i < listSize;
		 * i++) { Question question = questionTemplate.get(i); String query =
		 * getQuestionsQueryByRole(question, event.getRoleId());
		 * questionQuery.append(query); if (i != (listSize - 1)) {
		 * questionQuery.append(" UNION "); } } }
		 */

		// log.debug("getQuestionsForEvent " + questionQuery.toString());
		// questionList =
		// questionDAO.getQuestionsByCustomQuery(questionQuery.toString());

		return questionList;
	}

	public String getQuestionsSQLQueryByRole(Question question, int roleId) {

		// List<Question> questionList = null;

		// int roleId = getRoleIdForRole(role);

		StringBuffer query = new StringBuffer("select Q.* from Question Q, Applicability A where ");
		query.append(" Q.questionNo = A.question.questionNo");
		query.append(" and A.applicabilityId.roleId = " + roleId);
		query.append(" and Q.questionComplexity = '" + question.getQuestionComplexity() + "'");
		query.append(" and Q.questionType = '" + question.getQuestionType() + "'");
		query.append(" and Q.questionActive = 'Y'");
		query.append(" and Q.questionTechnology = '" + question.getQuestionTechnology() + "'");

		log.debug(query);
		return query.toString();

	}

	public String getQuestionsHQLQueryByRole(Question question, int roleId) {

		// List<Question> questionList = null;

		// int roleId = getRoleIdForRole(role);

		StringBuffer query = new StringBuffer("select Q from Question Q, Applicability A where ");
		query.append(" Q.questionNo = A.question.questionNo");
		query.append(" and A.applicabilityId.roleId = " + roleId);
		query.append(" and Q.questionComplexity = '" + question.getQuestionComplexity() + "'");
		query.append(" and Q.questionType = '" + question.getQuestionType() + "'");
		query.append(" and Q.questionActive = 'Y'");
		query.append(" and Q.questionTechnology = '" + question.getQuestionTechnology() + "'");

		log.debug(query);
		return query.toString();

	}

}
