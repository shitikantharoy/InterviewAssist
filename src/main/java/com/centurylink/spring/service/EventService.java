package com.centurylink.spring.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.centurylink.spring.dao.EventDAO;
import com.centurylink.spring.model.Event;
import com.mysql.jdbc.StringUtils;

public class EventService {

	private static Logger log = Logger.getLogger(EventService.class.getName());

	@Autowired
	EventDAO eventDAO;

	public boolean validateEvent(String eventName) {

		boolean isValidEvent = false;

		Event event = eventDAO.get(eventName);

		if (event != null && validateDate(event.getEventEndDate() + " " + event.getEventEndTime(),
				event.getEventStartDate() + " " + event.getEventStartTime())) {
			isValidEvent = true;
		}
		return isValidEvent;
	}

	public Event getEvent(String eventName) {
		return eventDAO.get(eventName);
	}

	public int totalMCQ(Event event) {

		int count = 0;

		if (!StringUtils.isNullOrEmpty(event.getSimpleMcq())) {
			count += Integer.parseInt(event.getSimpleMcq());
		}

		if (!StringUtils.isNullOrEmpty(event.getMediumMcq())) {
			count += Integer.parseInt(event.getMediumMcq());
		}

		if (!StringUtils.isNullOrEmpty(event.getComplexMcq())) {
			count += Integer.parseInt(event.getComplexMcq());
		}

		log.debug("totalMCQ " + count);

		return count;
	}

	public int totalProgram(Event event) {

		int count = 0;

		if (!StringUtils.isNullOrEmpty(event.getSimplePgm())) {
			count += Integer.parseInt(event.getSimplePgm());
		}

		if (!StringUtils.isNullOrEmpty(event.getMediumPgm())) {
			count += Integer.parseInt(event.getMediumPgm());
		}

		if (!StringUtils.isNullOrEmpty(event.getComplexPgm())) {
			count += Integer.parseInt(event.getComplexPgm());
		}

		log.debug("totalProgram " + count);

		return count;
	}

	private boolean validateDate(String endDate, String startDate) {

		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

		try {
			Date endDat = sdf.parse(endDate);
			Date startDat = sdf.parse(startDate);
			if (endDat.compareTo(new Date()) > 0 && startDat.compareTo(new Date()) < 0) {
				System.out.println("EndDate is after Current date");
				System.out.println("startDate is after Current date");
				return true;
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;

	}

}
