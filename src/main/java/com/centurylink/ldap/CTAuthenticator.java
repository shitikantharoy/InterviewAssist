package com.centurylink.ldap;

import javax.naming.AuthenticationException;

import com.ctl.appsec.actrl.AccessControl;
import com.ctl.appsec.actrl.AccessControlFactory;
import com.ctl.appsec.actrl.BasicCredential;

/**
 * This class use Qwest ClearTrust server to authenticate user and retrieve user
 * attributes
 */


public class CTAuthenticator {

	private AccessControl accessControl = null;
	private BasicCredential basicCred = null;
	private static final String ET_PORTAL_INT = "EtPortalIni";
	/**
	 * authenticate user using ClearTrust
	 * 
	 * @parm principal
	 * @parm credential
	 * @return true if authenticated by ClearTrust server, false otherwise
	 */
	public boolean authenticateUser(String principal, String credential)
			throws Exception {
		boolean isAuthenticated = false;
		
		
		
	    System.setProperty("com.ctl.appsec.actrl.applName", "CTLIInterviewTool");
		System.setProperty("com.ctl.appsec.actrl.ctenv", "employee.test");
		
		try {
			accessControl=AccessControlFactory.getInstance();
			basicCred = new BasicCredential(principal, credential);
			accessControl.authenticate(basicCred);
			isAuthenticated=true;
		}	
		catch (AuthenticationException authExc) {
			isAuthenticated=false;
		} 
		catch (Exception e) {
			throw e;
		} finally {
			
		}
		return isAuthenticated;

	}

	
	public boolean isValidUserNm(String userName) {
		boolean isValid = false;

		try {
			isValid = true;
		} catch (Exception ne) {
			isValid = false;
		}

		return isValid;

	}
	
	public static boolean authenticate(String user, String passwd) throws Exception{
		CTAuthenticator  ct = new CTAuthenticator();
		return ct.authenticateUser(user, passwd);
	}

	public static void main (String[] args){
		CTAuthenticator  ct = new CTAuthenticator();
		try {
			System.out.println(ct.authenticateUser("inqwest1", "Qwest_123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
