package com.centurylink.ldap;

public class LoginAuthenticator {
	
	
	public static boolean doAuthenticate(String cuid, String password, boolean flag) {
		try {
			return CTAuthenticator.authenticate(cuid, password);
		} catch (Exception e) {
			return false;
		}
	}
	
	public static void main (String[] args) {
		try {
		    System.out.println(
		    		LoginAuthenticator.doAuthenticate("", "", false));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
