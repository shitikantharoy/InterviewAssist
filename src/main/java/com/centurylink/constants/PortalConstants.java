package com.centurylink.constants;

public interface PortalConstants {

	public static final String COMMA = ",";
	public static final String SPACE = " ";
	public static final String DOT_DELIMETTER = "\\.";
	public static final String FORWARD_SLASH_DELIMETTER = "\\";
	public static final String DOT = ".";
	public static final String DOT_JAVA = ".java";

	public static final String ONE = "1";
	public static final String TWO = "2";
	public static final String THREE = "3";
	public static final String FOUR = "4";

	public static final String YES = "Yes";
	public static final String NO = "No";
	public static final String SUCCESS = "SUCCESS";

	public static final String NEXT = "NEXT";
	public static final String PREVIOUS = "PREVIOUS";
	public static final String EXIT = "EXIT";
	public static final String SAVE = "SAVE";
	public static final String COMPILE = "COMPILE";
	public static final String VALIDATE = "VALIDATE";
	public static final String PROGRAMMING = "Programming";

	public static final String INTERVIEW_LOGIN = "interviewlogin";

	public static final String INVALID_EVENT = "invalidevent";

	public static final String QUESTION_SIMPLE = "Simple";
	public static final String QUESTION_MEDIUM = "Medium";
	public static final String QUESTION_COMPLEX = "Complex";

	public static final String MCQ = "MCQ";
	public static final String CHECKED = "checked";

	public static final String ACTIVE_EVENT = "ACTIVE";
	public static final String ACTIVE_QUESTION = "Y";

	public static final String STARTTEST = "STARTTEST";
}
