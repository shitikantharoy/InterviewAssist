package com.centurylink.transaction.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.constants.PortalConstants;
import com.centurylink.spring.controller.QuestionFormBean;
import com.centurylink.spring.dao.AnswerDAO;
import com.centurylink.spring.dao.ApplicabilityDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.dao.TestCaseDAO;
import com.centurylink.spring.model.Answer;
import com.centurylink.spring.model.Applicability;
import com.centurylink.spring.model.ApplicabilityId;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.Role;
import com.centurylink.spring.model.TestCase;
import com.mysql.jdbc.StringUtils;

public class SessionFacade {
	@Autowired
	private QuestionDAO questionDAO;
	@Autowired
	private TestCaseDAO testCaseDAO;
	@Autowired
	private ApplicabilityDAO applicabilityDAO;
	@Autowired
	private RoleDAO roleDAO;
	@Autowired
	private AnswerDAO answerDAO;

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public void insertQuestion(QuestionFormBean questionForm) throws Exception {
		Question questionData = new Question();
		questionData.setQuestionDesc(questionForm.getQuestionDesc());
		questionData.setExample(questionForm.getExample());
		questionData.setQuestionTechnology(questionForm.getQuestionTechnology());
		questionData.setQuestionComplexity(questionForm.getQuestionComplexity());
		questionData.setQuestionType(questionForm.getQuestionType());
		//questionData.setQuestionTime((new Integer(questionForm.getQuestionTime())).intValue());
		questionData.setQuestionActive(PortalConstants.ACTIVE_QUESTION);
		questionData.setFileName(questionForm.getFileName());
		questionData.setTemplateCode(questionForm.getTemplateCode());
		
		List<Applicability> applicabilityList = new ArrayList<Applicability>();
		
		
		int answer;
		if (questionForm != null && null != questionForm.getRole() && questionForm.getRole().length > 0) {
			for (answer = 0; answer < questionForm.getRole().length; ++answer) {
				if (!StringUtils.isNullOrEmpty(questionForm.getRole()[answer])) {
					Role testCase = this.roleDAO.get(questionForm.getRole()[answer]);
					Applicability applicability = new Applicability();
					applicability.setQuestion(questionData);
					ApplicabilityId applicabilityId = new ApplicabilityId(testCase.getRoleId());
					applicability.setApplicabilityId(applicabilityId);
					//applicability.setRoleId(testCase.getRoleId());
					//this.applicabilityDAO.insert(applicability);
					applicabilityList.add(applicability);
				}
			}
		}
		
		questionData.setApplicability(applicabilityList);
		this.questionDAO.insert(questionData);

		if (questionForm != null && null != questionForm.getTestCode() && questionForm.getTestCode().length > 0
				&& "Programming".equalsIgnoreCase(questionForm.getQuestionType())) {
			for (answer = 0; answer < questionForm.getTestCode().length; ++answer) {
				if (!StringUtils.isNullOrEmpty(questionForm.getTestCode()[answer])) {
					TestCase arg6 = new TestCase();
					arg6.setQuestion(questionData);
					arg6.setTestCase(questionForm.getTestCode()[answer]);
					arg6.setTestFileName(questionForm.getTestFileNM()[answer]);
					arg6.setTestCaseWeightage((new Integer(questionForm.getTestWt()[answer])).intValue());
					this.testCaseDAO.insert(arg6);
				}
			}
		}

		if ("MCQ".equalsIgnoreCase(questionForm.getQuestionType())) {
			Answer arg5 = new Answer();
			arg5.setOptionType(questionForm.getOptionTypSelect());
			arg5.setQuestion(questionData);
			/*if ("Single".equalsIgnoreCase(questionForm.getOptionTypSelect())) {
				if (!StringUtils.isNullOrEmpty(questionForm.getSingleOptionTxt1())) {
					arg5.setOption1(questionForm.getSingleOptionTxt1());
				}

				if (!StringUtils.isNullOrEmpty(questionForm.getSingleOptionTxt2())) {
					arg5.setOption2(questionForm.getSingleOptionTxt2());
				}

				if (!StringUtils.isNullOrEmpty(questionForm.getSingleOptionTxt3())) {
					arg5.setOption3(questionForm.getSingleOptionTxt3());
				}

				if (!StringUtils.isNullOrEmpty(questionForm.getSingleOptionTxt4())) {
					arg5.setOption4(questionForm.getSingleOptionTxt4());
				}

				if (!StringUtils.isNullOrEmpty(questionForm.getSingleOptionTxt5())) {
					arg5.setOption5(questionForm.getSingleOptionTxt5());
				}

				if (!StringUtils.isNullOrEmpty(questionForm.getSingleChoiceAns())) {
					arg5.setCorrectAnswer(questionForm.getSingleChoiceAns());
				}
			}*/

			//if ("Multiple".equalsIgnoreCase(questionForm.getOptionTypSelect())) {
				if (!StringUtils.isNullOrEmpty(questionForm.getMultipleOptionTxt1())) {
					arg5.setOption1(questionForm.getMultipleOptionTxt1());
				}

				if (!StringUtils.isNullOrEmpty(questionForm.getMultipleOptionTxt2())) {
					arg5.setOption2(questionForm.getMultipleOptionTxt2());
				}

				if (!StringUtils.isNullOrEmpty(questionForm.getMultipleOptionTxt3())) {
					arg5.setOption3(questionForm.getMultipleOptionTxt3());
				}

				if (!StringUtils.isNullOrEmpty(questionForm.getMultipleOptionTxt4())) {
					arg5.setOption4(questionForm.getMultipleOptionTxt4());
				}

				if (!StringUtils.isNullOrEmpty(questionForm.getMultipleOptionTxt5())) {
					arg5.setOption5(questionForm.getMultipleOptionTxt5());
				}

				if (!StringUtils.isNullOrEmpty(questionForm.getSelectedMultiAnsOpt())) {
					arg5.setCorrectAnswer(questionForm.getSelectedMultiAnsOpt());
				}
		//	}

			this.answerDAO.insert(arg5);
		}

	}
}